<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/semantic/semantic.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/map.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>assets/DataTables/media/css/jquery.dataTables.css" />
<!--        <link rel="stylesheet" href="<?php echo base_url() ?>assets/DataTables/media/css/responsive.dataTables.css" />-->
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/leaflet/leaflet.css" />
        <script src="<?php echo base_url() ?>/assets/js/jquery-1.10.1.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/semantic/semantic.js"></script>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="icon" type="image/png" href="<?php echo base_url() ?>/public/img/favicon.ico" sizes="32x32">
        <base href="<?php echo site_url(); ?>" />
    </head>
    <title>Welcome to Kabaadi App</title>
    <body>
        <?
        if ($this->session->userdata('group') == 1) {
            $group = 'Super Admin';
            $name = $this->session->userdata('name');
        } elseif ($this->session->userdata('group') == 2) {
            $group = 'Admin';
            $name = $this->session->userdata('name');
        } elseif ($this->session->userdata('group') == 3) {
            $group = 'User';
            $name = $this->session->userdata('contact');
        }
        ?>
        <div class="fixed-container">
            <a class="<? if ($uri == 'home') echo'active' ?> item" href="<?= site_url('main') ?>">
                <img class="ui image"  src="<?= base_url("/assets/imgs/logo.png"); ?>" style="display: inline-block;width:78px; padding-left: 10px;">
                <span style="font-size: 22px; color: #003399;">Kabaadi</span>
            </a>
            <div class="ui menu">
                <a class="item" onclick="sidebar()">
                    <i class="sidebar icon"></i>
                </a>
                <div class="right menu">
                    <!-- <img src="/public/img/favicon.ico" class="img-rounded"> -->
                    <div class="ui yellow segment"> <i class="user large icon"></i> <?= @$name ?> (<?= $group ?>)</div>           
                    <div class="ui top pointing right dropdown item">
                        <i class="settings icon"></i>
                        <div class="menu">
                            <a class="item" href="<?= site_url('login/password_change') ?>">
                                <i class="edit icon"></i>
                                Change Password
                            </a>
                            <a class="item" href="<?= site_url('login/logout') ?>">
                                <i class="log out icon"></i>
                                Logout
                            </a>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <!-- Navbar -->
        <div class="ui left vertical overlay labeled sidebar menu">
            <a class="<? if ($uri == 'home') echo'active' ?> item" href="<?= site_url('main') ?>">
                <i class="large home icon"></i>
                Home
            </a>
            <? if ($this->session->userdata('group') == 1) : ?>
                <a class="<? if ($uri == 'dealers') echo'active' ?> item" href="<?= site_url('dealer') ?>">
                    <i class="large users icon"></i>
                    Dealers
                </a>
                <a class="<? if ($uri == 'users') echo'active' ?> item" href="<?= site_url('users') ?>">
                    <i class="large mobile icon"></i>
                    Users
                </a>
                <a class="<? if ($uri == 'notification') echo'active' ?> item" href="<?= site_url('notification') ?>">
                    <i class="large mail icon"></i>
                    Notice
                </a>
                <a class="<? if ($uri == 'category') echo'active' ?> item" href="<?= site_url('waste_category') ?>">
                    <i class="large trash icon"></i>
                    Waste Category
                </a>
                <a class="<? if ($uri == 'pickup') echo'active' ?> item" href="<?= site_url('pickup') ?>">
                    <i class="large shipping icon"></i>
                    Pickup Request
                </a>
                <a class="<? if ($uri == 'aboutus') echo'active' ?> item" href="<?= site_url('aboutus') ?>">
                    <i class="large at icon"></i>
                    About Us
                </a>
            <? elseif ($this->session->userdata('group') == 2) : ?>
                <a class="<? if ($uri_segment == 'category') echo'active' ?> item" href="<?= site_url('dealer_control/category') ?>">
                    <i class="large trash icon"></i>
                    Waste Category
                </a>
                <a class="<? if ($uri_segment == 'pickup') echo'active' ?> item" href="<?= site_url('dealer_control/pickup') ?>">
                    <i class="large shipping icon"></i>
                    Pickup Request
                </a>
                <a class="<? if ($uri_segment == 'pickup image') echo'active' ?> item" href="<?= site_url('dealer_control/pickup_image') ?>">
                    <i class="large shipping icon"></i>
                    Image Pickup Request
                </a>
            <? elseif ($this->session->userdata('group') == 3) : ?>
                <a class="<? if ($uri == 'activity') echo'active' ?> item" href="<?= site_url('mobile_users/dashboard') ?>">
                    <i class="large dashboard icon"></i>
                    Dashboard
                </a>
                <a class="<? if ($uri == 'activity') echo'active' ?> item" href="<?= site_url('mobile_users/editprofile') ?>">
                    <i class="large user icon"></i>
                    My Profiles
                </a>
                <a class="<? if ($uri == 'activity') echo'active' ?> item" href="<?= site_url('mobile_users/activities') ?>">
                    <i class="large trash icon"></i>
                    My Activities
                </a>
                <a class="<? if ($uri == 'activity') echo'active' ?> item" href="<?= site_url('mobile_users/allpickups') ?>">
                    <i class="large trash icon"></i>
                    All Previous Pickups
                </a>
            <? endif; ?>
            <div class="item" style="width: 100%; padding: 10px 0px;">
                <center>Copyright © 2016  Kabaadi</center>
                <center>Design & Developed</center>
                <center>By <a href="#" target="_blank">Powered By: Recycle Up Managed By: Blue Waste to Value</a></center>
            </div>
        </div>
        <!-- END of Navbar -->