<? $this->load->view('header'); ?>
<div class="waste-container">
    <div class="ui huge breadcrumb">
        <a class="section">Kabaadi App </a>
        <i class="right chevron icon divider"></i>
        <a href="<?= site_url('dealer_control/pickup') ?>" class="active section"><i class="shipping icon"></i>Pickup Request</a>
    </div>
    <div class="ui divider"></div>
    <? if ($this->session->flashdata('failure-msg') != ''): ?>
        <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
                Error!!
            </div>
            <p><?= $this->session->flashdata('failure-msg') ?></p>
        </div>
    <? elseif ($this->session->flashdata('success-msg') != ''): ?>
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Congratulation!!
            </div>
            <p><?= $this->session->flashdata('success-msg') ?></p>
        </div>
    <? endif; ?>
    <div class="ui three column grid margin-top">
        <!--        <div class="column"> 
                    <h3 class="ui blue header">
                        
                        <div class="content">
                            Request
                        </div>
                    </h3>
                </div>
                <div class="column"></div>
                <div class="column">
                    <form id="search" method="post" action="<?php echo site_url('dealer_control/search/') ?> ">
                        <div class="ui  search">
                            <div class="ui icon input">
                                <input class="prompt required" required="" name="search" placeholder="Search products..." type="text">
                                <i id="search_link" class="inverted blue circular search link icon"></i>
                            </div>
                            <div class="results"></div>
                        </div>
                    </form>
                </div>-->
    </div>
    <table class="ui celled table" id="pickup_request">
        <thead>
            <tr><th>SN</th>
                <th>Title</th>
                <th>Requester Contact</th>
                <th>Pickup location</th>
                <th>Pickup Status</th>
                <th>Pickup Status Change</th>
                <th>Pickup Request Date</th>
                <th>Action</th>
            </tr></thead>
        <tbody>
            <?
            $s = 1;
            foreach ($pickup_request as $row): $sn = $s++;
                ?>
                <?php
                if ($row['status'] == 0) {
                    $status = 'On Queue';
                    $color = 'blue';
                } elseif ($row['status'] == 1) {
                    $status = 'On Process';
                    $color = 'yellow';
                } elseif ($row['status'] == 2) {
                    $status = 'Collected';
                    $color = 'green';
                } elseif ($row['status'] == 3) {
                    $status = 'Canceled';
                    $color = 'red';
                }
                ?>
                <tr>
                    <td><?= $sn ?></td>
                    <td><?= $row['title'] ?></td>
                    <td><?= $row['pickup_contact'] ?></td>
                    <td><?= $row['pickup_location'] ?>
                        <a class="circular ui yellow icon button" style="float:right;" data-content="View location in map" onclick="getlocation('<?= $row['lat'] ?>', '<?= $row['lng'] ?>')">
                            <i class="icon marker"></i>
                        </a>
                    </td>
                    <td>
                        <div class="ui <?= $color ?> horizontal label"><?= $status ?> </div>
                    </td>
                    <td>
                        <a href="<?= site_url('dealer_control/pickup_status_change/' . $row['pickupid'] . '/' . $row['user_id'] . '/onprocess') ?>" class="circular ui yellow icon button <? if ($status != 'On Queue') echo 'disabled' ?>" data-content="On Process" onclick="return confirm('Are you sure you want to continue : \'On Process Task\'?')" >
                            <i class="icon road"></i>
                        </a>
                        <a href="<?= site_url('dealer_control/pickup_status_change/' . $row['pickupid'] . '/' . $row['user_id'] . '/collected') ?>" class="circular ui green icon button <? if ($status == 'Collected' || $status == 'Canceled') echo 'disabled' ?>" data-content="Collected" onclick="return confirm('Are you sure you want to continue : \'Collected Task\'?')" >
                            <i class="icon in cart "></i>
                        </a>
                        <a href="<?= site_url('dealer_control/pickup_status_change/' . $row['pickupid'] . '/' . $row['user_id'] . '/canceled') ?>" class="circular ui red icon button <? if ($status == 'Collected' || $status == 'Canceled') echo 'disabled' ?>" data-content="Canceled" onclick="return confirm('Are you sure you want to Cancel the \'Pickup Request\'?')"  >
                            <i class="icon ban"></i>
                        </a>
                    </td>
                    <td><?= $row['added_date'] ?></td>
                    <td>
                        <a href="<?= site_url('dealer_control/pickup_details/' . $row['pickupid'] . '/' . $row['user_id']) ?>" class="circular ui green icon button" data-content="View Pickup Details" >
                            <i class="icon eye"></i>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            </tr></tfoot>
    </table>
    <? $this->load->view('admin/modal'); ?>
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/jquery.dataTables.js"></script>
<script src="<?php echo base_url() ?>/assets/leaflet/leaflet.js"></script> 
<script>


                            var grayscale = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7bjebp/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7bjebp'}),
                                    satelite = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7fjonb/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7fjonb'}),
                                    streets = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lfj3i4nk/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lfj3i4nk'});
                            var map = L.map('map', {
                                center: [27.68824, 85.33567],
                                zoom: 12,
                                maxZoom: 18,
                                layers: [streets]
                            });
                            var baseMaps = {
                                "Streets": streets,
                                "Satelite": satelite,
                                "Grayscale": grayscale

                            };
                            L.control.layers(baseMaps).addTo(map);

                            var marker = null;
                            function getlocation(lat, lng) {
                                $('.ui.small.long.modal.map').modal('show');
                                map.invalidateSize();
                                var zoom = 15;

                                marker = L.marker([lat, lng]).addTo(map);
                                map.setView([lat, lng], zoom);

                            }
                            function remove_marker() {
                                if (marker !== null) {
                                    map.removeLayer(marker);
                                }
                            }

//////////////// /////////////// 

//                            var form = document.getElementById("search");
//                            document.getElementById("search_link").addEventListener("click", function () {
//                                form.submit();
//                                loader();
//                            });
                            ///////
                            $(document).ready(function () {
                                $('#pickup_request').DataTable({
                                    "iDisplayLength": 5,
                                    "aLengthMenu": [[5, 10, 15, 25, 35, 50, 100, -1], [5, 10, 15, 25, 35, 50, 100, "All"]]
                                });
                            });
</script>
<? $this->load->view('footer'); ?>