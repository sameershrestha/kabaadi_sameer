<? $this->load->view('header'); ?>
<div class="container" style="margin-top:135px;">
    <div class="waste-container">
        <div class="ui huge breadcrumb">
            <a class="section">Kabaadi App </a>
            <i class="right chevron icon divider"></i>
            <a href="<?= site_url('dealer_control/category') ?>" class="active section"><i class="trash icon"></i>Waste Category</a>
        </div>
        <div class="ui divider"></div>
        <div class="column"> 
            <!--            <h3 class="ui blue header">
                            <i class="trash icon"></i>
                            <div class="content">
                                Categories
                            </div>
                        </h3>-->
        </div>
        <? if ($this->session->flashdata('failure-msg') != ''): ?>
            <div class="ui negative message">
                <i class="close icon"></i>
                <div class="header">
                    Error!!
                </div>
                <p><?= $this->session->flashdata('failure-msg') ?></p>
            </div>
        <? elseif ($this->session->flashdata('success-msg') != ''): ?>
            <div class="ui success message">
                <i class="close icon"></i>
                <div class="header">
                    Congratulation!!
                </div>
                <p><?= $this->session->flashdata('success-msg') ?></p>
            </div>
        <? endif; ?>
        <table class="ui celled table">
            <thead>
                <tr><th>S.no</th>
                    <th>Waste Category</th>
                    <th>Waste Sub Category</th>
                    <th>Action</th>
                </tr></thead>
            <tbody>
                <?
                $bn = 1;
                foreach ($category as $row): $sn = $bn++;
                    ?>
                    <?
                    $sub = '';
                    foreach ($subcategory as $subcat):
                        ?>
                        <? if ($row['id'] == $subcat['waste_type_id']): ?>
                            <?
                            if ($sub == '') {
                                $sub = $sub . '';
                            } else {
                                $sub = $sub . ' , ';
                            }
                            $sub = $sub . $subcat['sub_type'] . '(Rs. ' . $subcat['rate'] . ')';
                            ?>
                        <? endif; ?>
                    <? endforeach; ?>
                    <tr>
                        <td><?= $sn ?></td>
                        <td><?= $row['type']; ?></td>
                        <td><? if ($sub != ''): ?>
                                <?= $sub ?>
                            <? else: ?>
                                Not Available
                            <? endif; ?>
                        </td>
                        <td>
                            <? if ($sub != ''): ?>
                                <button  class="circular ui yellow icon button" data-content="Edit Sub Category" onclick="edit_subcat('<?= $row['id'] ?>')">
                                    <i class="icon edit"></i>
                                </button>
                            <? endif; ?>
                            <button  class="circular ui green icon button" data-content="Add Sub Category" onclick="add_subcat('<?= $row['id'] ?>')">
                                <i class="icon add"></i>
                            </button>
                        </td>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="ui segment load" style="display: none;" >
        <div class="ui active dimmer" style="position: fixed !important">
            <div class="ui large text loader"></div>
        </div>
        <p></p>
    </div>
    <? $this->load->view('admin/modal'); ?>
</div>
</body>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
<script>
                            $('#add_subcatgory').validate();
                            $('#edit_subcatgory').validate();

                            function checkvalid(tag) {
                                var valid_add = $("#add_subcatgory").valid();
                                var valid_edit = $("#edit_subcatgory").valid();
                                if (valid_add === true && tag === 'add') {
                                    $('.ui.long.modal.add-subcat').modal('hide');
                                    $('.text.loader').html('Adding SubCategory..<br> Please Wait !!');
                                    $('.load').show();
                                } else if (valid_edit === true && tag === 'edit') {
                                    $('.ui.long.modal.edit-subcat').modal('hide');
                                    $('.text.loader').html('Updating SubCategory Details..<br> Please Wait !!');
                                    $('.load').show();
                                }
                            }

                            function clearappend() {
                                $('#subcat_id input').remove();
                                $('#edit_subcat input').remove();
                                $('#edit_rate input').remove();
                            }

                            function clearaddappend() {
                                var i = $("#subcat").find('input').length;
                                for (j = 1; j <= i; j++) {
                                    $('#subcat' + j).remove();
                                    $('#rate' + j).remove();
                                }
                            }

                            function edit_subcat(id) {
                                $('.ui.long.modal.edit-subcat').modal('show');
                                $('#category_id').val(id);
                                $.ajax({
                                    type: 'POST',
                                    url: '<?= base_url("dealer_control/get_dealer_subcat"); ?>',
                                    data: {'id': id},
                                    dataType: 'json',
                                    success: function (retVal) {
                                        var i = 0;
                                        $.each(retVal, function (key, value) {
                                            $('#subcat_id').append('<input  type="text" class="required" name="id' + i + '" value="' + value["id"] + '" readonly="">');
                                            $('#edit_subcat').append('<input  type="text" class="required" name="subcat' + i + '" value="' + value["sub_type"] + '" placeholder="Sub Category">');
                                            $('#edit_rate').append('<input id="lat" class="required" type="text" name="rate' + i + '" value="' + value["rate"] + '" placeholder="Rate">');
                                            i++;
                                        });
                                    }
                                });
                                return true;
                            }

                            function add_subcat(id) {
                                $('.ui.long.modal.add-subcat').modal('show');
                                $('#cat_id').val(id);
                            }

                            function add_more() {
                                var i = $("#subcat").find('input').length;
                                $('#subcat').append('<input id="subcat' + i + '"  type="text" class="required" name="subcat' + i + '" placeholder="Sub Category">');
                                $('#rate').append('<input id="rate' + i + '" class="required" type="text" name="rate' + i + '" placeholder="Rate">');
                            }

                            $('.ui.dropdown').dropdown({
                                on: 'hover'
                            });
                            $('.ui.checkbox').checkbox();
</script>
<? $this->load->view('footer'); ?>