<? if ($uri_segment == 'pickup' || $uri_segment=='pickup image'): ?>
    <div class="ui small long modal map">
        <i class="close icon" onclick="remove_marker()"></i>
        <div class="header">
            View Pickup Location
        </div>
        <div id="map" class="content" style="height:400px;">

        </div>
        <div class="actions">
            <div class="ui close button" onclick="remove_marker()">Cancel</div>
            <div class="ui close green button" onclick="remove_marker()">Ok</div>
        </div>
    </div>
<? elseif ($uri_segment == 'category'): ?>
    <div class="ui long modal edit-subcat">
        <i class="close icon" onclick="clearappend()"></i>
        <div class="header">
            Edit Sub Category
        </div>
        <div class="content">
            <form class="ui form" id="edit_subcatgory" method="post" action="<?= site_url('dealer_control/edit_subcat') ?>">
                <input  type="text" id="category_id" name="catid" placeholder="category id" readonly="">
                <div class="ui form segment" style="max-height: 250px; overflow: auto;">
                    <div class="three fields">
                        <div class="field" id="subcat_id">
                            <center><label>Id</label></center>
                        </div>
                        <div class="field" id="edit_subcat">
                            <center><label>Sub Type</label></center>
                        </div>
                        <div class="field" id="edit_rate">
                            <center><label>Rate</label></center>
                        </div>
                    </div>
                </div>
                <input class="ui orange submit button" type="submit" name="add_subcat" value="Update" onclick="checkvalid('edit')">
            </form>
        </div>
        <div class="actions">
            <div class="ui close button" onclick="clearappend()">Cancel</div>
        </div>
    </div>
    <div class="ui long modal add-subcat">
        <i class="close icon" onclick="clearaddappend()"></i>
        <div class="header">
            Add Sub Category
        </div>
        <div class="content">
            <form class="ui form" id="add_subcatgory" method="post" action="<?= site_url('dealer_control/add_subcat') ?>">
                <center>
                    <a  class="circular ui massive blue icon button" data-content="Add More" onclick="add_more()">
                        <i class="icon plus"></i>
                    </a>
                </center><br>
                <input  type="text" id="cat_id" name="id" placeholder="category id" readonly="">
                <div class="ui form segment" style="max-height: 250px; overflow: auto;">
                    <div class="two fields">
                        <div class="field" id="subcat">
                            <input  type="text" class="required" name="subcat0" placeholder="Sub Category">
                        </div>
                        <div class="field" id="rate">
                            <input id="lat" class="required" type="text" name="rate0" placeholder="Rate">
                        </div>
                    </div>
                </div>
                <input class="ui orange submit button" type="submit" name="add_subcat" value="ADD" onclick="checkvalid('add')">
            </form>
        </div>
        <div class="actions">
            <div class="ui close button" onclick="clearaddappend()">Cancel</div>
        </div>
    </div>
<? endif; ?>
