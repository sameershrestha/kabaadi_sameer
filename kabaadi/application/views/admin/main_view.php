<? $this->load->view('header'); ?>
<div class="container" style="margin-top:125px;">
    <div class="right-fixed-container">
        <div class="ui vertical menu m-menu">
            <!--            <a class="item">
                            <img src="<?= base_url('assets/markers/recycle.png') ?>"/>
                            <labell>Dealers</labell>
                        </a>-->
            <a href="<?= site_url('dealer_control/pickup') ?>" class="item">
                <img src="<?= base_url('assets/markers/pickup_camper.png') ?>"/>
                Pickup Request <div class="ui violet horizontal circular label less-margin"><?= count($users) ?></div>
            </a>
        </div>
    </div>
    <div class="ui grid">
        <div class="map-container" id="map"></div>
    </div>
</div>
</body>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    var baseurl = '<?= base_url() ?>';
    var map = '';
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: new google.maps.LatLng(28.38824, 84.33567),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    if (!map) {
        map = new google.maps.Map(mapCanvas, mapOptions);
    }
    else {
        map.setOptions(mapOptions);
    }
    var infoWindow = new google.maps.InfoWindow();
<? foreach ($users as $row): ?>
        var lat = '<?= $row['lat'] ?>';
        var lng = '<?= $row['lng'] ?>';
        var marker = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            icon: baseurl + 'assets/markers/pickup_camper.png',
            map: map
        });
        google.maps.event.addListener(marker, 'click', function () {
            infoWindow.setContent('<b>Title: ' + '<?= $row['title'] ?>' + '</b><hr>Location: ' + '<?= $row['location'] ?>');
            infoWindow.open(map, this);
        });
<? endforeach; ?>

    $(document).ready(function () {
        $("#map").height($(document).height() - 250);
        $(window).resize(function () {
            $("#map").height($(document).height() - 250);
        });

    });
    $('.ui.dropdown').dropdown({
        on: 'hover'
    });
    $('.ui.checkbox').checkbox();
</script>
<!--<script src="<?php echo base_url() ?> /assets/leaflet/leaflet.js"></script>
    <script>
    ////                                    var koteshwor = L.marker([27.67859, 85.3496]).bindPopup('<b>Koteshwor</b>'),
    ////                                            baneshwor = L.marker([27.68824, 85.33567]).bindPopup('<b>Baneshwor</b>'),
    //////                                            Imadol = L.marker([27.66987, 85.34568]).bindPopup('<b>Imadol</b>');
    ////
    ////                                    var cities = L.layerGroup([koteshwor, baneshwor, Imadol]);
    //
    //
    //                var grayscale = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7bjebp/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7bjebp'}),
    //                satelite = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7fjonb/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7fjonb'}),
    //                streets = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lfj3i4nk/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lfj3i4nk'});
    //
    //                var map = L.map('map', {
    //                    center: [27.68824, 85.33567],
    //                    zoom: 12,
    //                    maxZoom: 18,
    //                    layers: [streets]
    //                });
    //
    //                var baseMaps = {
    //                    "Streets": streets,
    //                    "Satelite": satelite,
    //                    "Grayscale": grayscale
    //
    //                };
    //
    ////                                    var overlayMaps = {
    ////                                         "Cities": cities
    ////                                    };
    //
    //                L.control.layers(baseMaps).addTo(map);
    //
    //<? foreach ($users as $row): ?>
                //                    var lat = '<?= $row['lat'] ?>';
                //                    var lng = '<?= $row['lng'] ?>';
                //                    var marker = L.marker([lat, lng]).addTo(map);
                //                    marker.bindPopup('<b>Title: ' + '<?= $row['title'] ?>' + '</b><hr>Location: ' + '<?= $row['location'] ?>');
                //<? endforeach; ?>
    //
    //                function loc(latlng) {
    ////                                        cities.clearLayers();
    //                    var lat = latlng.substr(0, 7);
    //                    var lng = latlng.substr(9, 7);
    //
    //                    var circle = L.circle([lat, lng], 50, {
    //                        color: 'green',
    //                        fillColor: '#608',
    //                        fillOpacity: 0.5
    //                    }).addTo(map);
    //
    //                    var marker = L.marker([lat, lng]).addTo(map);
    //
    //                }

    </script>-->
<? $this->load->view('footer'); ?>
