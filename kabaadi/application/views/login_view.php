<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/semantic/semantic.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/leaflet/leaflet.css" />
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="<?php echo site_url(); ?>" />
    </head>
    <title>Login</title>
    <body>
        <div class="login-container">
            <div class="inline" style="border-bottom: 1px solid #e7e7e7;">                    
                <a href="<?= site_url('login') ?>">
                    <img class="ui image"  src="<?= base_url("/assets/imgs/logo.png"); ?>" style="display: inline-block;width:90px; padding-left: 10px;">
                    <span style="font-size: 22px; color: #003399;">Kabaadi</span>
                    <!--   <label class="waste-color">Waste App</label> -->
                </a> 
            </div>
            <br>
            <div class="container-fluid">
                <? if ($this->session->flashdata('login_error') != ''): ?>
                    <div class="ui negative message">
                        <i class="close icon"></i>
                        <div class="header">
                            Error!!
                        </div>
                        <p><?= $this->session->flashdata('login_error') ?></p>
                    </div>
                <? elseif ($this->session->flashdata('msg') != ''): ?>
                    <div class="ui success message">
                        <i class="close icon"></i>
                        <div class="header">
                            Congratulation!!
                        </div>
                        <p><?= $this->session->flashdata('msg') ?></p>
                    </div>
                <? endif; ?>
                <form  id="login" method="post"  action="<?php echo site_url('login/process_login/') ?>">
                    <div class="column">
                        <div class="ui form">
                            <div class="required field">
                                <label>Email / Contact Number</label>
                                <div class="ui left icon input">
                                    <input id="email_no" required="" placeholder="Email / Contact Number" name="email" type="text">
                                    <i class="user icon"></i>
                                </div>
                            </div>
                            <div class="required field">
                                <label>Password</label>
                                <div class="ui left icon input">
                                    <input id="password" required="" placeholder="Password" name="password" type="password">
                                    <i class="lock icon"></i>
                                </div>
                            </div>
                            <button type="submit" class="ui labeled icon medium waste-blue submit button"><i class="sign in icon"></i>Login</button>
                        </div>
                    </div>
                    <br>
                </form> 
                <div class="ui horizontal divider">
                    OR
                </div>
                <div class="ui center aligned basic segment">
                    <?php
                    include_once("assets/google/config.php");
                    include_once("assets/google/functions.php");
                    $authUrl = $gClient->createAuthUrl();
                    ?>
                    <? if (isset($authUrl)): ?>
                        <a href="<?= $authUrl ?>" class="ui google plus button">
                            <i class="google plus icon"></i>
                            Signin with Google
                        </a>
                    <? endif; ?>
                </div>
            </div>
            <? $this->load->view('login_modal'); ?>
        </div>
        <br><br>

        <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery-1.10.1.min.js"></script> 
        <script type="text/javascript" src="<?= base_url(); ?>assets/semantic/semantic.js"></script>
        <script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
        <script>
            $('#login').validate();

            $(document).ready(function () {
                $("#email_no").keyup(function (event) {
                    newText = event.target.value;
                    if (newText != "") {
                        $(this).css({"border-left": "2px solid #0099d4"
                        });
                    }
                });

                $("#password").keyup(function (event) {
                    newText1 = event.target.value;
                    if (newText1 != "") {
                        $(this).css({
                            "border-left": "2px solid #0099d4"
                        });
                    }
                });
            });

            $('.message .close').on('click', function () {
                $(this).closest('.message').fadeOut();
            });
        </script>
    </body>
</html>