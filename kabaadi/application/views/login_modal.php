<div class="ui modal signup">
    <i class="close icon"></i>
    <div class="header">
        Sign up process only for mobile users
    </div>
    <div class="content">
        <form  id="sign_up" method="post"  action="<?php echo site_url('login/process_signup/') ?>">
            <div class="column">
                <div class="ui form">
                    <div class="required field">
                        <label>Contact Number</label>
                        <div class="ui left icon input">
                            <input id="contact_number" class="required" title="Contact number is Required" placeholder="Contact Number" name="contact" type="number">
                            <i class="phone icon"></i>
                        </div>
                    </div>
                    <div class="required field">
                        <label>District</label>
                        <select class="required" name="district">
                            <option value="0">District</option>
                            <option value="Achham">Achham</option>
                            <option value="Arghakhanchi">Arghakhanchi</option>
                            <option value="Baglung">Baglung</option>
                            <option value="Baitadi">Baitadi</option>
                            <option value="Bajhang">Bajhang</option>
                            <option value="Bajura">Bajura</option>
                            <option value="Banke">Banke</option>
                            <option value="Bara">Bara</option>
                            <option value="Bardiya">Bardiya</option>
                            <option value="Bhaktapur">Bhaktapur</option>
                            <option value="Bhojpur">Bhojpur</option>
                            <option value="Chitwan">Chitwan</option>
                            <option value="Dadeldhura">Dadeldhura</option>
                            <option value="Dailekh">Dailekh</option>
                            <option value="Dang">Dang</option>
                            <option value="Darchula">Darchula</option>
                            <option value="Dhading">Dhading</option>
                            <option value="Dhankuta">Dhankuta</option>
                            <option value="Dhanusa">Dhanusa</option>
                            <option value="Dolakha">Dolakha</option>
                            <option value="Dolpa">Dolpa</option>
                            <option value="Doti">Doti</option>
                            <option value="Gorkha">Gorkha</option>
                            <option value="Gulmi">Gulmi</option>
                            <option value="Humla">Humla</option>
                            <option value="Ilam">Ilam</option>
                            <option value="Jajarkot">Jajarkot</option>
                            <option value="Jhapa">Jhapa</option>
                            <option value="Jumla">Jumla</option>
                            <option value="Kailali">Kailali</option>
                            <option value="Kalikot">Kalikot</option>
                            <option value="Kanchanpur">Kanchanpur</option>
                            <option value="Kapilbastu">Kapilbastu</option>
                            <option value="Kaski">Kaski</option>
                            <option value="Kathmandu">Kathmandu</option>
                            <option value="Kavrepalanchok">Kavrepalanchok</option>
                            <option value="Khotang">Khotang</option>
                            <option value="Lalitpur">Lalitpur</option>
                            <option value="Lamjung">Lamjung</option>
                            <option value="Mahottari">Mahottari</option>
                            <option value="Makwanpur">Makwanpur</option>
                            <option value="Manang">Manang</option>
                            <option value="Morang">Morang</option>
                            <option value="Mugu">Mugu</option>
                            <option value="Mustang">Mustang</option>
                            <option value="Myagdi">Myagdi</option>
                            <option value="Nawalparasi">Nawalparasi</option>
                            <option value="Nuwakot">Nuwakot</option>
                            <option value="Okhaldhunga">Okhaldhunga</option>
                            <option value="Palpa">Palpa</option>
                            <option value="Panchthar">Panchthar</option>
                            <option value="Parbat">Parbat</option>
                            <option value="Parsa">Parsa</option>
                            <option value="Pyuthan">Pyuthan</option>
                            <option value="Ramechhap">Ramechhap</option>
                            <option value="Rasuwa">Rasuwa</option>
                            <option value="Rautahat">Rautahat</option>
                            <option value="Rolpa">Rolpa</option>
                            <option value="Rukum">Rukum</option>
                            <option value="Rupandehi">Rupandehi</option>
                            <option value="Salyan">Salyan</option>
                            <option value="Sankhuwasabha">Sankhuwasabha</option>
                            <option value="Saptari">Saptari</option>
                            <option value="Sarlahi">Sarlahi</option>
                            <option value="Sindhuli">Sindhuli</option>
                            <option value="Sindhupalchok">Sindhupalchok</option>
                            <option value="Siraha">Siraha</option>
                            <option value="Solukhumbu">Solukhumbu</option>
                            <option value="Sunsari">Sunsari</option>
                            <option value="Surkhet">Surkhet</option>
                            <option value="Syangja">Syangja</option>
                            <option value="Tanahu">Tanahu</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Udayapur">Udayapur</option>
                        </select>
                    </div>
                    <div class="required field">
                        <label>Location</label>
                        <input id="location" placeholder="Location" class="required" title="Location is required" type="text" name="location" >
                    </div>
                    <div class="required field">
                        <label>Map Location <div id="add_location" class="ui submit button" onclick="loadmap()"><i class="marker icon"></i>Set Location</div></label>
                        <div class="two fields">
                            <div class="field">
                                <input id="lat" class="required" title="Lattitude is required" type="text" name="lat" placeholder="Lattitude">
                            </div>
                            <div class="field">
                                <input id="long" class="required" title="Longitude is required" type="text" name="long" placeholder="Longitude">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>Password</label>
                        <div class="ui left icon input">
                            <input id="password" class="required" title="Password is required" required="" placeholder="Password" name="password" type="password">
                            <i class="lock icon"></i>
                        </div>
                    </div>  
                    <div class="required field">
                        <label>Conform Password</label>
                        <div class="ui left icon input">
                            <input id="c_password" class="required" title="Conform Password is required" required="" placeholder="Password" name="c_password" type="password">
                            <i class="lock icon"></i>
                        </div>
                    </div>                   
                </div>
            </div>
            <br>
            <div class="actions">
                <div class="ui close button">Cancel</div>
                <button type="submit" class="ui labeled icon medium waste-blue submit button"><i class="signup icon"></i>Signup</button>
            </div>
        </form> 
    </div>
</div>

<div class="ui small long modal map">
    <i class="close icon"></i>
    <div class="header">
        Select Location
    </div>
    <div id="map" class="content" style="height:400px;">

    </div>
    <div class="actions">
        <div class="ui close button" onclick="clearlatlong()">Cancel</div>
        <div class="ui close green button">Ok</div>
    </div>
</div>