<button  id="delete_<?= $row['id'] ?>" class="circular ui red icon button" data-content="Delete Dealer">
                            <i class="icon remove user"></i>
                        </button>
                        <div class="ui small modal delete_dealer_<?= $row['id'] ?>">
                            <i class="close icon"></i>
                            <div class="header">
                                Delete Dealer
                            </div>
                            <div class="content">
                                <p>Are you sure you want to remove this Dealer ?</p>
                            </div>
                            <div class="actions">
                                <div class="ui negative button">
                                    No
                                </div>
                                <a href="<?= site_url('dealer/delete_selected_dealer/' . $row['id']) ?>"  class="ui positive right labeled icon button">
                                    Yes
                                    <i class="checkmark icon"></i>
                                </a>
                            </div>
                        </div>