		<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href="public/css/style.css" rel='stylesheet' type='text/css' />
		<!--features-->
		<div id="features" class="features">
			<div class="container">
				<div class="section-head text-center">
					<h3><span class="frist"> </span>AMAZING FEATURES<span class="second"> </span></h3>
					<p>Bringing Kabaadi to your doorstep. A handy mobile app which assists users to find the nearest scrap dealer in their location, upload the data about dry waste and get the finest value for the scrap. Selling recyclable scrap is now as easy as ordering pizza over phone.</p>
				</div>
<!--features-grids-->
				<div class="features-grids">
					<div class="col-md-4 features-grid">
						<div class="features-grid-info">
							<div class="col-md-2 features-icon">
								<i class="fa fa-recycle fa-4x"></i> <!-- <span class="glyphicon glyphicon-usd" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Nearby Scrap Dealers:</h4>
								<p>The mobile app’s landing page displays the list of nearest agents who deals with scrap, proving users with unparalleled deals.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						
						<div class="features-grid-info">
							<div class="col-md-2 features-icon">
								<i class="fa fa-truck fa-4x"></i><!-- <span class="glyphicon glyphicon-usd" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Pickup request: </h4>
								<p>User can send pickup request to their selected dealers. Select a dealer, select waste categories, enter waste details and send your request. It’s easy, simple and quick. </p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>

					<div class="col-md-4 features-grid">
						<div class="features-grid-info">
							<div class="col-md-2 features-icon1">
								<i class="fa fa-check-square fa-4x"></i><!-- <span class="glyphicon glyphicon-send" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Pickup Status: </h4>
								<p>See the status of your pickup requests from the app itself. Our dedicated team always look after every user’s pickup requests and set the real time status about it. </p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="features-grid-info">
							<div class="col-md-2 features-icon1">
								<i class="fa fa-usd fa-4x"></i><!-- <span class="glyphicon glyphicon-usd" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Best Prices:</h4>
								<p>Browse through the scrap dealers with best price and deals for scrap. It all happens just by a touch on your smartphone.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div>

					<div class="col-md-4 features-grid">
						<div class="features-grid-info">
							<div class="col-md-2 features-icon1">
								<i class="fa fa-trash fa-4x"></i><!-- <span class="glyphicon glyphicon-trash" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Waste Management</h4>
								<p>Perfect wat to manage dry waste and make your contribution to cleaner environment. And yes, you also make some money in all these process.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
						<div class="features-grid-info">
							<div class="col-md-2 features-icon">
								<i class="fa fa-tasks fa-4x"></i><!-- <span class="glyphicon glyphicon-usd" aria-hidden="true"></span></span> -->
							</div>
							<div class="col-md-10 features-info">
								<h4>Track your own Activity</h4>
								<p>Track history of your activities i.e. the deals to check the value generated from scrap.</p>
							</div>
							<div class="clearfix"> </div>
						</div>
					</div><!---end-features-grid-->

					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
				<!----//features-grids--->