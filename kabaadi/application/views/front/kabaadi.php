<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>Kabaadi at your door step</title>

        <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
        <link rel="icon" type="image/png" href="public/img/favicon.ico" sizes="32x32">

        <link href="public/img/kabaadi-logo.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">

        <!-- Bootstrap Core CSS -->
        <link href="public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <link href="public/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Merriweather:400,300,300italic,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>

        <!-- Plugin CSS -->
        <link href="public/vendor/magnific-popup/magnific-popup.css" rel="stylesheet">

        <!-- Theme CSS -->
        <link href="public/css/creative.min.css" rel="stylesheet">
        
        <link href='public/css/custom.css' rel='stylesheet' type='text/css'>
        <!-- <link href="public/css/style.css" rel='stylesheet' type='text/css' /> -->
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" > -->


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
            <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body id="page-top">
        <nav id="mainNav" class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header"> 

                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse">
                        <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                    </button>
                    <img src="public/img/kabaadi-logo.png">
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li>
                            <a class="page-scroll" href="#page-top">Home</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#about">About</a>
                        </li>
                        
                        <li>
                            <a class="page-scroll" href="#features">Features</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#services">Services</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#portfolio">Portfolio</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#faq">FAQ's</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#team">Team</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#contact">Contact</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#">Login</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            </div>
            <!-- /.container-fluid -->
        </nav>

        <header>
            <div class="header-content" id="home">
                <div class="header-content-inner" >
                    <h1 id="homeHeading">CASH OUT YOUR TRASH!</h1>
                    <hr>
                    <p>Help Yourself, "Recycle, Reuse, Reduce" & REQUEST @ Kabaadi.org</p>
                    <a href="#about" class="btn btn-primary btn-xl page-scroll">Find Out More</a>
                </div>
            </div>
        </header>

        <section class="bg-primary" id="about">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-lg-offset-2 text-center">
                        <h2 class="section-heading">About Us!</h2>
                        <hr class="light">
                        <p >Kabaadi.org is a doorstep free service recycling based company. We provide a door-to-door service that allows institutions, organizations, and households to dispose of their recyclables through a friendly, reliable, and fair process. </P>
                        <p class="text-faded">We provide the service to collect junk/scrap material from your location & a friendly environment  to sell more, earn more and recycle more.</p>
                        <a href="index.php/pages/about" class="page-scroll btn btn-default btn-xl sr-button">Read More!</a>
                    </div>
                </div>
            </div>
        </section>

        <br>

        <section class="no-padding" id="features">
           <?php $this->load->view('front/features'); ?>
        </section>

        <section  id="services">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 text-center">
                        <h2 class="section-heading">At Your Service</h2>
                        <hr class="primary">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-home text-primary sr-icons"></i>
                            <h3>Home</h3>
                            <p class="text-muted">Scrap from Households.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-institution text-primary sr-icons"></i>
                            <h3>Institutions</h3>
                            <p class="text-muted">Scrap from Institutions: Schools & Colleges</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-newspaper-o text-primary sr-icons"></i>
                            <h3>Hotels & Restaurants</h3>
                            <p class="text-muted">Scrap from Hotels/Restaurants/Canteen.</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-shopping-cart text-primary sr-icons"></i>
                            <h3>Mall/Shop</h3>
                            <p class="text-muted">Scrap from Mall/Shop like plastics & papers</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-money text-primary sr-icons"></i>
                            <h3>Banks</h3>
                            <p class="text-muted">Scrap from Banks like papers</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-group text-primary sr-icons"></i>
                            <h3>Corporate Offices</h3>
                            <p class="text-muted">Scrap from Offices like papers</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-plus-square-o text-primary sr-icons"></i>
                            <h3>Hospitals</h3>
                            <p class="text-muted">Scrap from Hospitals</p>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 text-center">
                        <div class="service-box">
                            <i class="fa fa-4x fa-building text-primary sr-icons"></i>
                            <h3>Organizations</h3>
                            <p class="text-muted">Scrap from Organizations</p>
                        </div>
                    </div>
                   
                </div>
            </div>
        </section>

        <section class="no-padding" id="portfolio">
            <div class="container-fluid">
                <div class="row no-gutter popup-gallery">
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/1.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/1.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/2.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/2.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/3.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/3.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/4.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/4.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/5.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/5.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <a href="public/img/portfolio/fullsize/6.jpg" class="portfolio-box">
                            <img src="public/img/portfolio/thumbnails/6.jpg" class="img-responsive" alt="">
                            <div class="portfolio-box-caption">
                                <div class="portfolio-box-caption-content">
                                    <div class="project-category text-faded">
                                        Category
                                    </div>
                                    <div class="project-name">
                                        Project Name
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <aside class="bg-dark" style="padding:15px;">
                <div class="container text-center">
                    <div class="call-to-action">
                        <h2>Try Dealing With Us Once!</h2>
                        <a href="#" class="btn btn-default btn-xl sr-button">See More!</a>
                    </div>
                </div>
            </aside>
        </section>

        <section class="no-padding" id="faq">
            <?php $this->load->view('front/faq'); ?>
            <div class="container text-center">
                   <div class="call-to-action">
                    <a href="index.php/pages/faq" class="btn btn-default btn-xl sr-button">Read More!</a>
                </div>
            </div>
        </section>

        <section class="no-padding"id="team">
           <?php $this->load->view('front/team'); ?>
        </section>

        <section class="no-padding" id="contact">
           <?php $this->load->view('front/contact'); ?>
        </section>

        <section id="footer" style="padding-bottom: 0px;">
            <footer class="text-center">
                <div class="footer-above">
                    <div class="container">
                        <div class="row">
                            <div class="footer-col col-md-4">
                                <h3>Location</h3>
                                <p>Lal Durbar, Durbarmarg, 
                                    <br>Kathmandu, Nepal</p>
                            </div>

                            <div class="footer-col col-md-4">
                                <h3>Around the Web</h3>
                                <ul class="list-inline">
                                    <li>
                                        <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" class="btn-social btn-outline"><i class="fa fa-fw fa-youtube"></i></a>
                                    </li>
                                </ul>
                            </div>

                            <div class="footer-col col-md-4">
                                <ul class="list-inline quicklinks">
                                    <li><a href="#">Privacy Policy</a>
                                    </li>
                                    <li><a href="#">Terms of Use</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-below">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                                Copyright &copy; Kabaadi 2016
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </section>

        <!-- jQuery -->
        <script src="public/vendor/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="public/vendor/bootstrap/js/bootstrap.min.js"></script>

        <!-- Plugin JavaScript -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
        <script src="public/vendor/scrollreveal/scrollreveal.min.js"></script>
        <script src="public/vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

        <!-- Theme JavaScript -->
        <script src="public/js/creative.min.js"></script>
    </body>
</html>