<link href="../public/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../public/css/full-slider.css" rel="stylesheet">
<style>

    body{
  padding-top:50px;
  background: #fff;
}
.flip {
  -webkit-perspective: 800;
perspective: 800;

    position: relative;
 text-align: center;
}
.flip .card.flipped {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
.flip .card {

  height: 100%;
  -webkit-transform-style: preserve-3d;
  -webkit-transition: 0.5s;
    transform-style: preserve-3d;
    transition: 0.5s;
}
.flip .card .face {

  -webkit-backface-visibility: hidden ;
    backface-visibility: hidden ;
  z-index: 2;
   
}
.flip .card .front {
  position: absolute;
   width: 100%;
  z-index: 1;

}
.flip .card .back {
  -webkit-transform: rotatey(-180deg);
    transform: rotatey(-180deg);
}
  .inner{margin:0px !important;}
 </style>

<div class="container">
	<div class="row">
		<div class="col-md-6 col-lg-8">
			<!-- Full Page Image Background Carousel Header -->
		    <header id="myCarousel" class="carousel slide">
		        <!-- Indicators -->
		        <ol class="carousel-indicators">
		            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
		            <li data-target="#myCarousel" data-slide-to="1"></li>
		            <li data-target="#myCarousel" data-slide-to="2"></li>
		        </ol>

		        <!-- Wrapper for Slides -->
		        <div class="carousel-inner">
		            <div class="item active">
		                <!-- Set the first background image using inline CSS below. -->
		                <div class="fill" style="background-image:url('public/img/portfolio/thumbnails/1');"></div>
		                <div class="carousel-caption">
		                    <h2>Caption 1</h2>
		                </div>
		            </div>
		            <div class="item">
		                <!-- Set the second background image using inline CSS below. -->
		                <div class="fill" style="background-image:url('public/img/portfolio/thumbnails/2');"></div>
		                <div class="carousel-caption">
		                    <h2>Caption 2</h2>
		                </div>
		            </div>
		            <div class="item">
		                <!-- Set the third background image using inline CSS below. -->
		                <div class="fill" style="background-image:url('public/img/portfolio/thumbnails/3');"></div>
		                <div class="carousel-caption">
		                    <h2>Caption 3</h2>
		                </div>
		            </div>
		        </div>

		        <!-- Controls -->
		        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
		            <span class="icon-prev"></span>
		        </a>
		        <a class="right carousel-control" href="#myCarousel" data-slide="next">
		            <span class="icon-next"></span>
		        </a>
		    </header>
		</div>

		<div class="col-md-6 col-lg-4">
			<h2 >Cash our your trash now!</h2>
			<form>
			  <div class="form-group">
			    <select id="sourcetype" class="form-control" name="sourcetype" required="required" aria-describedby="ngoError">
                  <option value="Select Source Type" selected="">Select Your Type</option>
                  <option value="households">Households</option>
                  <option value="apartments">Apartments</option>
                  <option value="hotelsandrestaurants">Hotels & Restaurants</option>
                  <option value="institutions">Institutions</option>
                </select>
			  </div>

			  <div class="row">
				  <div class="col-md-6">
					  <div class="form-group">
					    <input type="text" class="form-control" id="formGroupExampleInput2" required="required" placeholder="Pickup Address">
					  </div>
				  </div>

				  <div class="col-md-6">
					  <div class="form-group">
					    <select id="pickup_date" name="pickup_date" class="form-control" aria-describedby="dateError">
		                  <option value="Pickup Date" selected="">Pickup Date</option>
		                  <option value="Friday, 19 August 2016">Tomorrow, 19 August 2016</option>
		                  <option value="Saturday, 20 August 2016">Saturday, 20 August 2016</option>
		                  <option value="Sunday, 21 August 2016">Sunday, 21 August 2016</option>
		                  <option value="Monday, 22 August 2016">Monday, 22 August 2016</option>
		                  <option value="Tuesday, 23 August 2016">Tuesday, 23 August 2016</option>
		                  <option value="Wednesday, 24 August 2016">Wednesday, 24 August 2016</option>
		                  <option value="Thursday, 25 August 2016">Thursday, 25 August 2016</option>
		                </select>
					  </div>
				  </div>
			  </div>

			  <div class="row">
				  <div class="col-md-6">
						<div class="form-group">
						  	<div class="checkbox">
							 	<label><input type="checkbox" value="">Plastic</label>
								<label><input type="checkbox" value="">Paper</label>
								<label><input type="checkbox" value="">Glasses</label>
								<label><input type="checkbox" value="">Metal</label>
							</div>
						</div>
				  </div>

				  <div class="col-md-6">
					  <div class="form-group">
					    <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Approx. Weight">
					  </div>
				  </div>
			  </div>

			  <div class="row">
			  	<div class="row-md-6">
			  		
			  	</div>
			  	<div class="col-md-6">
			  		<button type="submit" class="btn btn-success">Sell</button>
			  	</div>
			  </div>
			</form>
		</div>
	</div>
</div>



	<div class="container">
	  <div class="row">
	    <div class="col-sm-3">
	      <div class="flip" ontouchstart="this.classList.toggle('hover');">
	        <div class="card"> 
	          <div class="face front"> 
	            <div class="well well-sm inner"> Paper</div>
	             <!-- front content -->
	                      <img src="../assets/img/icon-glass-157.png" data-pin-nopin="true">
	          </div> 
	          <div class="face back"> 
	            <div class="well well-sm inner"> Back paper</div>
	             <!-- back content -->
	                      <img src="../assets/img/flipped-glass-157.png" data-pin-nopin="true">
	          </div>
	        </div>	 
	      </div>
	    </div>
	    <div class="col-sm-3">
	      <div class="flip">
	        <div class="card"> 
	          <div class="face front"> 
	            <div class="well well-sm inner"> Front</div>
	          </div> 
	          <div class="face back"> 
	            <div class="well well-sm inner"> Back</div>
	          </div>
	        </div>	 
	      </div>
	    </div>
	    <div class="col-sm-3">
	      <div class="flip">
	        <div class="card"> 
	          <div class="face front"> 
	            <div class="well well-sm inner"> Front</div>
	          </div> 
	          <div class="face back"> 
	            <div class="well well-sm inner"> Back</div>
	          </div>
	        </div>	 
	      </div>
	    </div>
	    <div class="col-sm-3">
	      <div class="flip">
	        <div class="card"> 
	          <div class="face front"> 
	            <div class="well well-sm inner"> Front</div>
	          </div> 
	          <div class="face back"> 
	            <div class="well well-sm inner"> Back</div>
	          </div>
	        </div>	 
	      </div>
	    </div>
	  </div>
	</div>	 

	<!-- jQuery -->
    <script src="../public/js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../public/js/bootstrap.min.js"></script>

    <!-- Script to Activate the Carousel -->
    <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })

    $('.flip').bind('touchstart', function() {
        $(this).find('.card').toggleClass('flipped');

    });
    $('.flip').click(function(){
        $(this).find('.card').toggleClass('flipped');

    });
    /*bind('touchstart', function() {});*/

    </script>
