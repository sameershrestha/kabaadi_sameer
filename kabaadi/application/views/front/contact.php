        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"> -->
        <link href='public/css/custom.css' rel='stylesheet' type='text/css'>
        <style type="text/css">
            .map-responsive{
                overflow:hidden;
                padding-bottom:60%;
                position: relative;
                padding-bottom: 56.25%; /* 16:9 */
                height: 0;
            }
            .map-responsive iframe{
                left:0;
                top:0;
                height:100%;
                width:100%;
                position:absolute;
            }

        </style>
    <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6">

                    <h2>Send Us a Message!</h2>

                    <form id="contact-form" method="post" action="<?php echo site_url('index.php/pages/send_mail/') ?>" role="form">
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label for="form_name">Name *</label>
                                        <input id="form_name" type="text" name="name" class="form-control" placeholder="Please enter your Name *" required="required" data-error="Name is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="form_email">Email *</label>
                                        <input id="form_email" type="email" name="email" class="form-control" placeholder="Please enter your email *" required="required" data-error="Valid email is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="form_phone">Phone</label>
                                        <input id="form_phone" type="tel" name="phone" class="form-control" placeholder="Please enter your phone">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="form_subject">Subject *</label>
                                        <input id="form_subject" type="subject" name="subject" class="form-control" placeholder="Please enter your subject *" required="required" data-error="Subject is required.">
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <div class="form-group">
                                        <label for="form_message">Message *</label>
                                        <textarea id="form_message" name="message" class="form-control" placeholder="Message for me *" rows="4" required="required" data-error="Please,leave us a message."></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div>

                                    <input type="submit" class="btn btn-success btn-send" value="Send message">

                                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                                </div>
                            </div>
                    </form>
                </div><!-- /.8 -->

                <div class="col-lg-6 col-md-6 col-sm-6 ">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <div style="text-align:left;" class="">
                            <h2 >Our Office</h2>
                    
                            <h5><span class="glyphicon glyphicon-home" aria-hidden="true"></span> Blue Waste To Value, Yak & Yeti, Durbarmarg</h5>
                    
                            <h5><span class="glyphicon glyphicon-phone-alt" aria-hidden="true"></span> 98000000000</h5>
                    
                            <h5><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> info.kabaadi@gmail.com</h5>
                    
                            <h5><span class="glyphicon glyphicon-briefcase" aria-hidden="true"></span> Business hours</h5>
                    
                            <h5><span class="glyphicon glyphicon-clock" aria-hidden="true"></span> Sunday - Friday</h5>
                            <h5><strong> 10am-5pm</strong></h5>
                        </div>
                    </div>
               
                    <div class="col-lg-12 col-md-12 col-sm-12">
                            <div id="googleMap" class="map-responsive" style="border: 1px solid black"></div>
                    </div>
                </div>
                <!-- </div> -->
            </div> <!-- /.row-->
    </div>

    <script src='https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyA694pm08n2FwW7E0JtZUBAKpRFryImUIE'></script>

    <script>
        var lat = 27.7167;
        var longt = 85.31669999999997;
        /*27.7167,85.31669999999997*/
        var myCenter = new google.maps.LatLng(lat,longt);

        function initialize(){
            var mapProp = {
              center:myCenter,
              zoom:14,
              mapTypeId:google.maps.MapTypeId.ROADMAP
        };
        infowindow = new google.maps.InfoWindow({content:'<strong>Kabaadi.org</strong><br>Yak & Yeti, Durbarmarg<br> Kathmandu<br>'});

        var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

        var marker=new google.maps.Marker({
          position:myCenter,
          });

        marker.setMap(map);
        infowindow.open(map,marker);
        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
        
        <script src="public/js/validator.js"></script>
        <script src="public/js/contact.js"></script>
