        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='public/css/custom.css' rel='stylesheet' type='text/css'>

        <style type="text/css">
        .rc-drk-green {
            color: #5c821a;
        }

        h3, .h3 {
            font-size: 24px;
        }
        </style>

        <div class="container">

            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    
                        <h1>Frequently Asked Questions </h1>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <h3 class="rc-drk-green">Q1 Why choose kabaadi.org?</h3>
                                    </div>
                                    <p>Kabaadi provides a doorstep free, professional and reliable service for all your unused, recyclable stuffs. Request and Scheduling a pickup using Kabaadi is easy and reliable. You get paid for selling your unused stuffs.</p>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <h3 class="rc-drk-green">Q2 How to request a pickup??</h3>
                                    </div>
                                    <p>You can schedule a pickup through our website or mobile apps. You have to login to our apps or site and request the pickups.</p>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <h3 class="rc-drk-green">Q3 Are there any additional/hidden costs/charges of Pickups?</h3>
                                    </div>
                                    <p>Absolutely Not. There’s no hidden charges, instead get handsomely rewarded for letting us serve you.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <h3 class="rc-drk-green">Q4 What about refunds/returns?</h3>
                                    </div>
                                    <p>There is no return or refund of items once picked up by Kabaadi.</p>
                                </div>
                            </div>
                            
                        </div>
                </div><!-- /.8 -->
            </div> <!-- /.row-->
        </div> <!-- /.container-->

