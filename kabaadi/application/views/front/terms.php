<html>
    <head>
        <title>Terms & Conditions</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='custom.css' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-lg-offset-2">

                    <h1>Terms and Conditions </h1>

                        <div class="messages">Terms and Conditions</div>

                        <div class="controls">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="">
                                        <label>Related to your Information.</label>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque facere sint architecto nisi ratione saepe! Libero ipsum expedita, enim facere, accusamus nihil odit officiis dignissimos porro necessitatibus ipsa architecto deleniti!</p>
                                </div>

                                <div class="col-md-12">
                                    <div class="">
                                        <label>Related to our service.</label>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque facere sint architecto nisi ratione saepe! Libero ipsum expedita, enim facere, accusamus nihil odit officiis dignissimos porro necessitatibus ipsa architecto deleniti!</p>
                                </div>

                                <div class="col-md-12">
                                    <div class="">
                                        <label>Related to Pricing and Action</label>
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Doloremque facere sint architecto nisi ratione saepe! Libero ipsum expedita, enim facere, accusamus nihil odit officiis dignissimos porro necessitatibus ipsa architecto deleniti!</p>
                                </div>
                                
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <p class="text-muted"><strong>*</strong> Last Updated on 9 Aug 2016.</p>
                                </div>
                            </div>
                        </div>
                </div><!-- /.8 -->
            </div> <!-- /.row-->
        </div> <!-- /.container-->

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    </body>
</html>
