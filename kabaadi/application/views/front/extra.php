<html>
    <head>
        <title>Team Members</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href="public/css/style.css" rel='stylesheet' type='text/css' />
    </head>
    <body>
<!----team---->
	<div id="about" class="team">
		<div class="container">
			<div class="section-head text-center">
				<h3><span class="first"> </span>PEOPLE BEHIND THE APP<span class="second"> </span></h3>
				<p>Meet our incredible team. A team of diversified personalities from different fields of society.</p>
			</div>
		</div>
		
		<!----team-members---->
		<div class="team-members">
			<div class="container">
				<div class="col-md-3 team-member">
					<div class="team-member-info">
						<img class="member-pic" src="public/img/team-member4.jpg" title="name">
						<h5><a href="#">Varun Saraf</a></h5>
						<span>Social Entrepreneur </span>
						<label class="team-member-caption text-center">
							<p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
							<ul>
								<li><a class="t-twitter" href="#"><i class="fa fa-facebook"><span> </span></a></li> <!-- line 626 style.css -->
								<li><a class="t-facebook" href="#"><span> </span></a></li>
								<li><a class="t-googleplus" href="#"><span> </span></a></li>
								<div class="clearfix"> </div>
							</ul>
						</label>
					</div>
				</div><!-- end-team-member-->

				<div class="col-md-3 team-member">
					<div class="team-member-info">
						<img class="member-pic" src="public/img/team-member1.jpg" title="name">
						<h5><a href="#">Mukut Pareek</a></h5>
						<span>Project Manager</span>
						<label class="team-member-caption text-center">
							<p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
							<ul>
								<li><a class="t-twitter" href="#"><span> </span></a></li>
								<li><a class="t-facebook" href="#"><span> </span></a></li>
								<li><a class="t-googleplus" href="#"><span> </span></a></li>
								<div class="clearfix"> </div>
							</ul>
						</label>
					</div>
				</div><!-- end-team-member -->

				<div class="col-md-3 team-member">
					<div class="team-member-info">
						<img class="member-pic" src="public/img/team-member2.jpg" title="name">
						<h5><a href="#">Nabin Bikash Maharjan</a></h5>
						<span>Waste Management Expert</span>
						<label class="team-member-caption text-center">
							<p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
							<ul>
								<li><a class="t-twitter" href="#"><span> </span></a></li>
								<li><a class="t-facebook" href="#"><span> </span></a></li>
								<li><a class="t-googleplus" href="#"><span> </span></a></li>
								<div class="clearfix"> </div>
							</ul>
						</label>
					</div>
				</div><!--- end-team-member -->

				<div class="col-md-3 team-member">
					<div class="team-member-info">
						<img class="member-pic" src="public/img/team-member3.jpg" title="name">
						<h5><a href="#">Sneha Singhal</a></h5>
						<span>Business Developer</span>
						<label class="team-member-caption text-center">
							<p>sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem.</p>
							<ul>
								<li><a class="t-twitter" href="#"><span> </span></a></li>
								<li><a class="t-facebook" href="#"><span> </span></a></li>
								<li><a class="t-googleplus" href="#"><span> </span></a></li>
								<div class="clearfix"> </div>
							</ul>
						</label>
					</div>
				</div><!--- end-team-member -->

				<div class="clearfix"> </div>
			</div>
				<div class="clearfix"> </div>
			</div>
	</div>
	<script src="public/js/jquery.min.js"></script>
</body>
</html>