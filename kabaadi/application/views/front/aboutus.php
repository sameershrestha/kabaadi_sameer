        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link href='https://fonts.googleapis.com/css?family=Lato:300,400,700' rel='stylesheet' type='text/css'>
        <link href='public/css/custom.css' rel='stylesheet' type='text/css'>
        <style type="text/css">
            p{
                text-align: justify;
            }
        </style>

        <div class="container">

            <div class="row">

                <div class="col-lg-8 col-lg-offset-2">

                    <h1>About Us </h1>

                        <div class="controls">

                            <h2>About</h2>
                            <p>Kabaadi.org is a doorstep free service recycling based company. We provide a door-to-door service that allows institutions, organizations, and households to dispose of their recyclables through a friendly, reliable, and fair process. We provide the service to collect junk/scrap material from your location. We provide environment friendly service to sell more, earn more and recycle more.</p>

                            <p>Our dedicated team work on your request and deliver you the best service and solution for your request.
                            We clean your house, your scrap, your waste and your world and also help you make some money out of them.</p>

                            <br>
                            
                            <h3>Mission & Vision</h3>
                            <p>We bring together normal people, vendors, recycling agents and other stakeholders to build a thriving green ecosystem in Nepal.</p>  
                            
                            <br>

                            <h3>Service we provide</h3>
                            <p>Free door-to-door step service to collect your 'kabaad' trash and help you make some money. Sell more earn more!
                            Free Pick Up Service, We are creating a GO GREEN environment.
                            Help Yourself, "Recycle Reuse Reduce" & REQUEST with kabaadi.org
                            Sell Newspaper, Scrap Materials etc. </p>
                        </div>   
                </div><!-- /.8 -->
            </div> <!-- /.row-->
        </div> <!-- /.container-->

        <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>