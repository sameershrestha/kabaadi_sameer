<? if ($uri_segment == 'location' || $uri_segment == 'category'): ?>
    <div class="ui small long modal map">
        <i class="close icon" onclick="remove_marker()"></i>
        <div class="header">
            View Buyers Location
        </div>
        <div id="map" class="content" style="height:400px;">

        </div>
        <div class="actions">
            <div class="ui close button" onclick="remove_marker()">Cancel</div>
            <div class="ui close green button" onclick="remove_marker()">Ok</div>
        </div>
    </div>
<? endif; ?>
