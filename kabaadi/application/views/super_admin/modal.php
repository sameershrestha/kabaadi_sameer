<? if ($uri == 'dealers'): ?>
    <!-- ADD NEW DEALER -->
    <div class="ui long modal add_dealer">
        <i class="close icon"></i>
        <div class="header">
            Create New Dealer
        </div>
        <div class="content">
            <form enctype = "multipart/form-data" id="add_dealers" autocomplete="off"  method="post"  action="<?php echo site_url('dealer/add_admin/') ?>">
                <div class="ui form segment">
                    <div class="required field">
                        <label>Dealer's Name</label>
                        <input placeholder="Name" class="required" type="text" name="name" required="">
                    </div>
                    <div class="required field">
                        <label>Email</label>
                        <input placeholder="Email" class="required" type="email" name="email" required="">
                    </div>
                    <div class="required field">
                        <label>Password</label>
                        <input style="display:none" type="password" name="fakepasswordremembered"/>
                        <input placeholder="Password" class="required" type="password" name="password" required="">                        
                    </div>
                    <div class="required field">
                        <label>District</label>
                        <select class="required" name="district">
                            <option value="0">District</option>
                            <option value="Achham">Achham</option>
                            <option value="Arghakhanchi">Arghakhanchi</option>
                            <option value="Baglung">Baglung</option>
                            <option value="Baitadi">Baitadi</option>
                            <option value="Bajhang">Bajhang</option>
                            <option value="Bajura">Bajura</option>
                            <option value="Banke">Banke</option>
                            <option value="Bara">Bara</option>
                            <option value="Bardiya">Bardiya</option>
                            <option value="Bhaktapur">Bhaktapur</option>
                            <option value="Bhojpur">Bhojpur</option>
                            <option value="Chitwan">Chitwan</option>
                            <option value="Dadeldhura">Dadeldhura</option>
                            <option value="Dailekh">Dailekh</option>
                            <option value="Dang">Dang</option>
                            <option value="Darchula">Darchula</option>
                            <option value="Dhading">Dhading</option>
                            <option value="Dhankuta">Dhankuta</option>
                            <option value="Dhanusa">Dhanusa</option>
                            <option value="Dolakha">Dolakha</option>
                            <option value="Dolpa">Dolpa</option>
                            <option value="Doti">Doti</option>
                            <option value="Gorkha">Gorkha</option>
                            <option value="Gulmi">Gulmi</option>
                            <option value="Humla">Humla</option>
                            <option value="Ilam">Ilam</option>
                            <option value="Jajarkot">Jajarkot</option>
                            <option value="Jhapa">Jhapa</option>
                            <option value="Jumla">Jumla</option>
                            <option value="Kailali">Kailali</option>
                            <option value="Kalikot">Kalikot</option>
                            <option value="Kanchanpur">Kanchanpur</option>
                            <option value="Kapilbastu">Kapilbastu</option>
                            <option value="Kaski">Kaski</option>
                            <option value="Kathmandu">Kathmandu</option>
                            <option value="Kavrepalanchok">Kavrepalanchok</option>
                            <option value="Khotang">Khotang</option>
                            <option value="Lalitpur">Lalitpur</option>
                            <option value="Lamjung">Lamjung</option>
                            <option value="Mahottari">Mahottari</option>
                            <option value="Makwanpur">Makwanpur</option>
                            <option value="Manang">Manang</option>
                            <option value="Morang">Morang</option>
                            <option value="Mugu">Mugu</option>
                            <option value="Mustang">Mustang</option>
                            <option value="Myagdi">Myagdi</option>
                            <option value="Nawalparasi">Nawalparasi</option>
                            <option value="Nuwakot">Nuwakot</option>
                            <option value="Okhaldhunga">Okhaldhunga</option>
                            <option value="Palpa">Palpa</option>
                            <option value="Panchthar">Panchthar</option>
                            <option value="Parbat">Parbat</option>
                            <option value="Parsa">Parsa</option>
                            <option value="Pyuthan">Pyuthan</option>
                            <option value="Ramechhap">Ramechhap</option>
                            <option value="Rasuwa">Rasuwa</option>
                            <option value="Rautahat">Rautahat</option>
                            <option value="Rolpa">Rolpa</option>
                            <option value="Rukum">Rukum</option>
                            <option value="Rupandehi">Rupandehi</option>
                            <option value="Salyan">Salyan</option>
                            <option value="Sankhuwasabha">Sankhuwasabha</option>
                            <option value="Saptari">Saptari</option>
                            <option value="Sarlahi">Sarlahi</option>
                            <option value="Sindhuli">Sindhuli</option>
                            <option value="Sindhupalchok">Sindhupalchok</option>
                            <option value="Siraha">Siraha</option>
                            <option value="Solukhumbu">Solukhumbu</option>
                            <option value="Sunsari">Sunsari</option>
                            <option value="Surkhet">Surkhet</option>
                            <option value="Syangja">Syangja</option>
                            <option value="Tanahu">Tanahu</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Udayapur">Udayapur</option>
                        </select>
                    </div>
                    <div class="required field">
                        <label>Office Location</label>
                        <input placeholder="Office Location" class="required" type="text" name="location" required="">
                    </div>
                    <div class="required field">
                        <label>Map Location <div id="add_location" class="ui submit button" onclick="loadmap('add')"><i class="marker icon"></i>Set Location</div></label>
                        <div class="two fields">
                            <div class="field">
                                <input id="lat" type="text" name="lat" placeholder="Lattitude">
                            </div>
                            <div class="field">
                                <input id="long" type="text" name="long" placeholder="Longitude">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>Waste Type</label>
                        <div class="ui <?= $inword; ?> column center aligned stackable divided grid segment">
                            <? foreach ($category as $cat): ?>
                                <div class="column">
                                    <div class="ui toggle checkbox">
                                        <input type="checkbox" name="type_<?= strtolower($cat['type']) ?>">
                                        <label><?= ucwords(str_replace('_', ' ', $cat['type'])); ?></label>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                        <!--                    <input placeholder="Waste Type" class="required" type="text" name="type" required="">-->
                    </div>
                    <div class="required field">
                        <label>Contact No</label>
                        <input placeholder="Contact" class="required" type="text" name="contact" required="">
                    </div>
                    <div class="ui right labeled left icon input">
                        <input class="required"  id="uploadFile" placeholder="Choose File" disabled="disabled" />
                        <div class="ui tag teal label fileUpload btn-fileupload">
                            <span>Upload Dealer's logo</span>
                            <input id="uploadBtn" type="file" name = "userfile" class="upload" />
                        </div>
                    </div><br><br>
                    <input class="ui orange submit button"  type="submit" name="add_admin" value="Add Dealer" onclick="checkvalid('add')">
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui close button">Cancel</div>
        </div>
    </div>
    <!-- END OF ADD NEW DEALER -->

    <!-- EDIT DEALER --> 
    <div class="ui long modal edit_dealer">
        <i class="close icon"></i>
        <div class="header">
            Edit Dealer
        </div>
        <div class="content">
            <form enctype = "multipart/form-data" autocomplete="off" id="edit_dealer"  method="post"  action="<?php echo site_url('dealer/edit_admin/') ?>">
                <div class="ui form segment">
                    <div class="required field">
                        <label>Id</label>
                        <input placeholder="Id" id="id" class="required" type="text" name="id" readonly="">
                    </div>
                    <div class="required field">
                        <label>Dealer's Name</label>
                        <input placeholder="Name" id="name" class="required" type="text" name="name" required="">
                    </div>
                    <div class="required field">
                        <label>Email</label>
                        <input placeholder="Email" id="email" class="required" type="email" name="email" required="">
                    </div>
                    <div class="field">
                        <label>New Password</label>
                        <input style="display:none" type="password" name="fakepasswordremembered"/>
                        <input placeholder="Password"  type="password" name="password">                        
                    </div>
                    <div class="required field">
                        <label>District</label>
                        <select class="required" id='edit_district' name="district">
                            <option value="0">District</option>
                            <option value="Achham">Achham</option>
                            <option value="Arghakhanchi">Arghakhanchi</option>
                            <option value="Baglung">Baglung</option>
                            <option value="Baitadi">Baitadi</option>
                            <option value="Bajhang">Bajhang</option>
                            <option value="Bajura">Bajura</option>
                            <option value="Banke">Banke</option>
                            <option value="Bara">Bara</option>
                            <option value="Bardiya">Bardiya</option>
                            <option value="Bhaktapur">Bhaktapur</option>
                            <option value="Bhojpur">Bhojpur</option>
                            <option value="Chitwan">Chitwan</option>
                            <option value="Dadeldhura">Dadeldhura</option>
                            <option value="Dailekh">Dailekh</option>
                            <option value="Dang">Dang</option>
                            <option value="Darchula">Darchula</option>
                            <option value="Dhading">Dhading</option>
                            <option value="Dhankuta">Dhankuta</option>
                            <option value="Dhanusa">Dhanusa</option>
                            <option value="Dolakha">Dolakha</option>
                            <option value="Dolpa">Dolpa</option>
                            <option value="Doti">Doti</option>
                            <option value="Gorkha">Gorkha</option>
                            <option value="Gulmi">Gulmi</option>
                            <option value="Humla">Humla</option>
                            <option value="Ilam">Ilam</option>
                            <option value="Jajarkot">Jajarkot</option>
                            <option value="Jhapa">Jhapa</option>
                            <option value="Jumla">Jumla</option>
                            <option value="Kailali">Kailali</option>
                            <option value="Kalikot">Kalikot</option>
                            <option value="Kanchanpur">Kanchanpur</option>
                            <option value="Kapilbastu">Kapilbastu</option>
                            <option value="Kaski">Kaski</option>
                            <option value="Kathmandu">Kathmandu</option>
                            <option value="Kavrepalanchok">Kavrepalanchok</option>
                            <option value="Khotang">Khotang</option>
                            <option value="Lalitpur">Lalitpur</option>
                            <option value="Lamjung">Lamjung</option>
                            <option value="Mahottari">Mahottari</option>
                            <option value="Makwanpur">Makwanpur</option>
                            <option value="Manang">Manang</option>
                            <option value="Morang">Morang</option>
                            <option value="Mugu">Mugu</option>
                            <option value="Mustang">Mustang</option>
                            <option value="Myagdi">Myagdi</option>
                            <option value="Nawalparasi">Nawalparasi</option>
                            <option value="Nuwakot">Nuwakot</option>
                            <option value="Okhaldhunga">Okhaldhunga</option>
                            <option value="Palpa">Palpa</option>
                            <option value="Panchthar">Panchthar</option>
                            <option value="Parbat">Parbat</option>
                            <option value="Parsa">Parsa</option>
                            <option value="Pyuthan">Pyuthan</option>
                            <option value="Ramechhap">Ramechhap</option>
                            <option value="Rasuwa">Rasuwa</option>
                            <option value="Rautahat">Rautahat</option>
                            <option value="Rolpa">Rolpa</option>
                            <option value="Rukum">Rukum</option>
                            <option value="Rupandehi">Rupandehi</option>
                            <option value="Salyan">Salyan</option>
                            <option value="Sankhuwasabha">Sankhuwasabha</option>
                            <option value="Saptari">Saptari</option>
                            <option value="Sarlahi">Sarlahi</option>
                            <option value="Sindhuli">Sindhuli</option>
                            <option value="Sindhupalchok">Sindhupalchok</option>
                            <option value="Siraha">Siraha</option>
                            <option value="Solukhumbu">Solukhumbu</option>
                            <option value="Sunsari">Sunsari</option>
                            <option value="Surkhet">Surkhet</option>
                            <option value="Syangja">Syangja</option>
                            <option value="Tanahu">Tanahu</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Taplejung">Taplejung</option>
                            <option value="Udayapur">Udayapur</option>
                        </select>
                    </div>
                    <div class="required field">
                        <label>Office Location</label>
                        <input id="location" placeholder="Office Location" class="required" type="text" name="location" required="">
                    </div>
                    <div class="required field">
                        <label>Map Location <div id="edit_location" class="ui submit button" onclick="loadmap('edit')"><i class="marker icon"></i>Set Location</div></label>
                        <div class="two fields">
                            <div class="field">
                                <input id="edit_lat" type="text" name="lat" placeholder="Lattitude">
                            </div>
                            <div class="field">
                                <input id="edit_long" type="text" name="long" placeholder="Longitude">
                            </div>
                        </div>
                    </div>
                    <div class="required field">
                        <label>Waste Type</label>
                        <div class="ui <?= $inword; ?> column center aligned stackable divided grid segment">
                            <? foreach ($category as $cat): ?>
                                <div class="column">
                                    <div id="<?= strtolower($cat['type']) ?>" class="ui toggle checkbox">
                                        <input type="checkbox" name="type_<?= strtolower($cat['type']) ?>">
                                        <label><?= ucwords(str_replace('_', ' ', $cat['type'])); ?></label>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                        <!--                    <input placeholder="Waste Type" class="required" type="text" name="type" required="">-->
                    </div>
                    <div class="required field">
                        <label>Contact No</label>
                        <input placeholder="Contact" id="contact" class="required" type="text" name="contact" required="">
                    </div>
                    <div class="ui right labeled left icon input">
                        <input class="required"  id="edituploadFile" placeholder="Choose File" disabled="disabled" />
                        <div class="ui tag teal label fileUpload btn-fileupload">
                            <span>Upload Dealer's logo</span>
                            <input id="edituploadBtn" type="file" name = "userfile" class="upload" />
                        </div>
                    </div><br><br>
                    <input class="ui orange submit button" type="submit" name="edit_admin" value="Update Dealer" onclick="checkvalid('edit')">
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui close button">Cancel</div>
        </div>
    </div>
    <!-- END OF EDIT THE DEALER -->

    <div class="ui small long modal map">
        <i class="close icon"></i>
        <div class="header">
            Select Location
        </div>
        <div id="map" class="content" style="height:400px;">

        </div>
        <div class="actions">
            <div class="ui close button" onclick="clearlatlong()">Cancel</div>
            <div class="ui close green button">Ok</div>
        </div>
    </div>
<? elseif ($uri == 'category'): ?>
    <div class="ui long modal category">
        <i class="close icon"></i>
        <div class="header">
            Add Category
        </div>
        <div class="content">
            <form id="cat_add" enctype = "multipart/form-data" method="post" action="<?= site_url('waste_category/add') ?>">
                <div class="ui right labeled left icon input">
                    <input   id="uploadFile" placeholder="Choose File" disabled="disabled" />
                    <div class="ui tag teal label fileUpload btn-fileupload">
                        <span>Upload Image</span>
                        <input id="uploadBtn" type="file" name = "userfile" class="upload required" />
                    </div>
                </div><br><br>
                <div class="ui right action input">                    
                    <input class="required" type="text" name="category" placeholder="Add Category">
                    <button class="ui teal labeled  icon button" onclick="checkvalid()">
                        <i class="unordered list icon"></i>
                        Add Category
                    </button>
                </div>
            </form>
        </div>
        <div class="actions">
            <div class="ui close button">Cancel</div>
        </div>
    </div>

    <div class="ui long modal add-subcat">
        <i class="close icon"></i>
        <div class="header">
            Add Sub Category
        </div>
        <div class="content">
            <form class="ui form" id="subcatgory" method="post" action="<?= site_url('waste_category/add_subcat') ?>">
                <center>
                    <a  class="circular ui massive blue icon button" data-content="Add More" onclick="add_more()">
                        <i class="icon plus"></i>
                    </a>
                </center><br>
                <input  type="text" id="cat_id" name="id" placeholder="category id" readonly="">
                <div class="ui form segment">
                    <div class="two fields">
                        <div class="field" id="subcat">
                            <input  type="text" class="required" name="subcat0" placeholder="Sub Category">
                        </div>
                        <div class="field" id="rate">
                            <input id="lat" class="required" type="text" name="rate0" placeholder="Rate">
                        </div>
                    </div>
                </div>
                <input class="ui orange submit button" type="submit" name="add_subcat" value="ADD">
            </form>
        </div>
        <div class="actions">
            <div class="ui close button">Cancel</div>
        </div>
    </div>
<? elseif ($uri == 'pickup'): ?>
    <div class="ui small long modal map">
        <i class="close icon"></i>
        <div class="header">
            View Pickup Location
        </div>
        <div id="map" class="content" style="height:400px;">

        </div>
        <div class="actions">
            <div class="ui close button" onclick="remove_marker()">Cancel</div>
            <div class="ui close green button" onclick="remove_marker()">Ok</div>
        </div>
    </div>
<? elseif ($uri == 'users'): ?>
    <div class="ui long modal msg">
        <i class="close icon"></i>
        <div class="header">
            Add Message
        </div>
        <div  class="content">
            <form class="ui form" id="msg" method="post" action="<?= site_url('notification/send_message') ?>">
                <div class="field">
                    <label>User ID</label>
                    <input type="text" readonly="" name="uid" id="uid">
                </div>
                <div class="field">
                    <label>Subject <span class="error">(Max Length 10)</span></label>
                    <input type="text" class="required" maxlength="10" name="subject" id="subject" placeholder="Subject">
                </div>
                <div class="field">
                    <label>Message <span class="error">(Max Length 250)</span></label>
                    <textarea type="text" class="required" maxlength="250" name="message" id="message" placeholder="Message" ></textarea>
                </div>
                <button class="ui yellow labeled  icon button" type="submit" onclick="checkvalid()">
                    <i class="send icon"></i>
                    Send
                </button>
            </form>
        </div>
        <div class="actions">
            <div class="ui close button" >Cancel</div>
        </div>
    </div>
<? endif; ?>
