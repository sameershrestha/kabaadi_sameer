<? $this->load->view('header'); ?>
<div class="container" style="margin-top:135px;">
    <div class="waste-container">
        <div class="ui huge breadcrumb">
            <a class="section">Kabaadi App </a>
            <i class="right chevron icon divider"></i>
            <a href="<?= site_url('waste_category') ?>" class="active section"><i class="trash icon"></i>Waste Category</a>
        </div>
        <button style="float: right; vertical-align: middle;" id="add_category" class="circular ui massive blue icon button" data-content="Add Category">
            <i class="icon plus"></i>
        </button>
        <div class="ui divider"></div>
        <? if ($this->session->flashdata('failure-msg') != ''): ?>
            <div class="ui negative message">
                <i class="close icon"></i>
                <div class="header">
                    Error!!
                </div>
                <p><?= $this->session->flashdata('failure-msg') ?></p>
            </div>
        <? elseif ($this->session->flashdata('success-msg') != ''): ?>
            <div class="ui success message">
                <i class="close icon"></i>
                <div class="header">
                    Congratulation!!
                </div>
                <p><?= $this->session->flashdata('success-msg') ?></p>
            </div>
        <? endif; ?>
        <table class="ui celled table">
            <thead>
                <tr><th>S.no</th>
                    <th>Waste Category</th>
                    <th>Category Images</th>
                    <th>Action</th>
                </tr></thead>
            <tbody>
                <?
                $bn = 1;
                foreach ($category as $row): $sn = $bn++;
                    ?>
                    <tr>
                        <td><?= $sn ?></td>
                        <td><?= ucwords(str_replace('_', ' ', $row['type'])); ?></td>
                        <td><img width="50px" src="<?= $row['img_url']?>" /></td>
                        <td>
                            <button  id="delete_<?= $row['id'] ?>" class="circular ui red icon button" data-content="Delete Category">
                                <i class="icon remove"></i>
                            </button>
                            <div class="ui small modal delete_cat_<?= $row['id'] ?>">
                                <i class="close icon"></i>
                                <div class="header">
                                    Delete Category
                                </div>
                                <div class="content">
                                    <p>Are you sure you want to remove this Category ?</p>
                                </div>
                                <div class="actions">
                                    <div class="ui negative button">
                                        No
                                    </div>
                                    <a href="<?= site_url('waste_category/delete_selected_category/' . $row['id'] . '/' . $row['type']) ?>"  class="ui positive right labeled icon button">
                                        Yes
                                        <i class="checkmark icon"></i>
                                    </a>
                                </div>
                            </div>
                        </td>
                    </tr>
                <? endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="ui segment load" style="display: none;" >
        <div class="ui active dimmer" style="position: fixed !important">
            <div class="ui large text loader">Adding Category... <br> Please Wait !!</div>
        </div>
        <p></p>
    </div>
    <? $this->load->view('super_admin/modal'); ?>
</div>
</body>

<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
<script>
    $('#cat_add').validate();
    function checkvalid() {
        var valid_cat = $('#cat_add').valid();
        if (valid_cat == true) {
            $('.ui.long.modal.category').modal('hide');
            $('.load').show();
        }
    }

    $('#add_category').click(function () {
        $('.ui.long.modal.category').modal('setting', 'transition', 'vertical flip').modal('show');
    });
<? foreach ($category as $row): ?>
        $('#delete_' + '<?= $row['id'] ?>').click(function () {
            $('.ui.small.modal.delete_cat_' + '<?= $row['id'] ?>').modal('show');
        });
<? endforeach; ?>

    $('.ui.dropdown').dropdown({
        on: 'hover'
    });
    $('.ui.checkbox').checkbox();

    document.getElementById("uploadBtn").onchange = function () {
        document.getElementById("uploadFile").value = this.value;
        $('#uploadFile').css('background', '#dff0d8');
        $('#uploadFile').css('color', '#468847');
        $('#uploadFile').css('border-color', '#d6e9c6');
    };
</script>
<? $this->load->view('footer'); ?>
