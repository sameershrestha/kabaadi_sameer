<? $this->load->view('header'); ?>
<div class="waste-container">
    <div class="ui huge breadcrumb">
        <a class="section">Kabaadi App </a>
        <i class="right chevron icon divider"></i>
        <a href="<?= site_url('notification') ?>" class="active section"><i class="mail icon"></i>Notice</a>
    </div>
    <div class="ui divider"></div>
    <? if ($this->session->flashdata('failure-msg') != ''): ?>
        <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
                Error!!
            </div>
            <p><?= $this->session->flashdata('failure-msg') ?></p>
        </div>
    <? elseif ($this->session->flashdata('success-msg') != ''): ?>
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Congratulation!!
            </div>
            <p><?= $this->session->flashdata('success-msg') ?></p>
        </div>
    <? endif; ?>
    <div class="ui three column grid margin-top">
    </div>
    <table class="ui celled table" id="message-table">
        <thead>
            <tr><th>SN</th>
<!--                <th>Name</th>
                -->                <th>Username</th>
                <th>Subject</th>                
                <th>Notice</th>
                <th>Added Date</th>
            </tr></thead>
        <tbody>
            <?
            $s = 1;
            foreach ($messages as $row): $sn = $s++;
                ?>
                <tr>
                    <td><?= $sn ?></td>
    <!--                    <td><?= $row['name'] ?></td>           -->  
                    <td><?= $row['email'] ?></td>
                    <td><?= $row['subject'] ?></td>
                    <td><?= $row['message'] ?></td>
                    <td><?= $row['added_date'] ?></td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            </tr></tfoot>
    </table>
    <? $this->load->view('super_admin/modal'); ?>
</div>

<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
        $('#message-table').DataTable({
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 15, 25, 35, 50, 100, -1], [5, 10, 15, 25, 35, 50, 100, "All"]]
        });
    });
</script>
<? $this->load->view('footer'); ?>