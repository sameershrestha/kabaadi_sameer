<? $this->load->view('header'); ?>
<div class="container" style="margin-top:135px;">
    <div class="waste-container">
        <div class="ui huge breadcrumb">
            <a class="section">Kabaadi App </a>
            <i class="right chevron icon divider"></i>
            <? if ($uri == 'users'): ?>
                <a href="<?= site_url('users') ?>" class="section">Users</a>
                <i class="right chevron icon divider"></i>
                <a href="<?= site_url('users/users_pickup_details/' . $uid) ?>" class="active section">Users Pickup Details</a>
            <? elseif ($uri == 'pickup'): ?>
                <a href="<?= site_url('pickup') ?>" class="section">Pickup Request</a>
                <i class="right chevron icon divider"></i>
                <a href="<?= site_url('pickup/pickup_details/' . $pid . '/' . $uid) ?>" class="active section">Pickup Details View</a>
            <? endif; ?>
        </div>
        <div class="ui circular segment" style="float:right; margin-top: 25px;">
            <h3 class="ui header">
                Total Sales
                <div class="sub header">Rs. <?= $total ?></div>
            </h3>
        </div>
        <div class="ui divider"></div>
        <br><br>
        <div class="ui styled fluid accordion"> 
            <? foreach ($activity_date as $date): ?>
                <?php
                if ($date['status'] == 0) {
                    $status = 'On Queue';
                    $color = 'blue';
                } elseif ($date['status'] == 1) {
                    $status = 'On Process';
                    $color = 'yellow';
                } elseif ($date['status'] == 2) {
                    $status = 'Collected';
                    $color = 'green';
                } elseif ($date['status'] == 3) {
                    $status = 'Canceled';
                    $color = 'red';
                }
                ?>
                <div class="title active">
                    <i class="dropdown icon"></i>
                    <?= $date['title'] ?>
                    <div class="ui default label">
                        Requested Date-Time:
                        <div class="detail"><?= $date['added_date'] ?></div>
                    </div>
                    <div class="ui <?= $color ?> label">
                        Status:
                        <div class="detail"><?= $status ?></div>
                    </div>
                </div>
                <div class="content active">
                    <div class="row">                                          
                        <table class="ui celled table">
                            <thead class='yellow'>
                                <tr>
                                    <th>SN</th>
                                    <th>Waste Products</th>                                    
                                    <th>Rate (Rs.) per kg/piece</th>
                                    <th>Qty</th>
                                    <th>Amt (Rs.)</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?
                                $sn = 1;
                                $total_sales = 0;
                                foreach ($activity as $row):
                                    ?>
                                    <?
                                    if ($date['id'] == $row['activity_id']):
                                        $dn = $sn++;
                                        $total_sales += $row['total_price'];
                                        ?>
                                        <tr>
                                            <td><?= $dn ?></td>
                                            <td><?= $row['sub_type']; ?></td>
                                            <td><?= $row['rate']; ?></td>
                                            <td><?= $row['quantity']; ?></td>
                                            <td><?= $row['total_price']; ?></td>
                                        </tr>
                                    <? endif; ?>
                                <? endforeach; ?>
                                <tr class='yellow'>
                                    <td></td>
                                    <td>Total Sales</td>
                                    <td></td>
                                    <td></td>
                                    <td><?= $total_sales ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>                
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
</body>
<script>
    $('.ui.accordion').accordion();
</script>
<? $this->load->view('footer'); ?>
