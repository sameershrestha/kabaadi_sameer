<!DOCTYPE html>
<html lang="en">
    <head>
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/semantic/semantic.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/map.css">
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/leaflet/leaflet.css" />
        <script src="<?php echo base_url() ?>/assets/js/jquery-1.10.1.min.js"></script>
        <script src="<?php echo base_url() ?>/assets/semantic/semantic.js"></script>
        <meta charset="utf-8">
        <meta content="IE=edge" http-equiv="X-UA-Compatible">
        <meta content="width=device-width, initial-scale=1.0" name="viewport">
        <meta content="" name="description">
        <meta content="" name="author">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <base href="<?php echo site_url(); ?>" />
    </head>
    <title>WelCome to Map</title>
    <body>
        <div class="container">
            <div class="left-fixed-container">
                <div class="ui horizontal menu">
                    <a class="item" href="<?= site_url('main') ?>">
                        <i class="home icon"></i>
                        Home
                    </a>
                    <a class="<? if ($uri == 'map') echo'active' ?>  item" href="<?= site_url('map') ?>">
                        Map
                    </a>
                    <a class="item" onclick='locate()'>
                        My Location
                    </a>
                    <div class="item">
                        <div class="ui selection dropdown">
                            <input name="gender" type="hidden">
                            <div class="default text">Places</div>
                            <i class="dropdown icon"></i>
                            <div class="menu">
                                <option class="item"  value="27.67859,85.3496" onclick="loc(this.value);" >Koteshwor</option>
                                <option class="item"  value="27.68824,85.33567" onclick="loc(this.value);">New Baneshwor</option>
                            </div>
                        </div>
                    </div>
                    <div class="item">
                        <div class="ui input"><input placeholder="Search..." type="text"></div>
                    </div>
                </div>
            </div>
            <div class="ui grid">
                <div class="map-container" id="map"></div>
            </div>
        </div>
    </body>
    <script src="<?php echo base_url() ?>/assets/leaflet/leaflet.js"></script>
    <script>
                                    var koteshwor = L.marker([27.67859, 85.3496]).bindPopup('<b>Koteshwor</b>'),
                                            baneshwor = L.marker([27.68824, 85.33567]).bindPopup('<b>Baneshwor</b>'),
                                            Imadol = L.marker([27.66987, 85.34568]).bindPopup('<b>Imadol</b>');

                                    var cities = L.layerGroup([koteshwor, baneshwor, Imadol]);


                                    var grayscale = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7bjebp/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7bjebp'}),
                                            satelite = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7fjonb/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7fjonb'}),
                                            streets = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lfj3i4nk/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lfj3i4nk'});

                                    var map = L.map('map', {
                                        center: [27.68824, 85.33567],
                                        zoom: 12,
                                        maxZoom: 18,
                                        layers: [streets, cities]
                                    });

                                    var baseMaps = {
                                        "Streets": streets,
                                        "Satelite": satelite,
                                        "Grayscale": grayscale

                                    };

                                    var overlayMaps = {
                                        "Cities": cities
                                    };

                                    L.control.layers(baseMaps, overlayMaps).addTo(map);




                                    function loc(latlng) {
                                        cities.clearLayers();
                                        var lat = latlng.substr(0, 7);
                                        var lng = latlng.substr(9, 7);

                                        var circle = L.circle([lat, lng], 50, {
                                            color: 'green',
                                            fillColor: '#608',
                                            fillOpacity: 0.5
                                        }).addTo(map);

                                        var marker = L.marker([lat, lng]).addTo(map);

                                    }
                                    map.on('click', function (e) {
                                        L.marker([e.latlng.lat, e.latlng.lng]).addTo(map);
                                        alert("Lat, Lon : " + e.latlng.lat + ", " + e.latlng.lng);
                                    });

//var popup = L.popup();
//                                function onMapClick(e) {
//                                    popup
//                                            .setLatLng(e.latlng)
//                                            .setContent("You clicked the map at " + e.latlng.toString())
//                                            .openOn(map);
//                                }
//
//                                map.on('click', onMapClick);
//
//                                function onmarker(e) {
//                                    popup
//                                            .setLatLng(e.latlng)
//                                            .setContent("<b><span class='glyphicon glyphicon-home'></span>  Imadol</b> at " + e.latlng.toString());
//                                    this.bindPopup(popup);
//                                }
//                                marker.on('click', onmarker);

    </script>
    <script>
        $('.ui.dropdown').dropdown({
            on: 'hover'
        });
        $('.ui.checkbox').checkbox();
    </script>
</body>
</html>