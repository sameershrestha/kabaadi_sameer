<? $this->load->view('header'); ?>
<div class="waste-container">
    <div class="ui huge breadcrumb">
        <a class="section">Kabaadi App </a>
        <i class="right chevron icon divider"></i>
        <a href="<?= site_url('dealer') ?>" class="active section"><i class="users icon"></i>Dealers</a>
    </div>
    <button style="float: right; vertical-align: middle;" id="add" class="circular ui massive blue icon button" data-content="Add Dealer">
        <i class="icon add user"></i>
    </button>
    <div class="ui divider"></div>
    <? if ($this->session->flashdata('failure-msg') != ''): ?>
        <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
                Error!!
            </div>
            <p><?= $this->session->flashdata('failure-msg') ?></p>
        </div>
    <? elseif ($this->session->flashdata('success-msg') != ''): ?>
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Congratulation!!
            </div>
            <p><?= $this->session->flashdata('success-msg') ?></p>
        </div>
    <? endif; ?>
    <div class="ui three column grid margin-top">
        <!--        <div class="column"> 
                    <h3 class="ui blue header">
                        <i class="users icon"></i>
                        <div class="content">
                            Admin Users
                        </div>
                    </h3>
                </div>
                <div class="column"></div>
                <div class="column">
                    <form id="search" method="post" action="<?php echo site_url('dealer/search/') ?> ">
                        <div class="ui  search">
                            <div class="ui icon input">
                                <input class="prompt required" required="" name="search" placeholder="Search products..." type="text">
                                <i id="search_link" class="inverted blue circular search link icon"></i>
                            </div>
                            <div class="results"></div>
                        </div>
                    </form>
                </div>-->
    </div>
    <table class="ui celled table" id="dealer_view">
        <thead>
            <tr><th>SN</th>
                <th>Name</th>
                <th>Email</th>
                <th>Logo</th>
                <th>Office Location</th>                
                <th>Waste Type</th>
                <th>Contact No.</th>
                <th>Action</th>
            </tr></thead>
        <tbody>
            <?
            $s = 1;
            foreach ($admin as $row): $sn = $s++;
                ?>
                <tr>
                    <td><?= $sn ?></td>
                    <td><?= $row['name'] ?></td>
                    <td><?= $row['email'] ?></td>
                    <td><image  src="<?= $row['img_url'] ?>" style="width:50px;height: auto;"/></td>
                    <td><?= $row['office_location'] ?></td>
                    <td><?= $row['category'] ?></td>
                    <td><?= $row['contact'] ?></td>
                    <td>
                        <button  id="edit_<?= $row['id'] ?>" class="circular ui yellow icon button" data-content="Edit Dealer" onclick="getdealer(<?= $row['id'] ?>)">
                            <i class="icon edit"></i>
                        </button>
                        <button  id="delete_<?= $row['id'] ?>" class="circular ui red icon button" data-content="Delete Dealer">
                            <i class="icon remove user"></i>
                        </button>
                        <div class="ui small modal delete_dealer_<?= $row['id'] ?>">
                            <i class="close icon"></i>
                            <div class="header">
                                Delete Dealer
                            </div>
                            <div class="content">
                                <p>Are you sure you want to remove this Dealer ?</p>
                            </div>
                            <div class="actions">
                                <div class="ui negative button">
                                    No
                                </div>
                                <a href="<?= site_url('dealer/delete_selected_dealer/' . $row['id']) ?>"  class="ui positive right labeled icon button">
                                    Yes
                                    <i class="checkmark icon"></i>
                                </a>
                            </div>
                        </div>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
    <div class="ui segment load" style="display: none;" >
        <div class="ui active dimmer" style="position: fixed !important">
            <div class="ui large text loader"></div>
        </div>
        <p></p>
    </div>
    <? $this->load->view('super_admin/modal'); ?>
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/jquery.dataTables.js"></script>
<!--<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/dataTables.responsive.js"></script>-->
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
                        $('#add_dealers').validate();
                        $('#edit_dealer').validate();


                        function checkvalid(tag) {
                            var valid_add = $("#add_dealers").valid();
                            var valid_edit = $("#edit_dealer").valid();
                            if (valid_add === true && tag === 'add') {
                                $('.ui.long.modal.add_dealer').modal('hide');
                                $('.text.loader').html('Adding Dealer..<br> Please Wait !!');
                                $('.load').show();
                            } else if (valid_edit === true && tag === 'edit') {
                                $('.ui.long.modal.edit_dealer').modal('hide');
                                $('.text.loader').html('Updating Dealer Details..<br> Please Wait !!');
                                $('.load').show();
                            }
                        }

                        $('#add').click(function () {
                            $('.ui.long.modal.add_dealer').modal('setting', 'transition', 'vertical flip').modal('show');
                        });
                        $('#add_location').click(function () {
                            $('.ui.long.modal.map').modal({allowMultiple: true}).modal('show');
                        });
                        $('#edit_location').click(function () {
                            $('.ui.long.modal.map').modal({allowMultiple: true}).modal('show');
                        });

                        var baseurl = '<?= base_url() ?>';
                        var map = '';
                        var markers = [];
                        function loadmap(action) {
                            setMapOnAll(null);
                            var mapCanvas = document.getElementById('map');
                            var mapOptions = {
                                center: new google.maps.LatLng(27.68824, 85.33567),
                                zoom: 12,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            }
                            if (!map) {
                                map = new google.maps.Map(mapCanvas, mapOptions);
                            }
                            else {
                                map.setOptions(mapOptions);
                            }
                            var geocoder = new google.maps.Geocoder;
                            var infowindow = new google.maps.InfoWindow;
                            google.maps.event.addListener(map, "click", function (e) {
                                var latlng = e.latLng;
                                geocodeLatLng(geocoder, map, infowindow, latlng, action);
                            });

                        }
                        function setMapOnAll(map) {
                            for (var i = 0; i < markers.length; i++) {
                                markers[i].setMap(map);
                            }
                        }

                        function geocodeLatLng(geocoder, map, infowindow, latlng, action) {
                            geocoder.geocode({'location': latlng}, function (results, status) {
                                if (status === google.maps.GeocoderStatus.OK) {
                                    if (results[1]) {
//                            map.setZoom(11);
                                        $('#location').val(results[1].formatted_address);
                                        if (markers) {
                                            setMapOnAll(null);
                                        }
                                        var marker = new google.maps.Marker({
                                            draggable: true,
                                            position: latlng,
                                            icon: baseurl + 'assets/markers/recycle.png',
                                            map: map
                                        });
                                        if (action === 'add') {
                                            $('#lat').val(latlng.lat);
                                            $('#long').val(latlng.lng);
                                        }
                                        else if (action === 'edit')
                                        {
                                            $('#edit_lat').val(latlng.lat);
                                            $('#edit_long').val(latlng.lng);
                                        }
                                        google.maps.event.addListener(marker, 'dragend', function (event) {
                                            latlng = event.latLng;
                                            if (action === 'add') {
                                                $('#lat').val(event.latLng.lat());
                                                $('#long').val(event.latLng.lng());
                                            }
                                            else if (action === 'edit')
                                            {
                                                $('#edit_lat').val(event.latLng.lat());
                                                $('#edit_long').val(event.latLng.lng());
                                            }
                                            geocodeLatLng(geocoder, map, infowindow, latlng, action);
                                        });
                                        markers.push(marker);
                                        infowindow.setContent(results[1].formatted_address);
                                        infowindow.open(map, marker);
                                    } else {
                                        window.alert('No results found');
                                    }
                                } else {
                                    window.alert('Geocoder failed due to: ' + status);
                                }
                            });

//                                setTimeout(function () {
//                                    map.invalidateSize();
//                                }, 10);
//                                var grayscale = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7bjebp/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7bjebp'}),
//                                        satelite = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lg7fjonb/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lg7fjonb'}),
//                                        streets = L.tileLayer('https://{s}.tiles.mapbox.com/v4/arpsth143.lfj3i4nk/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiYXJwc3RoMTQzIiwiYSI6InA3S3U3MFkifQ.Pwq7EMEtv7zRqpqqa-I5TQ', {id: 'arpsth143.lfj3i4nk'});
//                                var map = L.map('map', {
//                                    center: [27.68824, 85.33567],
//                                    zoom: 12,
//                                    maxZoom: 18,
//                                    layers: [streets]
//                                });
//                                var baseMaps = {
//                                    "Streets": streets,
//                                    "Satelite": satelite,
//                                    "Grayscale": grayscale
//
//                                };
//                                L.control.layers(baseMaps).addTo(map);
//                                if (marker)
//                                {
//                                    map.removeLayer(marker);
//                                }
//                                var marker;
//                                map.on('click', function (e) {
//                                    if (marker)
//                                    {
//                                        map.removeLayer(marker);
//                                    }
//                                    marker = L.marker([e.latlng.lat, e.latlng.lng], {draggable: true});
//                                    map.addLayer(marker);
//                                    if (action === 'add') {
//                                        $('#lat').val(e.latlng.lat);
//                                        $('#long').val(e.latlng.lng);
//                                    }
//                                    else if (action === 'edit')
//                                    {
//                                        $('#edit_lat').val(e.latlng.lat);
//                                        $('#edit_long').val(e.latlng.lng);
//                                    }
//                                });
                        }
                        function clearlatlong() {
                            $('#lat').val('');
                            $('#long').val('');
                        }
                        /////////////////////////////////////////
<? foreach ($admin as $row): ?>
                            $('#edit_' + '<?= $row['id'] ?>').click(function () {
                                $('.ui.long.modal.edit_dealer').modal('setting', 'transition', 'horizontal flip').modal('show');
                            });
                            $('#delete_' + '<?= $row['id'] ?>').click(function () {
                                $('.ui.small.modal.delete_dealer_' + '<?= $row['id'] ?>').modal('show');
                            });
<? endforeach; ?>

/////////////////////////////////////


//                        var form = document.getElementById("search");
//                        document.getElementById("search_link").addEventListener("click", function () {
//                            form.submit();
//                            loader();
//                        });

///////////////////////////////
                        function getdealer(id) {
                            $.ajax({
                                type: 'POST',
                                url: '<?= base_url("dealer/get_user_details"); ?>',
                                data: {'id': id},
                                dataType: 'json',
                                success: function (retVal) {
                                    $("#id").val(retVal[0]['user_id']);
                                    $("#name").val(retVal[0]['name']);
                                    $("#email").val(retVal[0]['email']);
                                    $("#edit_district").val(retVal[0]['district']);
                                    $("#location").val(retVal[0]['office_location']);
                                    $("#edit_lat").val(retVal[0]['lat']);
                                    $("#edit_long").val(retVal[0]['longitude']);
                                    $("#contact").val(retVal[0]['contact']);
                                    check(retVal);
                                }
                            });
                            return true;
                        }

                        function check(val) {
<? foreach ($category as $cat): ?>
                                $('#<?= strtolower($cat['type']) ?>').checkbox('uncheck');
                                if (val[0]['type_<?= strtolower($cat['type']) ?>'] == 'on') {
                                    $('#<?= strtolower($cat['type']) ?>').checkbox('check');
                                }
<? endforeach; ?>
                        }
//////////////////////////////

                        document.getElementById("uploadBtn").onchange = function () {
                            document.getElementById("uploadFile").value = this.value;
                            $('#uploadFile').css('background', '#dff0d8');
                            $('#uploadFile').css('color', '#468847');
                            $('#uploadFile').css('border-color', '#d6e9c6');
                        };
                        document.getElementById("edituploadBtn").onchange = function () {
                            document.getElementById("edituploadFile").value = this.value;
                            $('#edituploadFile').css('background', '#dff0d8');
                            $('#edituploadFile').css('color', '#468847');
                            $('#edituploadFile').css('border-color', '#d6e9c6');
                        };

                        ///////
                        $(document).ready(function () {
                            $('#dealer_view').DataTable({
//                                    "responsive": true,
                                "iDisplayLength": 5,
                                "aLengthMenu": [[5, 10, 15, 25, 35, 50, 100, -1], [5, 10, 15, 25, 35, 50, 100, "All"]]
                            });
                        });

</script>
<? $this->load->view('footer'); ?>