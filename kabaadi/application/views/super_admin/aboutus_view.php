<? $this->load->view('header'); ?>
<div class="waste-container">
    <div class="ui huge breadcrumb">
        <a class="section">Kabaadi App </a>
        <i class="right chevron icon divider"></i>
        <a href="<?= site_url('aboutus') ?>" class="active section"><i class="at icon"></i>About Us</a>
    </div>
    <div class="ui divider"></div>
    <? if ($this->session->flashdata('failure-msg') != ''): ?>
        <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
                Error!!
            </div>
            <p><?= $this->session->flashdata('failure-msg') ?></p>
        </div>
    <? elseif ($this->session->flashdata('success-msg') != ''): ?>
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Congratulation!!
            </div>
            <p><?= $this->session->flashdata('success-msg') ?></p>
        </div>
    <? endif; ?>
    <div class="ui three column grid margin-top">
    </div>
    <form class="ui form" enctype = "multipart/form-data" method="post"  action="<?php echo site_url('aboutus/edit/') ?>">
        <table class="ui table">
            <tr>
                <td>Image</td>
                <td>
                    <? if ($aboutus[0] != ''): ?>
                        <img src="<?php echo @$aboutus[0]['img'] ?>" style="width:300px;"/>
                    <? endif; ?><br>
                    <input id="uploadBtn" type="file" name = "userfile" class="upload" />
                    <input type="hidden" name="file" id="file" value="<?php echo @$aboutus[0]['img'] ?>">
                </td>
            </tr>
            <tr>
                <td>Title</td>
                <td> <input type="text" name="title" value="<?php echo @$aboutus[0]['title'] ?>" /></td>
            </tr>
            <tr>
                <td >Description</td>
                <td><textarea type="text" class="required txtinput" id="description" name="description" 
                value ="<?php echo @$aboutus[0]['description'] ?>"><?php echo @$aboutus[0]['description'] ?></textarea>
                </td>

            </tr>
            <tr>
                <td >&nbsp;</td>
                <td><input type="submit" name="submit_detail" id="submit_detail" value="Submit" class="btn front-next" /></td>
            </tr> 
        </table>
    </form>
    <? $this->load->view('super_admin/modal'); ?>
</div>

<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/jquery.dataTables.js"></script>
<script>
    $(document).ready(function () {
        $('#message-table').DataTable({
            "iDisplayLength": 10,
            "aLengthMenu": [[5, 10, 15, 25, 35, 50, 100, -1], [5, 10, 15, 25, 35, 50, 100, "All"]]
        });
    });
</script>
<? $this->load->view('footer'); ?>


