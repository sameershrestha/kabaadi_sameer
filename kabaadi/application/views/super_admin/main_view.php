<? $this->load->view('header'); ?>
<div class="container" style="margin-top:125px;">
    <div class="right-fixed-container">
        <div class="ui vertical menu m-menu">
            <a class="item">
                <img src="<?= base_url('assets/markers/recycle.png') ?>"/>
                <labell>Dealers</labell>
            </a>
            <a class="item">
                <img src="<?= base_url('assets/markers/phones.png') ?>"/>
                <label>Mobile Users</label>
            </a>
            <!--            <a class="item">
                            <i class="zoom out large icon"></i>
                        </a>
                        <a class="item" >
                            <i class="retweet large icon"></i>
                        </a>
                        <a class="item" onclick='locate()' >
                            <i class="marker large icon"></i>
                        </a>-->
        </div>
    </div>
    <div class="ui grid">
        <div class="map-container" id="map"></div>
    </div>
</div>
</body>
<script src="https://maps.googleapis.com/maps/api/js"></script>
<script>
    var baseurl = '<?= base_url() ?>';
    var map = '';
    var mapCanvas = document.getElementById('map');
    var mapOptions = {
        center: new google.maps.LatLng(28.38824, 84.33567),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    if (!map) {
        map = new google.maps.Map(mapCanvas, mapOptions);
    }
    else {
        map.setOptions(mapOptions);
    }
    var infoWindow = new google.maps.InfoWindow();
<? foreach ($users as $row): ?>
        var lat = '<?= $row['lat'] ?>';
        var lng = '<?= $row['longitude'] ?>';
    <? if ($row['group_id'] == 3): ?>
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: baseurl + 'assets/markers/phones.png',
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent('<b>District: ' + '<?= $row['district'] ?>' + '</b><hr>Location: ' + '<?= $row['office_location'] ?>');
                infoWindow.open(map, this);
            });
    <? elseif ($row['group_id'] == 2): ?>
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(lat, lng),
                icon: baseurl + 'assets/markers/recycle.png',
                map: map
            });
            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.setContent('<b>District: ' + '<?= $row['district'] ?>' + '</b><hr>Dealer Location: ' + '<?= $row['office_location'] ?>');
                infoWindow.open(map, this);
            });
    <? endif; ?>


<? endforeach; ?>

    $(document).ready(function () {
        $("#map").height($(document).height() - 250);
        $(window).resize(function () {
            $("#map").height($(document).height() - 250);
        });

    });

    $('.ui.dropdown').dropdown({
        on: 'hover'
    });
    $('.ui.checkbox').checkbox();
</script>
<? $this->load->view('footer'); ?>
