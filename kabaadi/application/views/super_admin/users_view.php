<? $this->load->view('header'); ?>
<div class="waste-container">
    <div class="ui huge breadcrumb">
        <a class="section">Kabaadi App </a>
        <i class="right chevron icon divider"></i>
        <a href="<?= site_url('users') ?>" class="active section"><i class="mobile icon"></i>Moble Users</a>
    </div>
    <div class="ui divider"></div>
    <? if ($this->session->flashdata('failure-msg') != ''): ?>
        <div class="ui negative message">
            <i class="close icon"></i>
            <div class="header">
                Error!!
            </div>
            <p><?= $this->session->flashdata('failure-msg') ?></p>
        </div>
    <? elseif ($this->session->flashdata('success-msg') != ''): ?>
        <div class="ui success message">
            <i class="close icon"></i>
            <div class="header">
                Congratulation!!
            </div>
            <p><?= $this->session->flashdata('success-msg') ?></p>
        </div>
    <? endif; ?>
    <div class="ui three column grid margin-top">
        <!--        <div class="column"> 
                    <h3 class="ui blue header">
                        <i class="mobile icon"></i>
                        <div class="content">
                            Moble Users
                        </div>
                    </h3>
                </div>
                <div class="column"></div>
                <div class="column">
                    <form id="search" method="post" action="<?php echo site_url('users/search/') ?> ">
                        <div class="ui  search">
                            <div class="ui icon input">
                                <input class="prompt required" required="" name="search" placeholder="Search products..." type="text">
                                <i id="search_link" class="inverted blue circular search link icon"></i>
                            </div>
                            <div class="results"></div>
                        </div>
                    </form>
                </div>-->
    </div>
    <table class="ui celled table" id="users_view">
        <thead>
            <tr><th>SN</th>
<!--                <th>Name</th>
                -->                <th>Email</th>
<!--                <th>Location</th>                
                <th>Contact No.</th>-->
                <th>Action</th>
            </tr></thead>
        <tbody>
            <?
            $s = 1;
            foreach ($admin as $row): $sn = $s++;
                ?>
                <tr>
                    <td><?= $sn ?></td>
    <!--                    <td><?= $row['name'] ?></td>           -->  
                    <td><?= $row['email'] ?></td>
    <!--                    <td><?= $row['office_location'] ?></td>
                    <td><?= $row['contact'] ?></td>-->
                    <td>
                        <a href="<?= site_url('users/users_pickup_details/' . $row['id']) ?>"  class="circular ui green icon button" data-content="View user activity">
                            <i class="icon eye"></i>
                        </a>
                        <a onclick="send_message('<?= $row['id']; ?>')"  class="circular ui yellow icon button" id="send_<?= $row['id']; ?>" data-content="Send Message">
                            <i class="icon send"></i>
                        </a>
                    </td>
                </tr>
            <? endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
            </tr>
        </tfoot>
    </table>
    <div class="ui segment load" style="display: none;" >
        <div class="ui active dimmer" style="position: fixed !important">
            <div class="ui large text loader">Sending Message... <br> Please Wait !!</div>
        </div>
        <p></p>
    </div>
    <? $this->load->view('super_admin/modal'); ?>
</div>
<script type="text/javascript" src="<?= base_url(); ?>assets/DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="<?= base_url(); ?>assets/js/jquery.validate.js"></script>
<script>
                            $('#msg').validate();

                            function checkvalid() {
                                var valid_msg = $('#msg').valid();
                                if (valid_msg == true) {
                                    $('.ui.long.modal.msg').modal('hide');
                                    $('.load').show();
                                }
                            }
//////////////// /////////////// 

//    var form = document.getElementById("search");
//    document.getElementById("search_link").addEventListener("click", function () {
//        form.submit();
//        loader();
//    });
<? foreach ($admin as $row): ?>
                                $('#send_<?= $row['id']; ?>').click(function () {
                                    $('.ui.long.modal.msg').modal('setting', 'transition', 'vertical flip').modal('show');
                                });
<? endforeach; ?>


                            function send_message(uid) {
                                $('#uid').val(uid);
                            }
                            ///////
                            $(document).ready(function () {
                                $('#users_view').DataTable({
                                    "iDisplayLength": 5,
                                    "aLengthMenu": [[5, 10, 15, 25, 35, 50, 100, -1], [5, 10, 15, 25, 35, 50, 100, "All"]]
                                });
                            });
</script>
<? $this->load->view('footer'); ?>