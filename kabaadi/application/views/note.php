<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <link rel="stylesheet" href="<?php echo base_url() ?>/assets/css/note.css">
    </head>
    <body>
        <div class="note-wrapper" >
            <div class="note note-help">
                <div class="note-heading">
                    <i class="icon bell"></i>
                </div>
                <strong>Warning</strong>: If you're outside the U.S., you may not see featured/promotional content for a bit. Once Google is ready to roll this release out in other countries, the content should start populating as you would expect. The rest of the Play Store works just fine.
            </div>
        </div>
    </body>
</html>
