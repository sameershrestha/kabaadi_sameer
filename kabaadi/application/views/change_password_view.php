<!DOCTYPE html>
<? $this->load->view('header'); ?>
<div class="container">
    <div class="suppliers-container">
        <div class="wrapper">
            <div class="change password">
                <? if ($this->session->flashdata('success-msg') != ''): ?>
                    <div class="ui success message">
                        <i class="close icon"></i>

                        <div class="header">
                            <?= $this->session->flashdata('success-msg') ?>
                        </div>
                    </div>
                <? elseif ($this->session->flashdata('failure-msg') != ''): ?>
                    <div class="ui negative message">
                        <i class="close icon"></i>

                        <div class="header">
                            <?= $this->session->flashdata('failure-msg') ?>
                        </div>
                    </div>
                <? endif; ?>
                <form id="change_password" method="post" action="<?php echo site_url('login/change_password/') ?>"
                      onsubmit="loader()">
                    <div class="ui form segment">
                        <h3 class="ui blue header">Change Password</h3>

                        <div class="ui divider"></div>
                        <div class="required field">
                            <label>Old Password</label>
                            <input placeholder="Old Password" type="password" name="old_psw" required="">
                        </div>
                        <div class="required field">
                            <label>New Password</label>
                            <input placeholder="New Password" type="password" name="new_psw" required="">
                        </div>
                        <div class="required field">
                            <label>Confirm Password</label>
                            <input placeholder="Confirm Password" type="password" name="con_psw" required="">
                        </div>
                        <button type="submit" class="ui green submit button" name="change" value="change">Change
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<? $this->load->view('footer'); ?>
