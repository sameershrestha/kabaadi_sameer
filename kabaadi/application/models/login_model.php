<?php

class Login_Model extends CI_Model {

    public function get_username($username) {
        $sql = "select * from login_users where email='$username'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_password($username) {
        $username = $this->db->escape_str($username);
        $sql = "select * from login_users where email='$username' or contact='$username'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function user_login($password) {
        $sql = "select * from login_users where  password='$password'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_old_password($uid) {
        $uid = $this->db->escape_str($uid);
        $sql = "select * from login_users where id='$uid'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function change_pass($user_id, $encrypted_password, $salt) {
        $sql = "update login_users set password ='$encrypted_password',salt='$salt' where id='$user_id'";
        $result = $this->db->query($sql);
        return $result;
    }

    public function get_contact($contact) {
        $sql = "select * from login_users where contact='$contact'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function signup_users($contact, $district, $location, $lat, $longitude, $encrypted_password, $salt) {
        $group = '3';
        $sql = "insert into login_users (contact,district,office_location,lat,longitude,password,salt,group_id,created_at) values('$contact','$district','$location','$lat','$longitude','$encrypted_password','$salt','$group',now())";
        $result = $this->db->query($sql);
        return $result;
    }

//    public function sign_up($name = '', $email = '', $username = '', $password = '') {
//        $group = '2';
//        $sql = "insert into login_users (name,username,email,password,group_id) values('$name','$username','$email','$password','$group')";
//        $result = $this->db->query($sql);
//        return $result;
//    }
}

?>