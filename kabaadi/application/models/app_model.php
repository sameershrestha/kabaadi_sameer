<?php

class App_Model extends CI_Model {

    public function get_username($username) {
        $username = $this->db->escape_str($username);
        $sql = "select * from login_users where email='$username' and group_id='3'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_admin($username) {
        $username = $this->db->escape_str($username);
        $sql = "select * from login_users where email='$username' and group_id='2'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function login_process($uid, $device_id, $device_type, $cookie, $user) {
        if ($user == 'old') {
            $sql = "Update mobile_users set device_id='$device_id',device_type='$device_type',session='$cookie' where user_id='$uid'";
            $result = $this->db->query($sql);
            return $result;
        } elseif ($user == 'new') {
            $sql = "insert into login_users (email,group_id,created_at) values('$uid','3',now())";
            $this->db->query($sql);
            $id = $this->db->insert_id();
            $sql1 = "insert into mobile_users (user_id,device_id,device_type,session) values('$id','$device_id','$device_type','$cookie')";
            $result = $this->db->query($sql1);
            return $result;
        }
    }

    public function get_session($username) {
        $sql = "select * from login_users lu join
                (select id as mid,user_id,device_id,device_type,session from mobile_users)mu on mu.user_id=lu.id where lu.email='$username'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function delete_session($uid, $session) {
        $sql = "Update mobile_users set session='$session' where user_id='$uid'";
        $result = $this->db->query($sql);
        return $result;
    }

//    public function get_dealer() {
//        $sql = "select * from login_users lu
//                join(
//                select * from admin_waste_category
//                )aw on lu.id=aw.user_id
//                where group_id='2'";
//        $result = $this->db->query($sql)->result_array();
//        return $result;
//    }
    public function get_dealer() {
        $sql = "select * from login_users lu
            join(select admin_id from waste_sub_type)wst on lu.id=wst.admin_id  where lu.group_id='2' group by wst.admin_id";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_buyer($did) {
        $sql = "select id as dealer_id,name,district,office_location,contact,img_url from login_users 
            where id='$did' and group_id='2'
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_category($buyer) {
        $sql = "select DISTINCT(waste_type_id) as cat_id,admin_id,wt.type as category,wt.img_url from waste_sub_type wst
            join(select * from waste_type)wt on wt.id=wst.waste_type_id
            where wst.admin_id='$buyer'
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_buyer_category($cat_id, $did) {
        $sql = "select id as subcategory_id,sub_type as subcategory_name,rate from waste_sub_type where waste_type_id='$cat_id' and admin_id='$did'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_user($email) {
        $sql = "select id as user_id from login_users where group_id='3' and email='$email'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_rate($sub_id, $did) {
        $sql = "select * from waste_sub_type where admin_id='$did' and id='$sub_id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function save_pickup($uid, $did, $title, $pickup_contact, $location, $house_no, $lat, $lng) {
        $sql1 = "insert into pickup_status (user_id,buyer_id,title,location,pickup_contact,house_no,lat,lng,status,added_date) values('$uid', '$did', '$title', '$location','$pickup_contact','$house_no', '$lat', '$lng', '0', now())";
        $this->db->query($sql1);
        return $this->db->insert_id();
    }

    public function save_pickup_img($uid, $did, $title, $pickup_contact, $location, $house_no, $lat, $lng) {
        $sql1 = "insert into pickup_status_img (user_id,buyer_id,title,location,pickup_contact,house_no,lat,lng,status,added_date) values('$uid', '$did', '$title', '$location','$pickup_contact','$house_no', '$lat', '$lng', '0', now())";
        $this->db->query($sql1);
        return $this->db->insert_id();
    }

    public function add_pickup_img($pid, $img_url) {
        $sql1 = "insert into pickup_img_path (psi_id,img_url) values('$pid','$img_url')";
        $this->db->query($sql1);
        return $this->db->insert_id();
    }

    public function get_date($last_id) {
        $sql = "select * from pickup_status where id='$last_id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function save_request($activity_id, $uid, $did, $title, $sub_type, $rate, $qty, $tot, $date) {
        $sql1 = "insert into pickup_request (activity_id,user_id,buyer_id,title,sub_type,rate,quantity,total_price,added_date) values('$activity_id','$uid', '$did', '$title', '$sub_type', '$rate', '$qty', '$tot', '$date')";
        $result = $this->db->query($sql1);
        return $result;
    }

//    public function get_activity_status($uid) {
//        $sql = "select id as activity_id,buyer_id,title,location,status,added_date,lu.name,lu.contact,lu.img_url from pickup_status ps
//                 join( select id as did,name,contact,img_url from login_users)lu on lu.did=ps.buyer_id
//                 where ps.user_id='$uid'";
//        $result = $this->db->query($sql)->result_array();
//        return $result;
//    }
    public function get_activity_status($uid) {
        $sql = "select id as activity_id,buyer_id,title,location,status,added_date from pickup_status
                 where user_id='$uid'and status='2' order by activity_id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_all_activity_status($uid) {
        $sql = "select id as activity_id,buyer_id,title,location,status,added_date from pickup_status
                 where user_id='$uid' and (status='0' or status='1') order by activity_id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_status($activity_id) {
        $sql = "select * from pickup_status where id='$activity_id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_pickup_status($activity_id) {
        $sql = "select id as activity_id,buyer_id,title,location,status,added_date from pickup_status where id='$activity_id' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_activity($activity_id) {
        $sql = "select activity_id,title,quantity,total_price,location,ps.added_date from pickup_request pr join(select id as act_id,location,status,added_date from pickup_status)ps on ps.act_id=pr.activity_id where activity_id='$activity_id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_aboutus() {
        $sql = "select * from about_us";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_sess($session) {
        $sql = "select * from mobile_users where session='$session'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function delete_all_activities($uid) {
        $sql = "delete pickup_status,pickup_request from pickup_status inner join pickup_request where pickup_status.user_id='$uid' and pickup_status.status='2' and pickup_status.id=pickup_request.activity_id";
        $result = $this->db->query($sql);
//        if ($result == true) {
//            $sql1 = "delete from pickup_request where user_id='$uid' and status='2'";
//            $result1 = $this->db->query($sql1);
//        }
        return $result;
    }

    public function dealer_pickup_request($did) {
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.pickup_contact,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status ps
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
            where ps.buyer_id='$did' and (ps.status='0' || ps.status='1')order by ps.id DESC
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function dealer_pickup_request_img($did) {
        $sql = "select pip.img_url,ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.pickup_contact,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status_img ps
            join(select * from pickup_img_path)pip on pip.psi_id=ps.id
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
            where ps.buyer_id='$did' and (ps.status='0' || ps.status='1')order by ps.id DESC
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

}

?>