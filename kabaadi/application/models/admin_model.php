<?php

class Admin_Model extends CI_Model {

/// dealer part

    public function check_duplicate($email, $contact) {
        $sql = "select * from login_users where email='$email' or contact='$contact'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function create_admin($name, $email, $encrypted_password, $salt, $district, $location, $lat, $long, $category, $contact, $img_url, $cate) {
        $group = '2';
        $sql = "insert into login_users (name,email,password,salt,district,office_location,lat,longitude,contact,img_url,group_id,created_at) values('$name','$email','$encrypted_password','$salt','$district','$location','$lat','$long','$contact','$img_url','$group',now())";
        $this->db->query($sql);
        $uid = $this->db->insert_id();

        $sql1 = "insert into admin_category (admin_id,category) values('$uid','$cate')";
        $this->db->query($sql1);

        $user_id = array("user_id" => $uid);
        $catt = array_merge($category, $user_id);
        $this->db->set($catt);
        $result = $this->db->insert('admin_waste_category');
        return $result;
    }

    public function get_admin() {
        $sql = "select * from login_users lu
            join(select admin_id,category from admin_category
            )ac on ac.admin_id=lu.id
                 where group_id='2' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function search_admin($search) {
        $sql = "select * from login_users lu
            join(select admin_id,category from admin_category
            )ac on ac.admin_id=lu.id where lu.group_id='2' and (lu.district='$search' or lu.office_location='$search' or ac.category like '%$search%')";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_dealer($id) {
        $sql = "select * from login_users lu
        join(select * from admin_waste_category
            )awc on lu.id=awc.user_id
where lu.group_id='2' and lu.id='$id'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function update_admin($uid, $name, $email, $newpassword, $encrypted_password, $salt, $district, $location, $lat, $long, $category, $contact, $image_url, $cate) {
        if ($image_url != '' && $newpassword == '') {
            $sql = "Update login_users set name='$name',email='$email',district='$district',office_location='$location',lat='$lat',longitude='$long',contact='$contact',img_url='$image_url' where id='$uid'";
        } elseif ($newpassword != '' && $image_url == '') {
            $sql = "Update login_users set name='$name',email='$email',password='$encrypted_password',salt='$salt',district='$district',office_location='$location',lat='$lat',longitude='$long',contact='$contact' where id='$uid'";
        } elseif ($newpassword != '' && $image_url != '') {
            $sql = "Update login_users set name='$name',email='$email',password='$encrypted_password',salt='$salt',district='$district',office_location='$location',lat='$lat',longitude='$long',contact='$contact',img_url='$image_url' where id='$uid'";
        } else {
            $sql = "Update login_users set name='$name',email='$email',district='$district',office_location='$location',lat='$lat',longitude='$long',contact='$contact' where id='$uid'";
        }
        $this->db->query($sql);
        $sql1 = "Update admin_category set category='$cate' where admin_id='$uid'";
        $this->db->query($sql1);
        $this->db->where(array('user_id' => $uid));
        $result = $this->db->update('admin_waste_category', $category);
        return $result;
    }

/// common part

    public function delete_user($uid) {
        $sql = "delete from login_users where id='$uid'";
        $this->db->query($sql);
        $sql1 = "delete from admin_category where admin_id='$uid'";
        $this->db->query($sql1);
        $sql2 = "delete from admin_waste_category where user_id='$uid'";
        $result = $this->db->query($sql2);
        return $result;
    }

/// user part

    public function search_users($search) {
        $sql = "select * from login_users where group_id='3' and office_location='$search'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_userslist() {
        $sql = "select * from login_users where group_id='3' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

/// for map

    public function get_pickup_users($did) {
        $sql = "select * from pickup_status ps
                join(select id as u_id from login_users)lu on lu.u_id=ps.user_id where ps.buyer_id='$did' and (ps.status='0' or ps.status='1')";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_users() {
        $sql = "select * from login_users where group_id='2'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    // for mobile user
    public function get_buyer() {
        $sql = "select * from login_users where group_id='2'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

}

?>