<?php

class Pickup_Model extends CI_Model {

    public function get_dealers_only(){
        //get list of dealers only
        //only select the dealers name where group_id = 2 indicates Dealers
        $sql = "select name from login_users where group_id = 2"; 
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_pickup_request() {
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.pickup_contact,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status ps
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
                order by ps.id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_request_info_by($dealer_name){
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.pickup_contact,ps.lat,
        ps.lng,ps.status,ps.added_date, lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,
        l.contact as b_contact from pickup_status ps 
        join(select * from login_users)lu on lu.id=ps.user_id 
        join(select * from login_users where name='".$dealer_name."')l on l.id=ps.buyer_id order by ps.id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function dealer_pickup_request($did) {
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.pickup_contact,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status ps
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
            where ps.buyer_id='$did'order by ps.id DESC
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function dealer_image_pickup_request($did) {
        $sql = "select * from pickup_status_img psi
            join(select * from pickup_img_path)pip on pip.psi_id=psi.id
            where psi.buyer_id='$did'order by id DESC
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function search_pickup_request($search) {
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status ps
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
            where l.office_location='$search' or ps.added_date='$search' or ps.status='$search'
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function search_pickup_request_dealer($did, $search) {
        $sql = "select ps.id as pickupid,ps.user_id,ps.buyer_id,ps.title,ps.location as pickup_location,ps.lat,ps.lng,ps.status,ps.added_date,
            lu.name as p_name,lu.contact as p_contact,l.name as b_name,l.office_location as b_location,l.contact as b_contact from pickup_status ps
            join(select * from login_users)lu on lu.id=ps.user_id
            join(select * from login_users)l on l.id=ps.buyer_id
            where ps.buyer_id='$did' and (ps.added_date='$search' or ps.location='$search' or ps.status='$search'  )
                ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_pickup_status($pid) {
        $sql = "select * from pickup_status where id='$pid' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_pickup($uid) {
        $sql = "select * from pickup_request where user_id='$uid' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function change_status($pid, $status) {
        $sql = "update pickup_status set status='$status' where id='$pid'";
        $result = $this->db->query($sql);
        return $result;
    }

    public function change_status_img($pid, $status) {
        $sql = "update pickup_status_img set status='$status' where id='$pid'";
        $result = $this->db->query($sql);
        return $result;
    }

}

?>