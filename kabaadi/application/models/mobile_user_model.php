<?php

class Mobile_User_Model extends CI_Model {

    public function pickup_request_add($user_id, $buyer_id, $title, $subcat, $rate, $quantity, $total) {
        $sql = "insert into pickup_request (user_id,buyer_id,title,sub_type,rate,quantity,total_price,added_date) value('$user_id','$buyer_id','$title','$subcat','$rate','$quantity','$total',now())";
        $result = $this->db->query($sql);
        return $result;
    }

    public function pickup_status_add($user_id, $buyer_id, $title, $location, $lat, $lng) {
        $sql = "insert into pickup_status (user_id,buyer_id,title,location,lat,lng,status,added_date) value('$user_id','$buyer_id','$title', '$location', '$lat', '$lng','0',now())";
        $result = $this->db->query($sql);
        return $result;
    }

    public function get_activity_status($uid) {
        $sql = "select * from pickup_status where user_id='$uid' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_activity($uid) {
        $sql = "select * from pickup_request where user_id='$uid' order by id DESC";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_dealer() {
        $sql = "select * from login_users lu
                join(
                select * from admin_waste_category
                )aw on lu.id=aw.user_id
                where group_id='2'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_user_detail($uid){
        $sql = "select * from login_users where group_id = 3 and id='".$uid."'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }
}

?>