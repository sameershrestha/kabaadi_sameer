<?php

class Notification_Model extends CI_Model {

    public function get_regid($uid) {
        $sql = "select * from mobile_users where user_id='$uid' ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function save_message($uid, $group, $admin_id, $subject, $msg) {
        $msg = mysql_real_escape_string($msg);
        $sql1 = "insert into message (send_by_group,send_by_id,send_to_id,subject,message,added_date) values('$group','$admin_id','$uid','$subject','$msg',now())";
        $result = $this->db->query($sql1);
        return $result;
    }

    public function get_messages() {
        $sql = "select * from message m join(select id as uid,email from login_users)lu on lu.uid=m.send_to_id order by id DESC ";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function send_notice() {

        $this->send_notification_android($regid, $message_android);



//GET IDS FROM IOS
//        $sql1 = "SELECT * from tbl_regid where (" . $cond . ") and device_type='ios' and status = 1 ORDER BY 'device_type' DESC  ";
//        $qry1 = $this->db->query($sql1);
//        $result1 = $qry1->result();
//
//        foreach ($result1 as $row) {
//            array_push($regid_ios, $row->registration_id);
//        }
//        $registration_id_ios = count($regid_ios);
//        $message_ios = $message;
//        for ($i = 0; $i < $registration_id_ios; $i++) {
//            $result1 = $this->send_notification_ios($regid_ios[$i], $message_ios);
//        }
    }

//    function send_silent_notification_ios($registration_id_ios, $message, $tag) {
//        $payload['aps'] = array('content-available' => 1);
//        $payload['code'] = $tag;
//        $payload = json_encode($payload);
//        $certificate = 'iospush/ck.pem';
//
//        $options = array('ssl' => array(
//                'local_cert' => $certificate,
//                'passphrase' => '123'
//        ));
//        $streamContext = stream_context_create();
//        stream_context_set_option($streamContext, $options);
//        //$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT,$streamContext);
//        $apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
//        if ($apns) {
//            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(array(' ', '<', '>'), '', $registration_id_ios)) . chr(0) . chr(strlen($payload)) . $payload;
//            fwrite($apns, $apnsMessage);
//        }
//        fclose($apns);
//    }
//
//    function send_notification_ios($registration_id_ios, $message) {
//        $payload['aps'] = array('alert' => $message, 'content-available' => 1, 'badge' => 1, 'sound' => 'default');
//        $payload['code'] = 200;
//        $payload = json_encode($payload);
//        $certificate = 'iospush/ck.pem';
//
//        $options = array('ssl' => array(
//                'local_cert' => $certificate,
//                'passphrase' => '123'
//        ));
//        $streamContext = stream_context_create();
//        stream_context_set_option($streamContext, $options);
//        //$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT,$streamContext);
//        $apns = stream_socket_client('ssl://gateway.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
//        if ($apns) {
//            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(array(' ', '<', '>'), '', $registration_id_ios)) . chr(0) . chr(strlen($payload)) . $payload;
//            fwrite($apns, $apnsMessage);
//        }
//        fclose($apns);
//    }

    function send_notification_android($registration_id, $message) {

        //wa api
        $api_key = "AIzaSyDUPyJxHIf0MexSTdoIaMfnYElJ8hEoQ98";

        // Set POST variables
//        $device_id = $registration_id;
        $msg = $message;
        $url = 'https://android.googleapis.com/gcm/send';
//        $device_ids = array($device_id);
        $t_data = array();
        $t_data['message'] = array($msg);
        $t_json = array('registration_ids' => $registration_id, 'data' => $t_data);

        $headers = array(
            'Authorization: key=' . $api_key,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($t_json));

        // Execute post
        $result = curl_exec($ch);

        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }
//         echo $result;
        // Close connection
        curl_close($ch);


        $array = json_decode(trim($result), TRUE);
        return $array;

        //   echo $result;   
    }

    function send_notification_ios($registration_id_ios, $subject, $message) {
        $payload['aps'] = array('subj' => $subject, 'alert' => $message, 'content-available' => 1, 'badge' => 1, 'sound' => 'default');
        $payload['code'] = 200;
        $payload = json_encode($payload);
        $certificate = 'iospush/ck.pem';

        $options = array('ssl' => array(
                'local_cert' => $certificate,
                'passphrase' => 123
        ));
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, $options);
        //$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT,$streamContext);
        $apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
        if ($apns) {
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(array(' ', '<', '>'), '', $registration_id_ios)) . chr(0) . chr(strlen($payload)) . $payload;
            $tResult = fwrite($apns, $apnsMessage);
        }
        fclose($apns);
        if ($tResult) {
            $return['success'] = 'true';
        } else {
            $return['success'] = 'false';
        }
        return $return;
    }

    function send_process_notification_ios($registration_id_ios, $message) {
        $payload['aps'] = array('content-available' => 1, 'badge' => 1, 'sound' => 'default');
        $payload['process'] = $message;
        $payload = json_encode($payload);
        $certificate = 'iospush/ck.pem';

        $options = array('ssl' => array(
                'local_cert' => $certificate,
                'passphrase' => 123
        ));
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, $options);
        //$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT,$streamContext);
        $apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
        if ($apns) {
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(array(' ', '<', '>'), '', $registration_id_ios)) . chr(0) . chr(strlen($payload)) . $payload;
            $tResult = fwrite($apns, $apnsMessage);
        }
        fclose($apns);
        if ($tResult) {
            $return['success'] = 'true';
        } else {
            $return['success'] = 'false';
        }
        return $return;
    }

    function send_silent_notification_ios($registration_id_ios, $message) {
        $payload['aps'] = array('content-available' => 1);
        $payload['about'] = $message;
        $payload = json_encode($payload);
        $certificate = 'iospush/ck.pem';

        $options = array('ssl' => array(
                'local_cert' => $certificate,
                'passphrase' => '123'
        ));
        $streamContext = stream_context_create();
        stream_context_set_option($streamContext, $options);
        //$apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT,$streamContext);
        $apns = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $error, $errorString, 60, STREAM_CLIENT_CONNECT, $streamContext);
        if ($apns) {
            $apnsMessage = chr(0) . chr(0) . chr(32) . pack('H*', str_replace(array(' ', '<', '>'), '', $registration_id_ios)) . chr(0) . chr(strlen($payload)) . $payload;
            $tResult = fwrite($apns, $apnsMessage);
        }
        fclose($apns);
        if ($tResult) {
            $return['success'] = 'true';
        } else {
            $return['success'] = 'false';
        }
        return $return;
    }

}

?>