<?php

class Category_Model extends CI_Model {

    public function get_category() {
        $sql = "select * from waste_type";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_admin_category($aid) {
        $sql = "select * from admin_category where admin_id='$aid'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_subcat($wid, $did) {
        $sql = "select * from waste_sub_type where waste_type_id='$wid' and admin_id='$did'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_dealer_subcategory($did) {
        $sql = "select * from waste_sub_type where admin_id='$did'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_subcategory($catid) {
        $sql = "select * from waste_sub_type where waste_type_id='$catid'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function get_selected_rate($subtype) {
        $sql = "select * from waste_sub_type where sub_type='$subtype'";
        $result = $this->db->query($sql)->result_array();
        return $result;
    }

    public function add_category($category, $category_type, $img_url) {
        $sql = "ALTER TABLE admin_waste_category ADD $category VARCHAR(250)  NULL";
        $result = $this->db->query($sql);

        $sql1 = "insert into waste_type (type,img_url) values('$category_type','$img_url')";
        $result1 = $this->db->query($sql1);
        return $result1;
    }

    public function add_subcategory($did, $catid, $subcat, $rate) {
        $sql = "insert into waste_sub_type (admin_id,waste_type_id,sub_type,rate) values('$did','$catid','$subcat','$rate')";
        $result = $this->db->query($sql);
        return $result;
    }

    public function update_subcategory($did, $catid, $subcat_id, $subcat, $rate) {
        $sql = "update waste_sub_type set sub_type='$subcat',rate='$rate' where id='$subcat_id' and admin_id='$did' and waste_type_id='$catid'";
        $result = $this->db->query($sql);
        return $result;
    }

    public function delete_category($cid, $category) {
        $sql = "ALTER TABLE admin_waste_category DROP COLUMN $category ";
        $result = $this->db->query($sql);

        $sql1 = "delete from waste_sub_type where waste_type_id='$cid'";
        $result1 = $this->db->query($sql1);

        $sql2 = "delete from waste_type where id='$cid'";
        $result2 = $this->db->query($sql2);
        return $result2;
    }

}

?>