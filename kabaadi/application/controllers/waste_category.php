<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Waste_Category extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('category_model');
        $this->data['uri'] = 'home';
        if ($this->session->userdata('group') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 1) {
            redirect('main');
        }
        //session_start();
    }

    public function index() {
        $this->data['uri'] = 'category';
        $this->data['category'] = $this->category_model->get_category();
        $this->load->view('super_admin/category_view', $this->data);
    }

    public function add() {
        $category = 'type_' . str_replace(' ', '_', $_POST['category']);
        $category_type = ucfirst(str_replace(' ', '_', $_POST['category']));

        $this->data['dir'] = 'assets/imgs/category/';
        $config['upload_path'] = $this->data['dir'];
        $config['allowed_types'] = 'gif|jpg|png|jpeg|x-png';
        $config['max_size'] = '400';
        $config['max_width'] = '1000';
        $config['max_height'] = '900';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
            $img = $this->upload->data();

            $config['image_library'] = "gd2";
            $config['source_image'] = $this->data['dir'] . $img['file_name'];
            $config['create_thumb'] = false;
            $config['new_image'] = 'kabadi_' . $img['file_name'];
            $config['maintain_ratio'] = TRUE;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            if (!$this->image_lib->resize()) {

                echo $this->image_lib->display_errors();
            } else {
                $image_url = base_url() . $this->data['dir'] . 'kabadi_' . $img['file_name'];
                unlink($this->data['dir'] . $img['file_name']);
                $result = $this->category_model->add_category($category, $category_type, $image_url);
                if ($result == true) {
                    $this->session->set_flashdata('success-msg', 'New Category Successfully Added !');
                    redirect('waste_category');
                } else {
                    $this->session->set_flashdata('failure-msg', 'New Category Failed To Add !');
                    redirect('waste_category');
                }
            }
        } else {
            $this->data['error'] = $this->upload->display_errors();
            $this->session->set_flashdata('failure-msg', $this->data['error']);
            redirect('waste_category');
        }
    }

    public function delete_selected_category($cid, $type) {
        $category = 'type_' . strtolower($type);
        $result = $this->category_model->delete_category($cid, $category);
        if ($result == true) {
            $this->session->set_flashdata('success-msg', 'Category Successfully Removed !');
            redirect('waste_category');
        } else {
            $this->session->set_flashdata('failure-msg', 'Category Failed To Remove !');
            redirect('waste_category');
        }
    }

}
