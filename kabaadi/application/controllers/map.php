<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Map extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->data['uri'] = 'map';
        if ($this->session->userdata('id') == '') {
            redirect('login');
        }
        //session_start();
    }

    public function index() {
        $this->load->view('super_admin/map_view', $this->data);
    }

}
