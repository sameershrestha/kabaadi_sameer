<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Users extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 1st Sep 2015
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('admin_model');
        $this->load->model('mobile_user_model');
        $this->data['uri'] = 'users';
        if ($this->session->userdata('id') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 1) {
            redirect('main');
        }
        //session_start();
    }

    public function index() {
        $this->data['admin'] = $this->admin_model->get_userslist();
//        $this->data['search_list'] = $this->admin_model->get_userslist();
        $this->load->view('super_admin/users_view', $this->data);
    }

    public function search() {
        if (!empty($_POST)) {
            $search = $_POST['search'];
            $this->data['admin'] = $this->admin_model->search_users($search);
            $this->data['search_list'] = $this->admin_model->get_userslist();
            $this->load->view('super_admin/users_view', $this->data);
        } else {
            redirect('users');
        }
    }

    public function get_user_details() {
        $uid = $_POST['id'];
        $result = $this->admin_model->get_user($uid);
        if (count($result) > 0) {
            echo json_encode($result);
        } else if ($result == FALSE) {
            echo json_encode(array('success-msg' => " Could Not Complete the Request. "));
        } else {
            echo json_encode(array('success-msg' => " No Records Available. "));
        }
    }

    public function users_pickup_details($uid) {
        $this->data['uid'] = $uid;
        $this->data['activity_date'] = $this->mobile_user_model->get_activity_status($uid);
        $this->data['activity'] = $this->mobile_user_model->get_activity($uid);
        $this->data['total'] = 0;
        foreach ($this->data['activity_date'] as $row) {
            foreach ($this->data['activity'] as $activity) {
                if ($row['id'] == $activity['activity_id']) {
                    $this->data['total'] = $this->data['total'] + $activity['total_price'];
                }
            }
        }
        $this->load->view('super_admin/pickup_details_view', $this->data);
    }

}
