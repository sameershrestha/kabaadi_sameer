<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Dealer_Control extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 1st Sep 2015
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('dealer_control_model');
        $this->load->model('pickup_model');
        $this->load->model('category_model');
        $this->data['uri'] = 'dealer';
        if ($this->session->userdata('id') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 2) {
            redirect('main');
        }
    }

    public function index() {
        
    }

    public function category() {
        $this->data['uri_segment'] = 'category';
        $adc = $this->category_model->get_admin_category($this->session->userdata('id'));
        $category = explode(',', $adc[0]['category']);

        $this->data['category'] = array();
        $result = $this->category_model->get_category();
        foreach ($result as $row) {
            foreach ($category as $cat) {
                if ($row['type'] == $cat) {
                    $this->data['category'][] = $row;
                }
            }
        }
        $this->data['subcategory'] = $this->category_model->get_dealer_subcategory($this->session->userdata('id'));
        $this->load->view('admin/category_view', $this->data);
    }

    public function add_subcat() {
        $did = $this->session->userdata('id');
        $cat_id = $_POST['id'];
        $count = count($_POST);
        $tc = $count - 2;
        $total_row = ($tc / 2);
        for ($i = 0; $i < $total_row; $i++) {
            $subcat = $_POST['subcat' . $i];
            $rate = $_POST['rate' . $i];
            $result = $this->category_model->add_subcategory($did, $cat_id, $subcat, $rate);
        }
        if ($result == true) {
            $this->session->set_flashdata('success-msg', 'Sub Category Successfully Added !');
            redirect('dealer_control/category');
        } else {
            $this->session->set_flashdata('failure-msg', 'Sub Category Failed To Add !');
            redirect('dealer_control/category');
        }
    }

    public function get_dealer_subcat() {
        $wid = $_POST['id'];
        $did = $this->session->userdata('id');
        $result = $this->category_model->get_subcat($wid, $did);
        if (count($result) > 0) {
            echo json_encode($result);
        } else if ($result == FALSE) {
            echo json_encode(array('success-msg' => " Could Not Complete the Request. "));
        } else {
            echo json_encode(array('success-msg' => " No Records Available. "));
        }
    }

    public function edit_subcat() {
        $did = $this->session->userdata('id');
        $cat_id = $_POST['catid'];
        $count = count($_POST);
        $tc = $count - 2;
        $total_row = ($tc / 2);
        for ($i = 0; $i < $total_row; $i++) {
            $subcat_id = $_POST['id' . $i];
            $subcat = $_POST['subcat' . $i];
            $rate = $_POST['rate' . $i];
            $result = $this->category_model->update_subcategory($did, $cat_id, $subcat_id, $subcat, $rate);
        }
        if ($result == true) {
            $this->session->set_flashdata('success-msg', 'Sub Category Successfully Updated !');
            redirect('dealer_control/category');
        } else {
            $this->session->set_flashdata('failure-msg', 'Sub Category Failed To Update !');
            redirect('dealer_control/category');
        }
    }

    public function pickup() {
        $this->data['uri_segment'] = 'pickup';
        $this->data['pickup_request'] = $this->pickup_model->dealer_pickup_request($this->session->userdata('id'));
//        $this->data['search_list'] = $this->data['pickup_request'];
        $this->load->view('admin/pickup_request_view', $this->data);
    }

    public function pickup_image() {
        $this->data['uri_segment'] = 'pickup image';
        $this->data['pickup_request'] = $this->pickup_model->dealer_image_pickup_request($this->session->userdata('id'));
//        var_dump($this->data['pickup_request']);die();
//        $this->data['search_list'] = $this->data['pickup_request'];
        $this->load->view('admin/pickup_request_image', $this->data);
    }

    public function search() {
        $search = $_POST['search'];

        if ($search == 'On Queue') {
            $search = 0;
        } else if ($search == 'On Process') {
            $search = 1;
        } else if ($search == 'Collected') {
            $search = 2;
        } else if ($search == 'Canceled') {
            $search = 3;
        }
        $this->data['uri_segment'] = 'pickup';
        $this->data['search_list'] = $this->pickup_model->dealer_pickup_request($this->session->userdata('id'));
        $this->data['pickup_request'] = $this->pickup_model->search_pickup_request_dealer($this->session->userdata('id'), $search);
        $this->load->view('admin/pickup_request_view', $this->data);
    }

    public function pickup_details($pid, $uid) {
        $this->data['pid'] = $pid;
        $this->data['uid'] = $uid;
        $this->data['uri_segment'] = 'pickup';
        $this->data['activity_date'] = $this->pickup_model->get_pickup_status($pid);
        $this->data['activity'] = $this->pickup_model->get_pickup($uid);
        $this->data['total'] = 0;
        foreach ($this->data['activity_date'] as $row) {
            foreach ($this->data['activity'] as $activity) {
                if ($row['id'] == $activity['activity_id']) {
                    $this->data['total'] = $this->data['total'] + $activity['total_price'];
                }
            }
        }
        $this->load->view('admin/pickup_details_view', $this->data);
    }

    public function pickup_status_change($pid, $uid, $process) {
        if ($process == 'onprocess') {
            $message = 'We are processing ur request';
            $status = 1;
            $result = $this->pickup_model->change_status($pid, $status);
        } elseif ($process == 'collected') {
            $message = 'Your waste has been collected';
            $status = 2;
            $result = $this->pickup_model->change_status($pid, $status);
        } elseif ($process == 'canceled') {
            $message = 'Your request has been cancelled';
            $status = 3;
            $result = $this->pickup_model->change_status($pid, $status);
        }
        if ($result == true) {
            $this->load->model('notification_model');
            $data = $this->notification_model->get_regid($uid);
            if (count($data) > 0) {
                if ($data[0]['device_type'] == 'Android') {
                    $regids = array();
                    $message_android = array("tag" => "tag_" . $process, "title" => $process, "message" => $message);
                    foreach ($data as $row) {
                        array_push($regids, $row['device_id']);
                    }

                    $msg = $this->notification_model->send_notification_android($regids, $message_android);
                    if ($msg['success'] == 1) {
                        $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed and notice has been sended to user!');
                        redirect('dealer_control/pickup');
                    } else {
                        $this->session->set_flashdata('failure-msg', $msg['results'][0]['error']);
                        redirect('dealer_control/pickup');
                    }
                } elseif ($data[0]['device_type'] == 'ios') {
                    $regid_ios = array();
                    $message_ios = $process;
                    foreach ($data as $row) {
                        array_push($regid_ios, $row['device_id']);
                    }
                    $registration_id_ios = count($regid_ios);
                    for ($i = 0; $i < $registration_id_ios; $i++) {
                        $return_msg = $this->notification_model->send_process_notification_ios($regid_ios[$i], $message_ios);
                    }
                    if ($return_msg['success'] == 'true') {
                        $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed and notice has been sended to user!');
                        redirect('dealer_control/pickup');
                    } else {
                        $this->session->set_flashdata('failure-msg', $return_msg['error']);
                        redirect('dealer_control/pickup');
                    }
                }
            } else {
                $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed but notice sending failed to user!');
                redirect('dealer_control/pickup');
            }
        } else {
            $this->session->set_flashdata('failure-msg', 'Please check your internet connection!!');
            redirect('dealer_control/pickup');
        }
    }

    public function pickup_status_change_img($pid, $uid, $process) {
        if ($process == 'onprocess') {
            $message = 'We are processing ur request';
            $status = 1;
            $result = $this->pickup_model->change_status_img($pid, $status);
        } elseif ($process == 'collected') {
            $message = 'Your waste has been collected';
            $status = 2;
            $result = $this->pickup_model->change_status_img($pid, $status);
        } elseif ($process == 'canceled') {
            $message = 'Your request has been cancelled';
            $status = 3;
            $result = $this->pickup_model->change_status_img($pid, $status);
        }
        if ($result == true) {
            $this->load->model('notification_model');
            $data = $this->notification_model->get_regid($uid);
            if (count($data) > 0) {
                if ($data[0]['device_type'] == 'Android') {
                    $regids = array();
                    $message_android = array("tag" => "tag_" . $process, "title" => $process, "message" => $message);
                    foreach ($data as $row) {
                        array_push($regids, $row['device_id']);
                    }

                    $msg = $this->notification_model->send_notification_android($regids, $message_android);
                    if ($msg['success'] == 1) {
                        $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed and notice has been sended to user!');
                        redirect('dealer_control/pickup_image');
                    } else {
                        $this->session->set_flashdata('failure-msg', $msg['results'][0]['error']);
                        redirect('dealer_control/pickup_image');
                    }
                } elseif ($data[0]['device_type'] == 'ios') {
                    $regid_ios = array();
                    $message_ios = $process;
                    foreach ($data as $row) {
                        array_push($regid_ios, $row['device_id']);
                    }
                    $registration_id_ios = count($regid_ios);
                    for ($i = 0; $i < $registration_id_ios; $i++) {
                        $return_msg = $this->notification_model->send_process_notification_ios($regid_ios[$i], $message_ios);
                    }
                    if ($return_msg['success'] == 'true') {
                        $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed and notice has been sended to user!');
                        redirect('dealer_control/pickup_image');
                    } else {
                        $this->session->set_flashdata('failure-msg', $return_msg['error']);
                        redirect('dealer_control/pickup_image');
                    }
                }
            } else {
                $this->session->set_flashdata('success-msg', 'Pickup Status Successfully Changed but notice sending failed to user!');
                redirect('dealer_control/pickup_image');
            }
        } else {
            $this->session->set_flashdata('failure-msg', 'Please check your internet connection!!');
            redirect('dealer_control/pickup_image');
        }
    }

}
