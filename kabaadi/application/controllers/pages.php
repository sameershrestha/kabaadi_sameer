<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->helper("url");
    }
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('kabaadi');
	}

	public function about()
	{
		$this->load->view('front/aboutus');
	}

	public function features()
	{
		$this->load->view('front/features');
	}

	public function team()
	{
		$this->load->view('front/team');
	}

	public function contact()
	{
		$this->load->view('front/contact');
		//$this->load->view('directory_name/file_name');
	}

	public function faq()
	{
		$this->load->view('front/faq');
	}

	public function terms()
	{
		$this->load->view('front/terms');
	}

	public function home()
	{
		$this->load->view('front/home');
	}

	public function send_mail() { 

		$this->load->helper('form'); 
        $this->load->view('email_form'); 
        echo $this->input->post('email'); 

	    $from_email = $this->input->post('email'); 
	    $to_email = "nicksamie@gmail.com";
	    $message = $this->input->post('message'); 

	     //Load email library 
	    $this->load->library('email'); 

	    $this->email->from($from_email, 'Your Name'); 
	    $this->email->to($to_email);
	    $this->email->subject('Email Test'); 
	    $this->email->message('Testing the email class.'); 

	     //Send mail 
	    if($this->email->send()) 
	    	$this->session->set_flashdata("email_sent","Email sent successfully."); 
	    else 
	    	$this->session->set_flashdata("email_sent","Error in sending Email."); 
	    $this->load->view('email_form'); 
    } 

	public function getMessage(){
		$config['protocol'] = 'sendmail';
		$config['mailpath'] = '/usr/sbin/sendmail';
		$config['charset'] = 'iso-8859-1';
		$config['wordwrap'] = TRUE;
		$config['mailtype'] = 'html'; // Append This Line
		$this->email->initialize($config);
		
		$_name = $_POST["name"];
        $from_email = $_POST["email"];
        $_phone = $_POST['phone'];
        $_subject = $_POST["subject"];
        $_message = $_POST["message"];

		$to      = 'nicksamie@gmail.com';

		$this->load->library('email');

		$this->email->from($from_email, $_name);
		$this->email->to($to);
		//$this->email->cc('another@another-example.com');
		//$this->email->bcc('them@their-example.com');

		$this->email->subject($_subject);
		$this->email->message($_message);

		$this->email->send();

		// $headers  = 'MIME-Version: 1.0' . "\r\n";
		// $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		// // Additional headers
		// $headers .= 'To: <nicksamie@gmail.com>' . "\r\n";
		// $headers .= 'From: Birthday Reminder <birthday@example.com>' . "\r\n";
		// $headers .= 'From: '.'<$_email>' . "\r\n";

		// Mail it
	}

	
}
