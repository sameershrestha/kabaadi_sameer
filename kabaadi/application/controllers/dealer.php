<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Dealer extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 1st Sep 2015
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('admin_model');
        $this->load->model('category_model');
        $this->data['uri'] = 'dealers';
        if ($this->session->userdata('id') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 1) {
            redirect('main');
        }
        //session_start();
    }

    public function index() {
        $this->data['admin'] = $this->admin_model->get_admin();
//        $this->data['search_list'] = $this->data['admin'];
        $this->data['category'] = $this->category_model->get_category();
        $count = count($this->data['category']);
        $result = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");
        $this->data['inword'] = $result[$count];
        $this->load->view('super_admin/dealers_view', $this->data);
    }

    public function add_admin() {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $district = $_POST["district"];
        $location = $_POST['location'];
        $lat = $_POST['lat'];
        $long = $_POST['long'];
        $contact = $_POST['contact'];

        $this->data['dir'] = 'assets/imgs/dealer/';
        $config['upload_path'] = $this->data['dir'];
        $config['allowed_types'] = 'gif|jpg|png|jpeg|x-png';
        $config['max_size'] = '400';
        $config['max_width'] = '1000';
        $config['max_height'] = '900';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
            $img = $this->upload->data();

            $config['image_library'] = "gd2";
            $config['source_image'] = $this->data['dir'] . $img['file_name'];
            $config['create_thumb'] = false;
            $config['new_image'] = 'dealer_' . $img['file_name'];
            $config['maintain_ratio'] = TRUE;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            } else {
                $image_url = base_url() . $this->data['dir'] . 'dealer_' . $img['file_name'];
                unlink($this->data['dir'] . $img['file_name']);

                $data = $this->admin_model->check_duplicate($email, $contact);
                $count = count($data);
                if ($count == 0) {
                    $category = $this->category_model->get_category();
                    $catt = array();
                    $cate = '';
                    foreach ($category as $cat) {
                        $catt = array_merge($catt, array("type_" . strtolower($cat['type']) => @$_POST['type_' . strtolower($cat['type'])]));
                        if (@$_POST['type_' . strtolower($cat['type'])] == 'on') {
                            if ($cate == '') {
                                $cate = $cat['type'];
                            } else {
                                $cate = $cate . ',' . $cat['type'];
                            }
                        }
                    }

                    $hash = $this->hashSSHA($password);
                    $encrypted_password = $hash["encrypted"];
                    $salt = $hash["salt"];
                    $result = $this->admin_model->create_admin($name, $email, $encrypted_password, $salt, $district, $location, $lat, $long, $catt, $contact, $image_url, $cate);
                    if ($result == true) {
                        $this->session->set_flashdata('success-msg', 'New Dealer is successfully created!');
                        redirect('dealer');
                    } else {
                        $this->session->set_flashdata('failure-msg', 'Something went wrong! Please try again!!');
                        redirect('dealer');
                    }
                } else {
                    $this->session->set_flashdata('failure-msg', 'Duplicate email or contact!!');
                    redirect('dealer');
                }
            }
        } else {
            $this->data['error'] = $this->upload->display_errors();
            $this->session->set_flashdata('failure-msg', $this->data['error']);
            redirect('dealer');
        }
    }

    public function search() {
        if (!empty($_POST)) {
            $search = $_POST['search'];
            $this->data['admin'] = $this->admin_model->search_admin($search);
            $this->data['search_list'] = $this->admin_model->get_admin();
            $this->data['category'] = $this->category_model->get_category();
            $count = count($this->data['category']);
            $result = array("zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine");
            $this->data['inword'] = $result[$count];
            $this->load->view('super_admin/dealers_view', $this->data);
        } else {
            redirect('dealer');
        }
    }

    public function get_user_details() {
        $uid = $_POST['id'];
        $result = $this->admin_model->get_dealer($uid);
        if (count($result) > 0) {
            echo json_encode($result);
        } else if ($result == FALSE) {
            echo json_encode(array('success-msg' => " Could Not Complete the Request. "));
        } else {
            echo json_encode(array('success-msg' => " No Records Available. "));
        }
    }

    public function edit_admin() {
        $uid = $_POST['id'];
        $name = $_POST['name'];
        $email = $_POST['email'];
        $newpassword = $_POST['password'];
        $district = $_POST["district"];
        $location = $_POST['location'];
        $lat = $_POST['lat'];
        $long = $_POST['long'];
        $contact = $_POST['contact'];
        $category = $this->category_model->get_category();

        $this->data['dir'] = 'assets/imgs/dealer/';
        $config['upload_path'] = $this->data['dir'];
        $config['allowed_types'] = 'gif|jpg|png|jpeg|x-png';
        $config['max_size'] = '400';
        $config['max_width'] = '1000';
        $config['max_height'] = '900';
        $config['remove_spaces'] = TRUE;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload()) {
            $img = $this->upload->data();

            $config['image_library'] = "gd2";
            $config['source_image'] = $this->data['dir'] . $img['file_name'];
            $config['create_thumb'] = false;
            $config['new_image'] = 'dealer_' . $img['file_name'];
            $config['maintain_ratio'] = TRUE;
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();

            if (!$this->image_lib->resize()) {
                echo $this->image_lib->display_errors();
            } else {
                $image_url = base_url() . $this->data['dir'] . 'dealer_' . $img['file_name'];
                unlink($this->data['dir'] . $img['file_name']);
            }
        } else {
            $this->data['error'] = $this->upload->display_errors();
            if ($this->data['error'] != '<p>You did not select a file to upload.</p>') {
                $this->session->set_flashdata('failure-msg', $this->data['error']);
                redirect('dealer');
            }
            $image_url = '';
        }

        $catt = array();
        $cate = '';
        foreach ($category as $cat) {
            $catt = array_merge($catt, array("type_" . strtolower($cat['type']) => @$_POST['type_' . strtolower($cat['type'])]));
            if (@$_POST['type_' . strtolower($cat['type'])] == 'on') {
                if ($cate == '') {
                    $cate = $cat['type'];
                } else {
                    $cate = $cate . ',' . $cat['type'];
                }
            }
        }
        $hash = $this->hashSSHA($newpassword);
        $encrypted_password = $hash["encrypted"];
        $salt = $hash["salt"];
        $result = $this->admin_model->update_admin($uid, $name, $email, $newpassword, $encrypted_password, $salt, $district, $location, $lat, $long, $catt, $contact, $image_url, $cate);
        if ($result == true) {
            $this->session->set_flashdata('success-msg', 'Dealer Updated successfully!');
            redirect('dealer');
        } else {
            $this->session->set_flashdata('failure-msg', 'Something went wrong! Please try again!!');
            redirect('dealer');
        }
    }

    public function delete_selected_dealer($uid) {
        $result = $this->admin_model->delete_user($uid);
        if ($result == TRUE) {
            $this->session->set_flashdata('success-msg', ' Dealer Successfully Removed. ');
            redirect('dealer');
        } else {
            $this->session->set_flashdata('failure-msg', ' ERROR! Someting Went Wrong. ');
            redirect('dealer');
        }
    }

    public function hashSSHA($password) {
        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

}
