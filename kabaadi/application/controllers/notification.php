<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Notification extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('notification_model');
        $this->data['uri'] = 'notification';
        if ($this->session->userdata('group') != '1') {
            redirect('login');
        }

        //session_start();
    }

    public function index() {
        $this->data['messages'] = $this->notification_model->get_messages();
        $this->load->view('super_admin/notice_view', $this->data);
    }

    public function send_message() {
        $uid = $_POST['uid'];
        $subject = $_POST['subject'];
        $msg = $_POST['message'];
        $group = $this->session->userdata('group');
        $admin_id = $this->session->userdata('id');

        $data = $this->notification_model->get_regid($uid);
        if (count($data) > 0) {
            if ($data[0]['device_type'] == 'Android') {
                $regids = array();
//            $regid_ios = array();
                $message_android = array("subject" => $subject, "message" => $msg, "tag" => "notice");
                foreach ($data as $row) {
                    array_push($regids, $row['device_id']);
                }

                $return_msg = $this->notification_model->send_notification_android($regids, $message_android);
                if ($return_msg['success'] == 1) {
                    $this->notification_model->save_message($uid, $group, $admin_id, $subject, $msg);
                    $this->session->set_flashdata('success-msg', 'Message Successfully send!');
                    redirect('users');
                } else {
                    $this->session->set_flashdata('failure-msg', $msg['results'][0]['error']);
                    redirect('users');
                }
            } elseif ($data[0]['device_type'] == 'ios') {
                $regid_ios = array();
                $subject_ios = $subject;
                $message_ios = $msg;
                foreach ($data as $row) {
                    array_push($regid_ios, $row['device_id']);
                }
                $registration_id_ios = count($regid_ios);
                for ($i = 0; $i < $registration_id_ios; $i++) {
                    $return_msg = $this->notification_model->send_notification_ios($regid_ios[$i], $subject_ios, $message_ios);
                }
                if ($return_msg['success'] == 'true') {
                    $this->notification_model->save_message($uid, $group, $admin_id, $subject, $msg);
                    $this->session->set_flashdata('success-msg', 'Message Successfully send!');
                    redirect('users');
                } else {
                    $this->session->set_flashdata('failure-msg', $return_msg['error']);
                    redirect('users');
                }
            }
        }
    }

}
