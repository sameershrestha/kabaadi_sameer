
<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login_Google extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('login_model');
        if ($this->session->userdata('email') != '' && $this->session->userdata('group') == '1') {
            redirect('main');
        } elseif ($this->session->userdata('email') != '' && $this->session->userdata('group') == '2') {
            redirect('main');
        } elseif ($this->session->userdata('email') != '' && $this->session->userdata('group') == '3') {
            redirect('main');
        }
    }

    public function index() {
        include_once("assets/google/config.php");
        include_once("assets/google/functions.php");
        if (isset($_REQUEST['code'])) {
            $gClient->authenticate();
            $_SESSION['token'] = $gClient->getAccessToken();
        }

        if (isset($_SESSION['token'])) {
            $gClient->setAccessToken($_SESSION['token']);
        }
        if ($gClient->getAccessToken()) {
            $userProfile = $google_oauthV2->userinfo->get();
            if ($userProfile['verified_email'] == true) {
                $username = $userProfile['email'];
                $result = $this->login_model->get_username($username);
                $count = count($result);
                if ($count > 0) {
                    foreach ($result as $row) {
                        $this->session->set_userdata('email', $row['email']);
                        $this->session->set_userdata('contact', $row['contact']);
                        $this->session->set_userdata('name', $row['name']);
                        $this->session->set_userdata('group', $row['group_id']);
                        $this->session->set_userdata('id', $row['id']);
                    }
                    if ($result[0]['group_id'] == 1) {
                        redirect('main');
                    } elseif ($result[0]['group_id'] == 2) {
                        redirect('main');
                    } elseif ($result[0]['group_id'] == 3) {
                        redirect('main');
                    }
                } else {
                    $this->session->set_flashdata('login_error', 'Please use mobile app to register first!');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('login_error', 'Not Verified Email!');
                redirect('login');
            }
//            $_SESSION['token'] = $gClient->getAccessToken();
        }
    }

}
