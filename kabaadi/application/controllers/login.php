<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('login_model');
    }

    public function index() {

        if ($this->session->userdata('email') != '' && $this->session->userdata('group') == '1') {
            redirect('main');
        } elseif ($this->session->userdata('email') != '' && $this->session->userdata('group') == '2') {
            redirect('main');
        } elseif ($this->session->userdata('email') != '' && $this->session->userdata('group') == '3') {
            redirect('main');
        }
        $this->load->view('login_view');
    }

    public function process_login() {
        $username = $_POST["email"];
        $password = $_POST['password'];
        $result = $this->login_model->get_password($username);
        if (count($result) == 1) {
            $hash = $this->checkhashSSHA($result[0]['salt'], $password);
            if ($hash == $result[0]['password']) {
                foreach ($result as $row) {
                    $this->session->set_userdata('email', $row['email']);
                    $this->session->set_userdata('contact', $row['contact']);
                    $this->session->set_userdata('name', $row['name']);
                    $this->session->set_userdata('group', $row['group_id']);
                    $this->session->set_userdata('id', $row['id']);
                }
                if ($result[0]['group_id'] == 1) {
                    redirect('main');
                } elseif ($result[0]['group_id'] == 2) {
                    redirect('main');
                } elseif ($result[0]['group_id'] == 3) {
                    redirect('main');
                }
            } else {
                $this->session->set_flashdata('login_error', 'Please provide valid password!');
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('login_error', 'Please provide valid username!');
            redirect('login');
        }
    }

    public function process_signup() {
        $contact = $_POST["contact"];
        $district = $_POST["district"];
        $location = $_POST["location"];
        $lat = $_POST["lat"];
        $long = $_POST["long"];
        $password = $_POST['password'];
        $c_password = $_POST['c_password'];
        $result = $this->login_model->get_contact($contact);
        if (count($result) != 1) {
            if ($password == $c_password) {
                $hash = $this->hashSSHA($password);
                $encrypted_password = $hash["encrypted"];
                $salt = $hash["salt"];
                $result = $this->login_model->signup_users($contact, $district, $location, $lat, $long, $encrypted_password, $salt);
                if ($result == true) {
                    $this->session->set_flashdata('msg', 'Account Successfully Created. Please login using your credentials!');
                    redirect('login');
                }
            } else {
                $this->session->set_flashdata('login_error', 'Password miss match!');
                redirect('login');
            }
        } else {
            $this->session->set_flashdata('login_error', 'Contact Number already exist !');
            redirect('login');
        }
    }

    public function password_change() {
        $this->data['uri'] = 'change password';
        $this->data['uri_segment'] = 'change password';
        $this->load->view('change_password_view', $this->data);
    }

    public function change_password() {
        $old_password = $_POST['old_psw'];
        $new_password = $_POST['new_psw'];
        $confirm_password = $_POST['con_psw'];
        $user_id = $this->session->userdata('id');
        $result = $this->login_model->get_old_password($user_id);
        if (count($result) > 0) {
            $hash = $this->checkhashSSHA($result[0]['salt'], $old_password);
            if ($hash == $result[0]['password']) {
                if ($old_password != $new_password) {
                    if ($new_password == $confirm_password) {
                        $hash = $this->hashSSHA($new_password);
                        $encrypted_password = $hash["encrypted"];
                        $salt = $hash["salt"];
                        $success = $this->login_model->change_pass($user_id, $encrypted_password, $salt);
                        if ($success == true) {
                            $this->session->set_flashdata('success-msg', 'Your password has been successfully changed!');
                            redirect('login/password_change');
                        } else {
                            $this->session->set_flashdata('failure-msg', "Sorry!! Something went wrong !!");
                            redirect('login/password_change');
                        }
                    } else {
                        $this->session->set_flashdata('failure-msg', "Sorry!! Password didn't match.Please Try Again !!");
                        redirect('login/password_change');
                    }
                } else {
                    $this->session->set_flashdata('failure-msg', "Sorry!! Your new password and the old password is same!!");
                    redirect('login/password_change');
                }
            } else {
                $this->session->set_flashdata('failure-msg', "Sorry!!Old Password is wrong.Please Try Again !!");
                redirect('login/password_change');
            }
        } else {
            $this->session->set_flashdata('failure-msg', "Sorry!! Something went wrong !!");
            redirect('login/password_change');
        }
    }

    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }
    public function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }
}