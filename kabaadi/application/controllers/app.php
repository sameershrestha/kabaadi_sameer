<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

header('Access-Control-Allow-Origin: *');

/**
 * Login Controller handles Login
 */
class App extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->helper('cookie');
        $this->load->model('app_model');
    }

    public function login() {
        $username = @$_REQUEST["username"];
        $device_id = @$_REQUEST['device_id'];
        $device_type = @$_REQUEST['device_type'];
        if (!empty($_REQUEST['username']) && !empty($_REQUEST['device_id']) && !empty($_REQUEST['device_type'])) {
            $result = $this->app_model->get_username($username);
            $count = count($result);
            if ($count > 0) {
                $user = 'old';
                $cookie = $this->generateRandomString() . $device_id;
                $log = $this->app_model->login_process($result[0]['id'], $device_id, $device_type, $cookie, $user);
                if ($log == true) {
                    $cookies = array(
//                        'name' => 'session_' . $cookie,
                        'name' => 'wasteapp_session',
                        'value' => $cookie,
                        'expire' => time() + 86500,
                        'path' => '/',
                    );
                    set_cookie($cookies);
                    $this->output->set_status_header('200', 'Login Success');
                    $this->output->set_header($cookie, true);
                    $row['status'] = 'success';
                    $row['msg'] = 'Login Success';
                    echo json_encode($row);
                } else {
                    $this->output->set_status_header('403', 'Login Failed');
                    $row['status'] = 'failed';
                    $row['msg'] = 'Login failed';
                    echo json_encode($row);
                }
            } else {
                $user = 'new';
                $cookie = $this->generateRandomString() . $device_id;
                $log = $this->app_model->login_process($username, $device_id, $device_type, $cookie, $user);
                if ($log == true) {
                    $cookies = array(
                        'name' => 'wasteapp_session',
                        'value' => $cookie,
                        'expire' => time() + 86500,
                        'path' => '/',
                    );
                    set_cookie($cookies);
                    $this->output->set_status_header('200', 'Login Success');
                    $this->output->set_header($cookie, true);
                    $row['status'] = 'success';
                    $row['msg'] = 'Login Success';
                    echo json_encode($row);
                } else {
                    $this->output->set_status_header('403', 'Login Failed');
                    $row['status'] = 'failed';
                    $row['msg'] = 'Login failed';
                    echo json_encode($row);
                }
            }
        } else {
            if (empty($_REQUEST["username"])) {
                $this->output->set_status_header('403', 'Empty Username');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Username';
                echo json_encode($row);
            } elseif (empty($_REQUEST["device_id"])) {
                $this->output->set_status_header('403', 'Empty Device Id');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Device Id';
                echo json_encode($row);
            } elseif (empty($_REQUEST["device_type"])) {
                $this->output->set_status_header('403', 'Empty Device Type');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Device Type';
                echo json_encode($row);
            }
        }
    }

    public function logout() {
        $username = @$_REQUEST['username'];
        $session = @$_REQUEST['session'];
        if (!empty($_REQUEST['username']) && !empty($_REQUEST['session'])) {
            $cok = get_cookie('wasteapp_session');
            if ($cok != '') {
                $cookie = array(
                    'name' => 'wasteapp_session',
                    'value' => '',
                    'expire' => 0,
                );
                delete_cookie($cookie);
            }
            $session_id = $this->app_model->get_session($username);
            if ($session_id[0]['session'] != '') {
                $session = '';
                $result = $this->app_model->delete_session($session_id[0]['user_id'], $session);
                if ($result == true) {
                    $this->output->set_status_header('200', 'Successful Logout');
                    $this->session->sess_destroy();
                    $row['status'] = 'success';
                    $row['msg'] = 'Successful Logout';
                    echo json_encode($row);
                }
            } else {
                $this->output->set_status_header('403', 'Invalid Session');
                $row['status'] = 'failed';
                $row['msg'] = 'Invalid Session';
                echo json_encode($row);
            }
        } else {
            if (empty($_REQUEST["username"])) {
                $this->output->set_status_header('403', 'Empty Username');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Username';
                echo json_encode($row);
            } elseif (empty($_REQUEST["session"])) {
                $this->output->set_status_header('403', 'Empty session');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Session';
                echo json_encode($row);
            }
        }
    }

    public function getdealer() {
        if (!empty($_REQUEST['latlng'])) {
            $latlng = explode(',', $_REQUEST['latlng']);
            $this->data['dealer'] = 0;
            $kilom = 10;
            $result = $this->app_model->get_dealer();
            $data = array();
            foreach ($result as $row) {
                $theta = $latlng[1] - $row['longitude'];
                $dist = sin(deg2rad($latlng[0])) * sin(deg2rad($latlng[0])) + cos(deg2rad($latlng[0])) * cos(deg2rad($latlng[0])) * cos(deg2rad($theta));
                $dist = acos($dist);
                $dist = rad2deg($dist);
                $miles = $dist * 60 * 1.1515;
                $km = $miles * 1.609344;
                if ($km < $kilom) {
                    $rslt = $this->app_model->get_buyer($row['id']);

                    $data[] = array(
                        'dealer_id' => $rslt[0]['dealer_id'],
                        'name' => $rslt[0]['name'],
                        'district' => number_format($km, 2) . 'km',
                        'office_location' => $rslt[0]['office_location'],
                        'contact' => $rslt[0]['contact'],
                        'img_url' => $rslt[0]['img_url']
                    );
                }
            }

            $dealer['dealer'] = $this->array_multi_subsort($data, 'district');
            if (count($dealer['dealer']) > 0) {
                $dealer['status'] = 'success';
                echo json_encode($dealer);
            } else {
                $this->output->set_status_header('204');
            }
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'Empty location';
            echo json_encode($row);
        }
    }

    public function array_multi_subsort($array, $subkey) {
        $b = array();
        $c = array();

        foreach ($array as $k => $v) {
            $b[$k] = strtolower($v[$subkey]);
        }

        asort($b);
        foreach ($b as $key => $val) {
            $c[] = $array[$key];
        }

        return $c;
    }

    public function getcategory() {
        if (!empty($_REQUEST['dealer'])) {
            $buyer = $_REQUEST['dealer'];
            $result = $this->app_model->get_category($buyer);
            $category = array();
            foreach ($result as $row) {
                $cat = $this->app_model->get_buyer_category($row['cat_id'], $row['admin_id']);
                $category['data'][] = array(
                    'category_id' => $row['cat_id'],
                    'category_name' => $row['category'],
                    'category_img' => $row['img_url'],
                    'subcategory' => $cat
                );
            }
            $category['status'] = 'success';
            echo json_encode($category);
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'Empty dealer';
            echo json_encode($row);
        }
    }

    public function pickup_request() {
        $title = @$_REQUEST['title'];
        $username = @$_REQUEST['username'];
        $data = $this->app_model->get_user($username);
        if (count($data) > 0) {
            $uid = $data[0]['user_id'];

            $did = @$_REQUEST['dealer'];
            $pickup_contact = @$_REQUEST['contact_no'];
            $pickup_address = @$_REQUEST['address'];
            $house_no = @$_REQUEST['house_no'];

            $latlng = explode(',', @$_REQUEST['latlng']);
            $lat = $latlng[0];
            $lng = $latlng[1];
            $total = @$_REQUEST['id_qty'];
            $id_qty = explode(',', $total);

            if (!empty($_REQUEST['title']) && !empty($_REQUEST['username']) && !empty($_REQUEST['dealer']) && !empty($_REQUEST['contact_no']) && !empty($_REQUEST['address']) && !empty($_REQUEST['latlng']) && !empty($_REQUEST['id_qty'])) {
                $result1 = $this->app_model->save_pickup($uid, $did, $title, $pickup_contact, $pickup_address, $house_no, $lat, $lng);
                $activity_id = $result1;
                if ($result1 != '') {
                    $result2 = $this->app_model->get_date($result1);
                    if (count($result1) > 0) {
                        foreach ($id_qty as $raw) {
                            $iq = explode(':', $raw);
                            $result = $this->app_model->get_rate($iq[0], $did);
                            $sub_type = $result[0]['sub_type'];
                            $rate = $result[0]['rate'];
                            $tot = $iq[1] * $rate;
                            $result3 = $this->app_model->save_request($activity_id, $uid, $did, $title, $sub_type, $rate, $iq[1], $tot, $result2[0]['added_date']);
                        }
//                    $row['pickup_status'] = $this->app_model->get_pickup_status($activity_id);
                        $this->output->set_status_header('200', 'Successful Submission');
                        $row['status'] = 'success';
                        $row['msg'] = 'Pickup request submited successfully';
                        echo json_encode($row);
                    } else {
                        $row['status'] = 'failed';
                        $row['msg'] = 'Empty dealer';
                        echo json_encode($row);
                    }
                } else {
                    $row['status'] = 'failed';
                    $row['msg'] = 'Please Try Again';
                    echo json_encode($row);
                }
            } else {
                $row['status'] = 'failed';
                $row['msg'] = 'Some Post value is empty';
                echo json_encode($row);
            }
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'User Doesnot exist';
            echo json_encode($row);
        }
    }

    public function image_pickup_request() {
        $title = @$_REQUEST['title'];
        $username = @$_REQUEST['username'];
        $data = $this->app_model->get_user($username);
        if (count($data) > 0) {
            $uid = $data[0]['user_id'];

            $did = @$_REQUEST['dealer'];
            $pickup_contact = @$_REQUEST['contact_no'];
            $pickup_address = @$_REQUEST['address'];
            $house_no = @$_REQUEST['house_no'];

            $latlng = explode(',', @$_REQUEST['latlng']);
            $lat = $latlng[0];
            $lng = $latlng[1];
            if (!empty($_REQUEST['title']) && !empty($_REQUEST['username']) && !empty($_REQUEST['dealer']) && !empty($_REQUEST['contact_no']) && !empty($_REQUEST['address']) && !empty($_REQUEST['latlng']) && !empty($_FILES)) {
                $result1 = $this->app_model->save_pickup_img($uid, $did, $title, $pickup_contact, $pickup_address, $house_no, $lat, $lng);
                $activity_id = $result1;
                if ($result1 != '') {
                    foreach ($_FILES as $key => $value) {
                        $file_path = "assets/pickupimgs/";
                        $file_path = $file_path . basename($_FILES[$key]['name']);
                        if (move_uploaded_file($_FILES[$key]['tmp_name'], $file_path)) {
                            $row['file_status'] = 'success';
                            $row['file_msg'] = 'File Uploaded successfully';
                        } else {
                            $row['file_status'] = 'failed';
                            $row['file_msg'] = 'Failed';
                        }
                        $image_url = base_url() . $file_path;

                        $result2 = $this->app_model->add_pickup_img($activity_id, $image_url);
                    }
                    if ($result2 != '') {
                        $row['status'] = 'success';
                        $row['msg'] = 'Pickup request submited successfully';
                        echo json_encode($row);
                    } else {
                        $row['status'] = 'failed';
                        $row['msg'] = 'Check Intenet Connection';
                        echo json_encode($row);
                    }
                } else {
                    $row['status'] = 'failed';
                    $row['msg'] = 'Check Intenet Connection';
                    echo json_encode($row);
                }
            } else {
                $row['status'] = 'failure';
                $row['msg'] = 'Some data is missing';
                echo json_encode($row);
            }
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'User Doesnot exist';
            echo json_encode($row);
        }
    }

    public function file_upload() {
        if (isset($_FILES) && !empty($_FILES)) {
            foreach ($_FILES as $key => $value) {
                $file_path = "assets/pickupimgs/";
                $file_path = $file_path . basename($_FILES[$key]['name']);
                if (move_uploaded_file($_FILES[$key]['tmp_name'], $file_path)) {
                    $row['status'] = 'success';
                    $row['msg'] = 'File Uploaded successfully';
                    echo json_encode($row);
                } else {
                    $row['status'] = 'failed';
                    $row['msg'] = 'Failed';
                    echo json_encode($row);
                }
//                $this->data['dir'] = 'assets/pickupimgs/';
//                $config['upload_path'] = $this->data['dir'];
//                $config['allowed_types'] = 'gif|jpg|png|jpeg|x-png';
//                $config['max_size'] = '1000';
//                $config['max_width'] = '1000';
//                $config['max_height'] = '900';
//                $config['remove_spaces'] = TRUE;
//                $this->load->library('upload', $config);
//                if ($this->upload->do_upload($key)) {
//                    $img = $this->upload->data();
//
//                    $config['image_library'] = "gd2";
//                    $config['source_image'] = $this->data['dir'] . $img['file_name'];
//                    $config['create_thumb'] = false;
//                    $config['new_image'] = 'pickup_' . $img['file_name'];
//                    $config['maintain_ratio'] = TRUE;
//                    $this->load->library('image_lib', $config);
//
//                    $image_url = base_url() . $this->data['dir'] . 'pickup_' . $img['file_name'];
//                    unlink($this->data['dir'] . $img['file_name']);
//                    $row['status'] = 'success';
//                    $row['msg'] = 'File Uploaded successfully';
//                    echo json_encode($row);
//                } else {
//                    $this->data['error'] = $this->upload->display_errors();
//                    $row['status'] = 'failed';
//                    $row['msg'] = $this->data['error'];
//                    echo json_encode($row);
//                }
            }
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'file is empty';
            echo json_encode($row);
        }
    }

    public function user_activities() {
        $username = @$_REQUEST['username'];
        if ($_REQUEST['username'] != '') {
            $data = $this->app_model->get_user($username);
            $uid = $data[0]['user_id'];
            $activity = $this->app_model->get_activity_status($uid);
            $this->data = array();
            foreach ($activity as $row) {
                $act = $this->app_model->get_activity($row['activity_id']);
                $total_price = 0;
                $total_kg = 0;
                foreach ($act as $activity) {
                    $total_kg = $total_kg + $activity['quantity'];
                    $total_price = $total_price + $activity['total_price'];
                }
                $this->data['history'][] = array(
                    'title' => $row['title'],
                    'location' => $row['location'],
                    'total_kg' => $total_kg,
                    'total_price' => $total_price,
                    'added_date' => substr($row['added_date'], 0, 10)
                );
            }
            if (isset($this->data['history']) && count($this->data['history']) > 0) {
                $this->data['status'] = 'success';
            } else {
//                $this->output->set_status_header('204');
                $this->data['status'] = 'empty';
            }

            echo json_encode($this->data);
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'Username doesnot exist';
            echo json_encode($row);
        }
    }

    public function activity_detail() {
        $activity_id = @$_REQUEST['activity_id'];
        if ($_REQUEST['activity_id'] != '') {
            $status = $this->app_model->get_status($activity_id);
            $this->data['activity'] = $this->app_model->get_activity($activity_id);
            $this->data['total'] = 0;
            foreach ($this->data['activity'] as $activity) {
                $this->data['total'] = $this->data['total'] + $activity['total_price'];
            }
            $this->data['status'] = 'success';
            echo json_encode($this->data);
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'Activity Id is empty';
            echo json_encode($row);
        }
    }

    public function pickup_status() {
        $username = @$_REQUEST['username'];
        $result = $this->app_model->get_username($username);
        if ($_REQUEST['username'] != '') {
            if (count($result) > 0) {
                $row['pickup_status'] = $this->app_model->get_all_activity_status($result[0]['id']);
                $row['status'] = 'success';
            } else {
//                $this->output->set_status_header('204');
                $row['status'] = 'failed';
                $row['msg'] = 'No user Found';
            }
        } else {
            $row['status'] = 'failed';
            $row['msg'] = 'Username Empty';
        }
        echo json_encode($row);
    }

//    public function delete_user_activities() {
//        $session = @$_REQUEST['session'];
//        $result = $this->app_model->get_sess($session);
//        if ($_REQUEST['session'] != '') {
//            if (count($result) > 0) {
//                $data = $this->app_model->delete_all_activities($result[0]['user_id']);
//                if ($data == true) {
//                    $row['status'] = 'success';
//                } else {
//                    $row['status'] = 'failure';
//                    $row['msg'] = 'Check Internet Connection';
//                }
//            } else {
//                $row['status'] = 'failed';
//                $row['msg'] = 'User is not logged in';
//            }
//        } else {
//            $row['status'] = 'failed';
//            $row['msg'] = 'Session Empty';
//        }
//        echo json_encode($row);
//    }

    public function aboutus() {
        $aboutus['aboutus'] = $this->app_model->get_aboutus();
        if (count($aboutus['aboutus'] > 0)) {
            $aboutus['status'] = 'success';
        } else {
            $aboutus['status'] = 'failure';
        }
        echo json_encode($aboutus);
    }

    public function aboutus_ios() {
        $aboutus['aboutus'] = $this->app_model->get_aboutus();
        if (count($aboutus['aboutus'] > 0)) {
            $aboutus['status'] = 'Success';
        } else {
            $aboutus['status'] = 'Failure';
        }
        echo json_encode($aboutus);
    }

    public function admin_login() {
        $username = @$_REQUEST["username"];
        $password = @$_REQUEST['password'];
        if (!empty($_REQUEST['username']) && !empty($_REQUEST['password'])) {
            $result = $this->app_model->get_admin($username);
            $count = count($result);
            if ($count == 1) {
                $hash = $this->checkhashSSHA($result[0]['salt'], $password);
                if ($hash == $result[0]['password']) {
                    $cookie = $this->generateRandomString();
                    $cookies = array(
//                        'name' => 'session_' . $cookie,
                        'name' => 'wasteapp_session',
                        'value' => $cookie,
                        'expire' => time() + 86500,
                        'path' => '/',
                    );
                    set_cookie($cookies);
                    $this->output->set_status_header('200', 'Login Success');
                    $this->output->set_header($cookie, true);
                    $row['agent'] = $this->app_model->get_admin($username);
                    $row['pickup_request'] = $this->app_model->dealer_pickup_request($result[0]['id']);
                    $row['status'] = 'success';
                    $row['admin_id'] = $result[0]['id'];
                    $row['msg'] = 'Login Success';
                    echo json_encode($row);
                } else {
                    $this->output->set_status_header('403', 'Login Failed');
                    $row['status'] = 'failed';
                    $row['msg'] = 'Login failed!!Incorrect Password';
                    echo json_encode($row);
                }
            }
        } else {
            if (empty($_REQUEST["username"])) {
                $this->output->set_status_header('403', 'Empty Username');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Username';
                echo json_encode($row);
            } elseif (empty($_REQUEST["password"])) {
                $this->output->set_status_header('403', 'Empty Password');
                $row['status'] = 'failed';
                $row['msg'] = 'Empty Empty Password';
                echo json_encode($row);
            }
        }
    }

    public function pickup() {
        $aid = $_REQUEST["aid"];
        $username = $_REQUEST["username"];

        $this->data['agent'] = $this->app_model->get_admin($username);
        $this->data['pickup_request'] = $this->app_model->dealer_pickup_request($aid);
        echo json_encode($this->data);
    }

    public function pickup_status_change() {
        $this->load->model('pickup_model');
        $this->load->model('notification_model');
        $pid = $_REQUEST["pid"];
        $uid = $_REQUEST["uid"];
        $process = $_REQUEST["status"];
        if ($process == '1') {
            $message = 'We are processing ur request';
            $status = 1;
            $process == 'onprocess';
            $result = $this->pickup_model->change_status($pid, $status);
        } elseif ($process == '2') {
            $message = 'Your waste has been collected';
            $status = 2;
            $process == 'collected';
            $result = $this->pickup_model->change_status($pid, $status);
        } elseif ($process == '3') {
            $message = 'Your request has been cancelled';
            $status = 3;
            $process == 'canceled';
            $result = $this->pickup_model->change_status($pid, $status);
        }
        if ($result == true) {
            $status_result['status'] = 'success';
            $this->load->model('notification_model');
            $data = $this->notification_model->get_regid($uid);
            if (count($data) > 0) {
                if ($data[0]['device_type'] == 'Android') {
                    $regids = array();
                    $message_android = array("tag" => "tag_" . $process, "title" => $process, "message" => $message);
                    foreach ($data as $row) {
                        array_push($regids, $row['device_id']);
                    }
                    $status_result['regid'] = $regids;
                    $msg = $this->notification_model->send_notification_android($regids, $message_android);
                    if ($msg['success'] == 1) {
                        $status_result['notification_status'] = 'success';
                    } else {
                        $status_result['notification_status'] = 'failure';
                    }
                } elseif ($data[0]['device_type'] == 'ios') {
                    $regid_ios = array();
                    $message_ios = $process;
                    foreach ($data as $row) {
                        array_push($regid_ios, $row['device_id']);
                    }
                    $registration_id_ios = count($regid_ios);
                    for ($i = 0; $i < $registration_id_ios; $i++) {
                        $return_msg = $this->notification_model->send_process_notification_ios($regid_ios[$i], $message_ios);
                    }
                    if ($return_msg['success'] == 'true') {
                        $status_result['notification_status'] = 'success';
                    } else {
                        $status_result['notification_status'] = 'failure';
                    }
                }
            } else {
                $status_result['notification_status'] = 'failure';
            }
        } else {
            $status_result['status'] = 'failure';
        }
        echo json_encode($status_result);
    }

    public function pickup_image() {
        $aid = $_REQUEST["aid"];
        $username = $_REQUEST["username"];

        $this->data['agent'] = $this->app_model->get_admin($username);
        $this->data['pickup_request_img'] = $this->app_model->dealer_pickup_request_img($aid);
        echo json_encode($this->data);
    }

    public function pickup_status_change_image() {
        $this->load->model('pickup_model');
        $this->load->model('notification_model');
        $pid = $_REQUEST["pid"];
        $uid = $_REQUEST["uid"];
        $process = $_REQUEST["status"];
        if ($process == '1') {
            $message = 'We are processing ur request';
            $status = 1;
            $process == 'onprocess';
            $result = $this->pickup_model->change_status_img($pid, $status);
        } elseif ($process == '2') {
            $message = 'Your waste has been collected';
            $status = 2;
            $process == 'collected';
            $result = $this->pickup_model->change_status_img($pid, $status);
        } elseif ($process == '3') {
            $message = 'Your request has been cancelled';
            $status = 3;
            $process == 'canceled';
            $result = $this->pickup_model->change_status_img($pid, $status);
        }
        if ($result == true) {
            $status_result['status'] = 'success';
            $this->load->model('notification_model');
            $data = $this->notification_model->get_regid($uid);
            if (count($data) > 0) {
                if ($data[0]['device_type'] == 'Android') {
                    $regids = array();
                    $message_android = array("tag" => "tag_" . $process, "title" => $process, "message" => $message);
                    foreach ($data as $row) {
                        array_push($regids, $row['device_id']);
                    }
                    $status_result['regid'] = $regids;
                    $msg = $this->notification_model->send_notification_android($regids, $message_android);
                    if ($msg['success'] == 1) {
                        $status_result['notification_status'] = 'success';
                    } else {
                        $status_result['notification_status'] = 'failure';
                    }
                } elseif ($data[0]['device_type'] == 'ios') {
                    $regid_ios = array();
                    $message_ios = $process;
                    foreach ($data as $row) {
                        array_push($regid_ios, $row['device_id']);
                    }
                    $registration_id_ios = count($regid_ios);
                    for ($i = 0; $i < $registration_id_ios; $i++) {
                        $return_msg = $this->notification_model->send_process_notification_ios($regid_ios[$i], $message_ios);
                    }
                    if ($return_msg['success'] == 'true') {
                        $status_result['notification_status'] = 'success';
                    } else {
                        $status_result['notification_status'] = 'failure';
                    }
                }
            } else {
                $status_result['notification_status'] = 'failure';
            }
        } else {
            $status_result['status'] = 'failure';
        }
        echo json_encode($status_result);
    }

    function generateRandomString($length = 16) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

}
