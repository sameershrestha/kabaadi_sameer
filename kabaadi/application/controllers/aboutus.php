<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Aboutus extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->data['uri'] = 'aboutus';
        $this->load->model('about_model');
        $this->load->model('notification_model');
        if ($this->session->userdata('id') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 1) {
            redirect('main');
        }

        //session_start();
    }

    public function index() {
        $this->data['aboutus'] = $this->about_model->get_about();
        $this->load->view('super_admin/aboutus_view', $this->data);
    }

    function edit() {

        if ($this->input->post('submit_detail') == 'Submit') {
            $this->data['dir'] = 'assets/imgs/about/';
            $config['upload_path'] = $this->data['dir'];
            $config['allowed_types'] = 'gif|jpg|png|jpeg|x-png';
            $config['max_size'] = '1000';
            $config['remove_spaces'] = TRUE;
            $this->load->library('upload', $config);
            if ($this->upload->do_upload()) {
                $img = $this->upload->data();
                $config['image_library'] = "gd2";
                $config['source_image'] = $this->data['dir'] . $img['file_name'];
                $config['create_thumb'] = false;
                $config['maintain_ratio'] = TRUE;
                $this->load->library('image_lib', $config);
                $this->image_lib->resize();

                if (!$this->image_lib->resize()) {
                    $this->session->set_flashdata('failure-msg', $this->image_lib->display_errors());
                    redirect('aboutus');
                } else {
                    $file = base_url() . $this->data['dir'] . $img['file_name'];
                    $result = $this->about_model->update_about($_POST['title'], $_POST['description'], $file);
                    if ($result == true) {

                        $data = $this->about_model->get_all_regid();

                        $regids = array();
                        $regid_ios = array();
                        $message_android = array("tag" => "aboutus");
                        foreach ($data as $row) {
                            if ($row['device_type'] == 'Android') {
                                array_push($regids, $row['device_id']);
                            } elseif ($row['device_type'] == 'ios') {
                                array_push($regid_ios, $row['device_id']);
                            }
                        }
                        $this->notification_model->send_notification_android($regids, $message_android);

                        $tag = 'aboutus';
                        $message_ios = '';

                        $registration_id_ios = count($regid_ios);
                        for ($i = 0; $i < $registration_id_ios; $i++) {
                            $this->notification_model->send_notification_ios($regid_ios[$i], $tag, $message_ios);
                        }

                        $this->session->set_flashdata('success-msg', 'About Us is updated Successfully !!');
                        redirect('aboutus');
                    } else {
                        $this->session->set_flashdata('failure-msg', 'Something went wrong! Please try again!!');
                        redirect('aboutus');
                    }
                }
            } else {
                $file = '';
                $result = $this->about_model->update_about($_POST['title'], $_POST['description'], $file);
                if ($result == true) {

                    $data = $this->about_model->get_all_regid();

                    $regids = array();
                    $regid_ios = array();
                    $message_android = array("tag" => "aboutus");
                    foreach ($data as $row) {
                        if ($row['device_type'] == 'Android') {
                            array_push($regids, $row['device_id']);
                        } elseif ($row['device_type'] == 'ios') {
                            array_push($regid_ios, $row['device_id']);
                        }
                    }
                    $this->notification_model->send_notification_android($regids, $message_android);

                    $tag = 'aboutus';
                    $message_ios = '';

                    $registration_id_ios = count($regid_ios);
                    for ($i = 0; $i < $registration_id_ios; $i++) {
                        $this->notification_model->send_notification_ios($regid_ios[$i], $tag, $message_ios);
                    }

                    $this->session->set_flashdata('success-msg', 'About Us is updated Successfully !!');
                    redirect('aboutus');
                } else {
                    $this->session->set_flashdata('failure-msg', 'Something went wrong! Please try again!!');
                    redirect('aboutus');
                }
            }
        }

        redirect('aboutus');
    }

}
