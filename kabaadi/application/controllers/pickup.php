<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Pickup extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('pickup_model');
        $this->data['uri'] = 'pickup';
        if ($this->session->userdata('id') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 1) {
            redirect('main');
        }
    }

    public function index() {
        $this->data['dealers_only'] = $this->pickup_model->get_dealers_only();
        $this->data['pickup_request'] = $this->pickup_model->get_pickup_request();
//        $this->data['search_list'] = $this->data['pickup_request'];
        $this->load->view('super_admin/pickup_request_view', $this->data);
    }

    public function viewDetailsByDealer() {
        $dealer_name = $_POST['dealers_name'];
        //echo $dealer_name;
        $this->data['selected_value'] = $dealer_name;
        $this->data['dealers_only'] = $this->pickup_model->get_dealers_only();
        $this->data['pickup_request'] = $this->pickup_model->get_request_info_by($dealer_name);
        //$this->data['pickup_request'] = $this->pickup_model->get_pickup_request();
//        $this->data['search_list'] = $this->data['pickup_request'];
        $this->load->view('super_admin/pickup_request_view', $this->data);
    }

    public function search() {
        $search = $_POST['search'];

        if ($search == 'On Queue') {
            $search = 0;
        } else if ($search == 'On Process') {
            $search = 1;
        } else if ($search == 'Collected') {
            $search = 2;
        } else if ($search == 'Cancelled') {
            $search = 3;
        }
        $this->data['search_list'] = $this->pickup_model->get_pickup_request();
        $this->data['pickup_request'] = $this->pickup_model->search_pickup_request($search);
        $this->load->view('super_admin/pickup_request_view', $this->data);
    }

    public function pickup_details($pid, $uid) {
        $this->data['pid'] = $pid;
        $this->data['uid'] = $uid;
        $this->data['activity_date'] = $this->pickup_model->get_pickup_status($pid);
        $this->data['activity'] = $this->pickup_model->get_pickup($uid);
        $this->data['total'] = 0;
        foreach ($this->data['activity_date'] as $row) {
            foreach ($this->data['activity'] as $activity) {
                if ($row['id'] == $activity['activity_id']) {
                    $this->data['total'] = $this->data['total'] + $activity['total_price'];
                }
            }
        }
        $this->load->view('super_admin/pickup_details_view', $this->data);
    }

}
