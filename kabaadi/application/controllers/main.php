<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Main extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('admin_model');
        $this->load->model('category_model');
        $this->data['uri'] = 'home';
        if ($this->session->userdata('group') == '') {
            redirect('login');
        }
        //session_start();
    }

    public function index() {
        if ($this->session->userdata('group') == 1) {
            $this->data['users'] = $this->admin_model->get_users();
            $this->load->view('super_admin/main_view', $this->data);
        } elseif ($this->session->userdata('group') == 2) {
            $this->data['uri_segment'] = '';
            $did = $this->session->userdata('id');
            $this->data['users'] = $this->admin_model->get_pickup_users($did);
            $this->load->view('admin/main_view', $this->data);
        } elseif ($this->session->userdata('group') == 3) {
            redirect('mobile_users/users_pickup_details');
        }
    }

}
