<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/**
 * Login Controller handles Login
 */
class Mobile_Users extends CI_Controller {

    /**
     * @author:arpan
     * default controller
     * 10th Nov 2013
     */
    public function __construct() {
        parent::__construct();
        $this->load->helper("url");
        $this->load->library('session');
        $this->load->model('admin_model');
        $this->load->model('category_model');
        $this->load->model('mobile_user_model');
        if ($this->session->userdata('group') == '') {
            redirect('login');
        } elseif ($this->session->userdata('group') != 3) {
            redirect('main');
        }
        //session_start();
    }
    public function index(){
        $this->data['uid'] = $uid;
        $this->data['user_detail'] = $this->mobile_user_model->get_user_detail($uid);
        $this->load->view('users/myprofile',$this->data);
    }

    public function editprofile($uid){
        $this->data['uid'] = $uid;
        $this->data['user_detail'] = $this->mobile_user_model->get_user_detail($uid);
        //$this->data['activity'] = $this->pickup_model->get_pickup($uid);
        //$this->data['total'] = 0;
        //$this->load->view('super_admin/pickup_details_view', $this->data);
        $this->load->view('users/myprofile',$this->data);
    }

    public function choosen_location() {
        $this->data['uri'] = 'main';
        $this->data['uri_segment'] = 'category';
        $this->data['title'] = $_POST['title'];
        $this->data['location'] = $_POST['location'];
        $this->data['lat'] = $_POST['lat'];
        $this->data['lng'] = $_POST['long'];
        $this->data['buyer'] = $this->admin_model->get_buyer();
        $this->data['category'] = $this->category_model->get_category();
        $this->load->view('users/main_view', $this->data);
    }

    public function choosen_category($title, $location, $lat, $lng) {
        $this->data['uri'] = 'main';
        $this->data['uri_segment'] = 'sub-category';
        $this->data['category'] = $this->category_model->get_category();
        $this->data['subcat'] = $this->category_model->get_dealer_subcategory($_POST['dealer']);
        $this->data['title'] = $title;
        $this->data['location'] = $location;
        $this->data['lat'] = $lat;
        $this->data['lng'] = $lng;
        $this->data['dealer'] = $_POST['dealer'];
        $this->data['post'] = $_POST;
//        $count = count($_POST) - 4;
//        $this->data['dealer'] = 0;
//        $kilom = 100000;
//        $result = $this->mobile_user_model->get_dealer();
//        foreach ($result as $row) {
//            $c = 0;
//            foreach ($_POST as $key => $value) {
//                if (isset($row[$key]) && $row[$key] == 'on') {
//                    $d = $c++;
//                }
//            }
//            if ($count == $c) {
//                $theta = $_POST['long'] - $row['longitude'];
//                $dist = sin(deg2rad($_POST['lat'])) * sin(deg2rad($row['lat'])) + cos(deg2rad($_POST['lat'])) * cos(deg2rad($row['lat'])) * cos(deg2rad($theta));
//                $dist = acos($dist);
//                $dist = rad2deg($dist);
//                $miles = $dist * 60 * 1.1515;
//                $km = $miles * 1.609344;
//                if ($km < $kilom) {
//                    $kilom = $km;
//                    $this->data['dealer'] = $row['user_id'];
//                } else {
//                    $kilom = $kilom;
//                    $this->data['dealer'] = $this->data['dealer'];
//                }
//            }
//        }
        $this->load->view('users/main_view', $this->data);
    }

    public function get_subcat() {
        $category_id = $_POST['id'];
        $result = $this->category_model->get_subcategory($category_id);
        if (count($result) > 0) {
            echo json_encode($result);
        } else if ($result == FALSE) {
            echo json_encode(array('success-msg' => " Could Not Complete the Request. "));
        } else {
            echo json_encode(array('success-msg' => " No Records Available. "));
        }
    }

    public function get_rate() {
        $sub_type = $_POST['sub_type'];
        $result = $this->category_model->get_selected_rate($sub_type);
        if (count($result) > 0) {
            echo json_encode($result);
        } else if ($result == FALSE) {
            echo json_encode(array('success-msg' => " Could Not Complete the Request. "));
        } else {
            echo json_encode(array('success-msg' => " No Records Available. "));
        }
    }

    public function add_pickup($title, $location, $lat, $lng) {
        $title = str_replace('%20', ' ', $title);
        $buyer_id = $_POST['buyer'];
        $user_id = $this->session->userdata('id');
        $count = count($_POST);
        $post_count = ($count - 1);
        $loop = ($post_count / 4);
        $abs = 0;
        for ($i = 1; $i <= $loop; $i++) {
            $subcat = array_values($_POST);
            $subcat = $subcat[$abs + 0];
            $rate = array_values($_POST);
            $rate = $rate[$abs + 1];
            $quantity = array_values($_POST);
            $quantity = $quantity[$abs + 2];
            $total = array_values($_POST);
            $total = $total[$abs + 3];
            $result = $this->mobile_user_model->pickup_request_add($user_id, $buyer_id, $title, $subcat, $rate, $quantity, $total);
            $abs = ($abs + 4);
        }
        if ($result == true) {
            $result1 = $this->mobile_user_model->pickup_status_add($user_id, $buyer_id, $title, $location, $lat, $lng);
            if ($result1 == true) {
                redirect('mobile_users/confirm_pickup');
            }
        }
    }

    public function confirm_pickup() {
        $this->data['uri'] = 'main';
        $this->data['uri_segment'] = 'confirm';
        $this->load->view('users/main_view', $this->data);
    }

    public function users_pickup_details() {
        $this->data['uri'] = 'activity';
        $uid = $this->session->userdata('id');
        $this->data['activity_date'] = $this->mobile_user_model->get_activity_status($uid);
        $this->data['activity'] = $this->mobile_user_model->get_activity($uid);
        $this->data['total'] = 0;
        foreach ($this->data['activity_date'] as $row) {
            foreach ($this->data['activity'] as $activity) {
                if ($row['id'] == $activity['activity_id']) {
                    $this->data['total'] = $this->data['total'] + $activity['total_price'];
                }
            }
        }
        $this->load->view('users/activity_view', $this->data);
    }

}
